#!/bin/bash

cp src/common/_helpers.scss src/common/_helpers.scss.back
sed '1d' src/common/_helpers.scss.back > src/common/_helpers.scss
node_modules/.bin/ng-xi18n --i18nFormat xlf -p src/tsconfig.i18n.json
mv src/common/_helpers.scss.back src/common/_helpers.scss
