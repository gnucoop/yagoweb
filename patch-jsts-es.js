#!/env/bin/node

var fs = require('fs');

var packageFile = './node_modules/jsts-es/package.json';
var package = require(packageFile);
package.module = 'main';

fs.writeFileSync(packageFile, JSON.stringify(package, null, 2));

console.log("jsts-es successfully patched!");
