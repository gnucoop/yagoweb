#!/bin/sh

CUSTOMISATION=$1
DIR="customisations/$CUSTOMISATION"

if [ -d "$DIR" ]; then
  cp $DIR/custom.css src/
  cp -Rf $DIR/assets/* src/assets
  cp $DIR/app.component.html $DIR/app.routes.ts src/app/
  cp $DIR/home.html src/home/
else
  echo "Invalid customisation"
fi
