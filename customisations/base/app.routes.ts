import { Routes } from '@angular/router';

import { HomeComponent } from '../home/index';

export const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'admin', loadChildren: 'admin/admin.module#AdminModule' }
];
