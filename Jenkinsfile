#!groovy
node {
  stage 'Build'
    echo 'Getting code...'
    checkout scm
    cleanBuild = sh(returnStdout: true, script: 'git log -n 1').contains('CLEANBUILD')
    noDeploy = sh(returnStdout: true, script: 'git log -n 1').contains('NODEPLOY')

    echo 'Installing Node packages...'
    if (cleanBuild) {
      sh 'rm -Rf node_modules typings'
    }
    sh 'npm install'
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'build', buildName: 'Build')
    try {
      echo 'Building...'
      sh 'npm run build'
      bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'build', buildName: 'Build')
    } catch(Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'build', buildName: 'Build')
      throw e
    }
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'linter', buildName: 'Linter')
    try {
      echo 'Running linter...'
      sh 'npm run lint'
      bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'linter', buildName: 'Linter')
    } catch(Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'linter', buildName: 'Linter')
      throw e
    }
  stage 'Test'
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'test', buildName: 'Test')
    try {
        echo 'Running tests...'
        sh 'npm run ci:test'
        bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'test', buildName: 'Test')
    } catch(Exception e) {
        bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'test', buildName: 'Test')
        throw e
    }
  if (!noDeploy) {
    stage 'Deploy'
      echo 'Building prod version (Perù)...'
      sh './apply_customisation.sh peru'
      sh 'npm run build:aot:es'
      echo 'Deploying to dev server (Perù)...'
      sh 'tar cfj - -C dist . | ssh yago@yago.gnucoop.io "tar xfj - -C frontend/"'
      echo 'Building prod version (Haiti Sud Est)...'
      sh './apply_customisation.sh haiti_sud_est'
      sh 'npm run build:aot:fr'
      echo 'Deploying to dev server (Haiti Sud Est)...'
      sh 'tar cfj - -C dist . | ssh haiti-sud-est@yago.gnucoop.io "tar xfj - -C frontend/"'
      echo 'Building prod version (Guatemala)...'
      sh './apply_customisation.sh guatemala'
      sh 'npm run build:aot:gt'
      echo 'Deploying to dev server (Guatemala)...'
      sh 'tar cfj - -C dist . | ssh guatemala@yago.gnucoop.io "tar xfj - -C frontend/"'
      echo 'Building prod version (Haiti Ouest)...'
      sh './apply_customisation.sh haiti_ouest'
      sh 'npm run build:aot:hto'
      echo 'Deploying to dev server (Haiti Ouest)...'
      sh 'tar cfj - -C dist . | ssh haiti-ouest@yago.gnucoop.io "tar xfj - -C frontend/"'
  }
  stage 'Publish docs'
    echo 'Creating docs..'
    sh 'npm run docs'
    echo 'Publishing docs..'
    sh 'tar cfj - -C doc . | ssh docs.gnucoop.io "tar xfj - -C yagoweb/"'
}
