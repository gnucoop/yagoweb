import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FlexLayoutModule } from '@angular/flex-layout';

import { YagoWebCommonModule } from '../common/index';
import { LayersBrowserItemComponent } from './layers-browser-item';
import { LayersBrowserComponent } from './layers-browser';
import { LayersBrowserDialogComponent } from './layers-browser-dialog';
import { LayerStyleEditorComponent } from './layer-style-editor';
import { LayerStyleEditorDialogComponent } from './layer-style-editor-dialog';
import { LayerTransformComponent } from './layer-transform';
import { LayerTransformDialogComponent } from './layer-transform-dialog';
import { MapsListComponent } from './maps-list';
import { MapsListDialogComponent } from './maps-list-dialog';
import { MapLoadFailureComponent } from './map-load-failure';
import { MapLoadSuccessComponent } from './map-load-success';
import { MapSaveFailureComponent } from './map-save-failure';
import { MapSaveSuccessComponent } from './map-save-success';
import { MapDeleteDialogComponent } from './map-delete-dialog';
import { MapBuilderComponent } from './map-builder';
import { MAP_BUILDER_ROUTES } from './map-builder.routes';


@NgModule({
  declarations: [
    LayersBrowserItemComponent,
    LayersBrowserComponent,
    LayersBrowserDialogComponent,
    LayerStyleEditorComponent,
    LayerStyleEditorDialogComponent,
    LayerTransformComponent,
    LayerTransformDialogComponent,
    MapsListComponent,
    MapsListDialogComponent,
    MapLoadFailureComponent,
    MapLoadSuccessComponent,
    MapSaveFailureComponent,
    MapSaveSuccessComponent,
    MapDeleteDialogComponent,
    MapBuilderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(MAP_BUILDER_ROUTES),

    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,

    FlexLayoutModule,
    YagoWebCommonModule
  ],
  entryComponents: [
    LayersBrowserDialogComponent,
    LayerStyleEditorDialogComponent,
    LayerTransformDialogComponent,
    MapsListDialogComponent,
    MapsListDialogComponent,
    MapLoadFailureComponent,
    MapLoadSuccessComponent,
    MapSaveFailureComponent,
    MapSaveSuccessComponent,
    MapDeleteDialogComponent
  ]
})
export class MapBuilderModule { }
