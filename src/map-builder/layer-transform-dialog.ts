import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import { MapBuilderService, State } from '@yago/core';


@Component({
  selector: 'yago-layer-transform-dialog',
  templateUrl: './layer-transform-dialog.html',
  styleUrls: ['./layer-transform-dialog.scss']
})
export class LayerTransformDialogComponent {
  private _canAdd: Observable<boolean>;
  get canAdd(): Observable<boolean> { return this._canAdd; }

  constructor(
    public dialogRef: MatDialogRef<LayerTransformDialogComponent>,
    private _store: Store<State>,
    private _service: MapBuilderService
  ) { }

  close(): void {
    this._service.hideTransformOptions();
  }
}
