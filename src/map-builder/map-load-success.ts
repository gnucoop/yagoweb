import { Component } from '@angular/core';


@Component({
  template: '<ng-container i18n>Map loaded!</ng-container>',
  styles: [':host { color: #ffffff; }']
})
export class MapLoadSuccessComponent { }
