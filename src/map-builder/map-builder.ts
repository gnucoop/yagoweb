import { Component, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatMenu, MatSnackBar } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/withLatestFrom';

import {
  getLayersTree, getSelectedLayer, getStyleEditedLayer,
  getLayersBrowserVisible, getTransformOptionsVisibile,
  ILayerTreeItem, IMapLayer,
  AuthService, LayersBrowserService, MapBaseLayer, MapImageFormat, MapService,
  IMap,
  GeoStatisticTransform, MapBuilderService
} from '@yago/core';

import { environment } from '../environments/environment';
import { LoadingDialogComponent } from '../common/index';
import { LayersBrowserDialogComponent } from './layers-browser-dialog';
import { LayerStyleEditorDialogComponent } from './layer-style-editor-dialog';
import { LayerTransformDialogComponent } from './layer-transform-dialog';
import { MapsListDialogComponent } from './maps-list-dialog';
import { MapLoadFailureComponent } from './map-load-failure';
import { MapLoadSuccessComponent } from './map-load-success';
import { MapSaveFailureComponent } from './map-save-failure';
import { MapSaveSuccessComponent } from './map-save-success';


@Component({
  selector: 'yago-map-builder',
  templateUrl: './map-builder.html',
  styleUrls: ['./map-builder.scss']
})
export class MapBuilderComponent implements OnDestroy {
  @ViewChild('geostatsMenu') geostatsMenu: MatMenu;
  @ViewChild('saveImageMenu') saveImageMenu: MatMenu;
  @ViewChild('baseLayerMenu') baseLayerMenu: MatMenu;

  showViewControl = true;

  private _map: IMap;
  get map(): IMap { return this._map; }

  private _loggedIn: Observable<boolean>;
  get loggedIn(): Observable<boolean> { return this._loggedIn; }

  private _layersBrowserDisabled: Observable<boolean>;
  get layersBrowserDisabled(): Observable<boolean> { return this._layersBrowserDisabled; }

  private _vectorLayerFunctionsDisabled: Observable<boolean>;
  get vectorLayerFunctionsDisabled(): Observable<boolean> { return this._vectorLayerFunctionsDisabled; }

  private _geostatsTransforms: (typeof GeoStatisticTransform)[];
  get geostatsTransforms(): (typeof GeoStatisticTransform)[] { return this._geostatsTransforms; }

  private _measureControlVisible: Observable<boolean>;
  get measureControlVisible(): Observable<boolean> { return this._measureControlVisible; }

  private _elevationProfileControlVisible: Observable<boolean>;
  get elevationProfileControlVisible(): Observable<boolean> { return this._elevationProfileControlVisible; }

  private _spatialAnalysisControlVisible: Observable<boolean>;
  get spatialAnalysisControlVisible(): Observable<boolean> { return this._spatialAnalysisControlVisible; }

  private _editedMapName: Observable<string | null>;
  get editedMapName(): Observable<string | null> { return this._editedMapName; }

  private _saveImageDisabled: Observable<boolean>;
  get saveImageDisabled(): Observable<boolean> { return this._saveImageDisabled; }

  private _printDisabled: Observable<boolean>;
  get printDisabled(): Observable<boolean> { return this._printDisabled; }

  private _boundingBox: number[];
  get boundingBox(): number[] { return this._boundingBox; }

  private _limits: number[];
  get limits(): number[] { return this._limits; }

  private _layerBrowserDialogRef: MatDialogRef<LayersBrowserDialogComponent> | null;
  private _layerStyleEditorDialogRef: MatDialogRef<LayerStyleEditorDialogComponent> | null;
  private _transformDialogRef: MatDialogRef<LayerTransformDialogComponent> | null;
  private _mapsListDialogRef: MatDialogRef<MapsListDialogComponent> | null;
  private _layersTree: Observable<ILayerTreeItem[]>;
  private _selectedLayer: Observable<IMapLayer | null>;
  private _editLayerStyleEvent: EventEmitter<void> = new EventEmitter<void>();
  private _editedMapNameChanged: EventEmitter<string> = new EventEmitter<string>();
  private _loadingDialogRef: MatDialogRef<LoadingDialogComponent> | null;

  private _layersBrowserVisibleSubscription: Subscription;
  private _layersBrowserDialogSubscription: Subscription | null;
  private _transformDialogVisibleSubscription: Subscription;
  private _transformDialogSubscription: Subscription | null;
  private _editLayerStyleSubscription: Subscription;
  private _editedLayerStyleSubscription: Subscription;
  private _mapsListSubscription: Subscription;
  private _editedMapNameSubscription: Subscription;
  private _mapSavingSubscription: Subscription;
  private _mapLoadingSubscription: Subscription;

  constructor(
    private _service: MapBuilderService,
    private _authService: AuthService,
    private _layersBrowserService: LayersBrowserService,
    private _mapService: MapService,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {
    _service.init();
    this._geostatsTransforms = _service.geostatsTransforms;
    this._layersTree = _layersBrowserService.getLayersTree();
    this._layersBrowserDisabled = this._layersTree.map(t => t == null || t.length === 0);
    this._map = {baseLayer: 'osm', layers: []};
    this._selectedLayer = _mapService.getSelectedLayer();
    this._measureControlVisible = _service.getMeasureControlVisible();
    this._elevationProfileControlVisible = _service.getElevationProfileControlVisible();
    this._spatialAnalysisControlVisible = _service.getSpatialAnalysisControlVisible();
    this._loggedIn = _authService.getLoggedIn();
    this._editedMapName = _service.getEditedMapName();

    if (environment.mapsConfig.boundingBox != null) {
      try {
        this._boundingBox = environment.mapsConfig.boundingBox
          .split(',')
          .map(x => parseFloat(x));
      } catch (e) { }
    }

    if (environment.mapsConfig.limits != null) {
      try {
        this._limits = environment.mapsConfig.limits
          .split(',')
          .map(x => parseFloat(x));
      } catch (e) { }
    }

    this._vectorLayerFunctionsDisabled = this._selectedLayer
      .map((l) => l == null || l.layerType !== 'VECTOR');

    this._editLayerStyleSubscription = this._editLayerStyleEvent
      .withLatestFrom(this._selectedLayer)
      .subscribe((r: [void, IMapLayer]) => {
        const layer = r[1];
        if (layer.layerType === 'VECTOR') {
          this._service.editLayerStyle(layer);
        }
      });

    this._editedLayerStyleSubscription = _service.getStyleEditedLayer()
      .subscribe((layer: IMapLayer) => {
        if (layer == null) {
          this._hideLayerStyleEditor();
        } else {
          this._showLayerStyleEditor();
        }
      });

    this._layersBrowserVisibleSubscription = _layersBrowserService.getVisible()
      .subscribe((visible: boolean) => {
        if (visible) {
          this._createLayersBrowser();
        } else {
          this._destroyLayersBrowser();
        }
      });

    this._transformDialogVisibleSubscription = _service.getTransformOptionsVisible()
      .subscribe((visible: boolean) => {
        if (visible) {
          this._createTransformOptions();
        } else {
          this._destroyTransformOptions();
        }
      });

    this._mapsListSubscription = _service.getMapsListVisible()
      .subscribe((visible: boolean) => {
        if (visible) {
          this._showMapsList();
        } else {
          this._hideMapsList();
        }
      });

    this._editedMapNameSubscription = this._editedMapNameChanged
      .distinctUntilChanged((n1, n2) => n1 === n2)
      .subscribe(name => this._service.setEditedMapName(name));

    this._saveImageDisabled = this._mapService.getSaveImageRequest().map((r) => r != null);
    this._printDisabled = this._mapService.getPrint()
      .combineLatest(this._mapService.getLayers())
      .map((r: [boolean, IMapLayer[]]) => {
        const printing = r[0];
        const layers = r[1];
        return printing || (layers == null || layers.length === 0);
      });

    this._mapLoadingSubscription = this._service.getMapLoading()
      .withLatestFrom(this._service.getError())
      .subscribe((r: [boolean, any]) => {
        const loading = r[0];
        const error = r[1];
        if (loading) {
          if (this._loadingDialogRef != null) { this._loadingDialogRef.close(); }
          this._loadingDialogRef = this._dialog.open(LoadingDialogComponent);
        } else {
          if (this._loadingDialogRef != null) {
            this._loadingDialogRef.close();
            this._loadingDialogRef = null;
            this._snackBar.openFromComponent(
              error == null ? MapLoadSuccessComponent : MapLoadFailureComponent,
              { duration: 2000 }
            );
          }
        }
      });

    this._mapSavingSubscription = this._service.getMapSaving()
      .withLatestFrom(this._service.getError())
      .subscribe((r: [boolean, any]) => {
        const loading = r[0];
        const error = r[1];
        if (loading) {
          if (this._loadingDialogRef != null) { this._loadingDialogRef.close(); }
          this._loadingDialogRef = this._dialog.open(LoadingDialogComponent);
        } else {
          if (this._loadingDialogRef != null) {
            this._loadingDialogRef.close();
            this._loadingDialogRef = null;
            this._snackBar.openFromComponent(
              error == null ? MapSaveSuccessComponent : MapSaveFailureComponent,
              { duration: 2000 }
            );
          }
        }
      });
  }

  ngOnDestroy(): void {
    if (this._editLayerStyleSubscription != null) { this._editLayerStyleSubscription.unsubscribe(); }
    if (this._editedLayerStyleSubscription != null) { this._editedLayerStyleSubscription.unsubscribe(); }
    if (this._layersBrowserDialogSubscription != null) { this._layersBrowserDialogSubscription.unsubscribe(); }
    if (this._layersBrowserVisibleSubscription != null) { this._layersBrowserVisibleSubscription.unsubscribe(); }
    if (this._transformDialogVisibleSubscription != null) { this._transformDialogVisibleSubscription.unsubscribe(); }
    if (this._transformDialogSubscription != null) { this._transformDialogSubscription.unsubscribe(); }
    if (this._mapsListSubscription != null) { this._mapsListSubscription.unsubscribe(); }
    if (this._editedMapNameSubscription != null) { this._editedMapNameSubscription.unsubscribe(); }
    if (this._mapLoadingSubscription != null) { this._mapLoadingSubscription.unsubscribe(); }
    if (this._mapSavingSubscription != null) { this._mapSavingSubscription.unsubscribe(); }
  }

  openSpatialAnalysis(): void {
    this._service.openSpatialAnalysisControl();
  }

  onEditedMapNameChange(name: string): void {
    this._editedMapNameChanged.emit(name);
  }

  editLayerStyle(): void {
    this._editLayerStyleEvent.emit();
  }

  showLayersBrowser(): void {
    this._service.showLayersBrowser();
  }

  openTransformDialog(transform: typeof GeoStatisticTransform): void {
    this._service.showTransformOptions(transform);
  }

  toggleMeasureControl(): void {
    this._service.toggleMeasureControl();
  }

  toggleElevationProfileControl(): void {
    this._service.toggleElevationProfileControl();
  }

  openMapsList(): void {
    this._service.toggleMapsList();
  }

  saveMap(): void {
    this._service.saveCurrentMap();
  }

  saveImage(format: MapImageFormat): void {
    this._mapService.saveImage(format);
  }

  printMap(): void {
    this._mapService.print();
  }

  setBaseLayer(baseLayer: MapBaseLayer): void {
    this._mapService.setBaseLayer(baseLayer);
  }

  translate(str: string): string {
    return environment.labels[str] || str;
  }

  private _createTransformOptions(): void {
    if (this._transformDialogRef != null) {
      this._destroyTransformOptions();
    }

    this._transformDialogRef = this._dialog.open(LayerTransformDialogComponent, {
      disableClose: true
    });
    this._transformDialogSubscription = this._transformDialogRef.afterClosed()
      .subscribe((res) => {
        this._transformDialogRef = null;
        if (this._transformDialogSubscription != null) {
          this._transformDialogSubscription.unsubscribe();
        }
      });
  }

  private _destroyTransformOptions(): void {
    if (this._transformDialogRef != null) {
      this._transformDialogRef.close();
      this._transformDialogRef = null;
    }
    if (this._transformDialogSubscription != null) {
      this._transformDialogSubscription.unsubscribe();
      this._transformDialogSubscription = null;
    }
  }

  private _createLayersBrowser(): void {
    if (this._layerBrowserDialogRef != null) {
      this._destroyLayersBrowser();
    }

    this._layerBrowserDialogRef = this._dialog.open(LayersBrowserDialogComponent, {
      disableClose: true,
      width: '50%',
      height: '500px'
    });
    this._layersBrowserDialogSubscription = this._layerBrowserDialogRef.afterClosed()
      .subscribe(res => {
        this._layerBrowserDialogRef = null;
        if (this._layersBrowserDialogSubscription != null) {
          this._layersBrowserDialogSubscription.unsubscribe();
        }
      });
  }

  private _destroyLayersBrowser(): void {
    if (this._layerBrowserDialogRef != null) {
      this._layerBrowserDialogRef.close();
      this._layerBrowserDialogRef = null;
    }
    if (this._layersBrowserDialogSubscription != null) {
      this._layersBrowserDialogSubscription.unsubscribe();
      this._layersBrowserDialogSubscription = null;
    }
  }

  private _hideLayerStyleEditor(): void {
    if (this._layerStyleEditorDialogRef != null) {
      this._layerStyleEditorDialogRef.close();
      this._layerStyleEditorDialogRef = null;
    }
  }

  private _showLayerStyleEditor(): void {
    if (this._layerStyleEditorDialogRef != null) {
      this._hideLayerStyleEditor();
    }
    this._layerStyleEditorDialogRef = this._dialog.open(LayerStyleEditorDialogComponent, {
      disableClose: true
    });
  }

  private _showMapsList(): void {
    if (this._mapsListDialogRef != null) {
      this._hideMapsList();
    }
    this._mapsListDialogRef = this._dialog.open(MapsListDialogComponent, {
      disableClose: true
    });
  }

  private _hideMapsList(): void {
    if (this._mapsListDialogRef != null) {
      this._mapsListDialogRef.close();
      this._mapsListDialogRef = null;
    }
  }
}
