import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Store } from '@ngrx/store';

import { getLayersBrowserSelected, MapBuilderService, State } from '@yago/core';


@Component({
  selector: 'yago-layers-browser-dialog',
  templateUrl: './layers-browser-dialog.html',
  styleUrls: ['./layers-browser-dialog.scss']
})
export class LayersBrowserDialogComponent {
  private _canAdd: Observable<boolean>;
  get canAdd(): Observable<boolean> { return this._canAdd; }

  constructor(
    public dialogRef: MatDialogRef<LayersBrowserDialogComponent>,
    private _store: Store<State>,
    private _service: MapBuilderService
  ) {
    this._canAdd = this._store.select(getLayersBrowserSelected)
      .map((selected: number[]) => selected != null && selected.length > 0);
  }

  addLayers(): void {
    this._service.addSelectedLayers();
    this._service.hideLayersBrowser();
  }

  close(): void {
    this._service.hideLayersBrowser();
  }
}
