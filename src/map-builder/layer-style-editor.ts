import { Component, EventEmitter, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';

import { Store } from '@ngrx/store';

import * as Color from 'color';

import {
  ColorParam,
  getStyleEditedLayer,
  guessGeometryTypes,
  IMapLayer, IMapLayerStyle, MapLayerLineCap, MapLayerLineJoin, MapLayerPointType,
  MapBuilderService,
  State
} from '@yago/core';


const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


@Component({
  selector: 'yago-layer-style-editor',
  templateUrl: './layer-style-editor.html',
  styleUrls: ['./layer-style-editor.scss']
})
export class LayerStyleEditorComponent implements OnDestroy {
  private _layer: Observable<IMapLayer>;
  get layer(): Observable<IMapLayer> { return this._layer; }

  private _featureTypes: Observable<string[]>;
  private _hasPoints: Observable<boolean>;
  get hasPoints(): Observable<boolean> { return this._hasPoints; }
  private _hasLines: Observable<boolean>;
  get hasLines(): Observable<boolean> { return this._hasLines; }
  private _hasPolygons: Observable<boolean>;
  get hasPolygons(): Observable<boolean> { return this._hasPolygons; }

  private _saveStyleEvent: EventEmitter<void> = new EventEmitter<void>();
  private _layerSub: Subscription;
  private _saveSub: Subscription;

  strokeEnabled = true;
  strokeColor = '#ff0000';
  strokeOpacity = 1;
  strokeWeight = 1;
  fillEnabled = true;
  fillColor = '#ff0000';
  fillOpacity = 1;
  lineCap: MapLayerLineCap = 'round';
  lineJoin: MapLayerLineJoin = 'round';
  pointType: MapLayerPointType = 'circle';
  radius = 10;

  get previewBorderColor(): string {
    return colorConstructor(this.strokeColor).alpha(this.strokeOpacity).string();
  }
  get previewBorderWidth(): string {
    return `${this.strokeWeight}px`;
  }
  get previewBackgroundColor(): string {
    return colorConstructor(this.fillColor).alpha(this.fillOpacity).string();
  }

  constructor(
    private _store: Store<State>,
    private _service: MapBuilderService
  ) {
    this._layer = _store.select(getStyleEditedLayer);
    this._featureTypes = this._layer
      .filter(l => l != null && l.features != null)
      .map(l => guessGeometryTypes(<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>l.features));
    this._hasPoints = this._featureTypes
      .map(t => t.indexOf('Point') > -1 || t.indexOf('MultiPoint') > -1);
    this._hasLines = this._featureTypes
      .map(t => t.indexOf('LineString') > -1 || t.indexOf('MultiLineString') > -1);
    this._hasPolygons = this._featureTypes
      .map(t => t.indexOf('Polygon') > -1 || t.indexOf('MultiPolygon') > -1);

    this._saveSub = this._saveStyleEvent
      .withLatestFrom(this._layer.filter(l => l != null))
      .subscribe((r: [void, IMapLayer]) => {
        const layer = r[1];
        this._service.saveLayerStyle(layer, {
          stroke: this.strokeEnabled,
          color: this.strokeColor,
          opacity: this.strokeOpacity,
          weight: this.strokeWeight,
          lineCap: this.lineCap,
          lineJoin: this.lineJoin,
          fill: this.fillEnabled,
          fillColor: this.fillColor,
          fillOpacity: this.fillOpacity,
          pointType: this.pointType,
          radius: this.radius
        });
      });

    this._layerSub = this._layer
      .subscribe(l => {
        if (l != null && l.style != null && (<any>l.style).prop == null) {
          const style = <IMapLayerStyle>l.style;
          this.strokeEnabled = style.stroke;
          this.strokeColor = style.color;
          this.strokeOpacity = style.opacity;
          this.strokeWeight = style.weight;
          this.lineCap = style.lineCap;
          this.lineJoin = style.lineJoin;
          this.fillEnabled = style.fill;
          this.fillColor = style.fillColor;
          this.fillOpacity = style.fillOpacity;
          this.pointType = style.pointType;
          this.radius = style.radius;
        }
      });
  }

  ngOnDestroy(): void {
    if (this._saveSub != null) { this._saveSub.unsubscribe(); }
    if (this._layerSub != null) { this._layerSub.unsubscribe(); }
  }

  saveStyle(): void {
    this._saveStyleEvent.emit();
  }

  cancelEdit(): void {
    this._service.cancelEditLayerStyle();
  }
}
