import { Component, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { LayerStyleEditorComponent } from './layer-style-editor';


@Component({
  selector: 'yago-layer-style-editor-dialog',
  templateUrl: './layer-style-editor-dialog.html',
  styleUrls: ['./layer-style-editor-dialog.scss']
})
export class LayerStyleEditorDialogComponent {
  @ViewChild(LayerStyleEditorComponent) layerStyle: LayerStyleEditorComponent;

  constructor(
    public dialogRef: MatDialogRef<LayerStyleEditorDialogComponent>
  ) { }

  saveStyle(): void {
    this.layerStyle.saveStyle();
  }

  cancel(): void {
    this.layerStyle.cancelEdit();
  }
}
