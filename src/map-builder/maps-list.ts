import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IPersistentMap, MapBuilderService, MapsListService } from '@yago/core';

import { MapDeleteDialogComponent } from './map-delete-dialog';


@Component({
  selector: 'yago-maps-list',
  templateUrl: './maps-list.html',
  styleUrls: ['./maps-list.scss']
})
export class MapsListComponent {
  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _maps: Observable<IPersistentMap[]>;
  get maps(): Observable<IPersistentMap[]> { return this._maps; }

  private _selectedMap: Observable<IPersistentMap | null>;
  get selectedMap(): Observable<IPersistentMap | null> { return this._selectedMap; }

  private _deleteDialog: MatDialogRef<MapDeleteDialogComponent> | null;
  private _deleteDialogSub: Subscription | null;

  constructor(
    private _service: MapsListService,
    private _mapBuilderService: MapBuilderService,
    private _dialog: MatDialog
  ) {
    this._loading = _service.getLoading();
    this._maps = _service.getMaps();
    this._selectedMap = _service.getSelectedMap();

    _service.listMaps();
  }

  selectMap(map: IPersistentMap): void {
    this._service.selectMap(map);
  }

  deleteMap(map: IPersistentMap): void {
    if (map.id == null) { return; }
    this._deleteDialog = this._dialog.open(MapDeleteDialogComponent);
    this._deleteDialogSub = this._deleteDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._mapBuilderService.deleteMap(<number>map.id);
      });
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteDialog == null) { return; }
    if (this._deleteDialogSub != null) { this._deleteDialogSub.unsubscribe(); }
    this._deleteDialog.close();
    this._deleteDialog = null;
    this._deleteDialogSub = null;
  }
}
