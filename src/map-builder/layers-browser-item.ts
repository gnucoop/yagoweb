import {
  Component, EventEmitter, Input, Output,
  animate, state, style, transition, trigger
} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';

import { ILayer, ILayerTreeItem, LayersBrowserService } from '@yago/core';


@Component({
  selector: 'yago-layers-browser-item',
  templateUrl: 'layers-browser-item.html',
  styleUrls: ['layers-browser-item.scss'],
  animations: [
    trigger('expandedState', [
      state('expanded', style({height: '*'})),
      state('collapsed', style({height: 0})),
      transition('collapsed => expanded', [
        style({height: '0'}),
        animate(100, style({height: '*'}))
      ]),
      transition('expanded => collapsed', [
        style({height: '*'}),
        animate(100, style({height: 0}))
      ]),
    ])
  ]
})
export class LayersBrowserItemComponent {
  private _expanded: Observable<boolean>;
  get expanded(): Observable<boolean> { return this._expanded; }

  private _expandedStateString: Observable<string>;
  get expandedStateString(): Observable<string> { return this._expandedStateString; }

  private _isFiltered: Observable<boolean>;
  get isFiltered(): Observable<boolean> { return this._isFiltered; }

  private _filteredItems: Observable<number[]>;
  get filteredItems(): Observable<number[]> { return this._filteredItems; }

  private _filteredLayers: Observable<number[]>;
  get filteredLayers(): Observable<number[]> { return this._filteredLayers; }

  private _selected: Observable<number[]>;

  @Input()
  item: ILayerTreeItem;

  constructor(private _service: LayersBrowserService) {
    this._expanded = defer(() => {
      return _service.getExpanded()
        .combineLatest(this.isFiltered)
        .map((r: [number[], boolean]) => r[1] || (this.item != null && r[0].indexOf(this.item.theme.id) > -1));
    });
    this._expandedStateString = this._expanded
      .map((expanded: boolean) => expanded ? 'expanded' : 'collapsed')
      .startWith('collapsed');
    this._selected = _service.getSelected();
    this._isFiltered = _service.getIsFiltered();
    this._filteredItems = _service.getFilteredItems();
    this._filteredLayers = _service.getFilteredLayers();
  }

  layerSelected(layer: ILayer): Observable<boolean> {
    return this._selected.map((selected: number[]) => layer.id != null && selected.indexOf(layer.id) > -1);
  }

  toggleExpanded(): void {
    this._service.toggleItem(this.item);
  }

  toggleLayer(layer: ILayer): void {
    this._service.toggleLayer(layer);
  }

  isItemVisible(item: ILayerTreeItem): Observable<boolean> {
    return this._isFiltered
      .combineLatest(this._filteredItems)
      .map((r: [boolean, number[]]) => !r[0] || r[1].indexOf(item.theme.id) > -1);
  }

  isLayerVisible(layer: ILayer): Observable<boolean> {
    return this._isFiltered
      .combineLatest(this._filteredLayers)
      .map((r: [boolean, number[]]) => !r[0] || (layer.id != null && r[1].indexOf(layer.id) > -1));
  }
}
