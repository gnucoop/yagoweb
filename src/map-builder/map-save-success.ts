import { Component } from '@angular/core';


@Component({
  template: '<ng-container i18n>Map saved!</ng-container>',
  styles: [':host { color: #ffffff; }']
})
export class MapSaveSuccessComponent { }
