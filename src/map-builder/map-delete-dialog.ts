import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-map-delete-dialog',
  templateUrl: './map-delete-dialog.html',
  styleUrls: ['./map-delete-dialog.scss']
})
export class MapDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<MapDeleteDialogComponent>) { }
}
