import { Component } from '@angular/core';


@Component({
  template: '<ng-container i18n>There was a problem saving your map. Please try again.</ng-container>',
  styles: [':host { color: #ffffff; }']
})
export class MapSaveFailureComponent { }
