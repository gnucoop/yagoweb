import { Routes } from '@angular/router';

import { MapBuilderComponent } from './map-builder';


export const MAP_BUILDER_ROUTES: Routes = [
  { path: 'mapBuilder', component: MapBuilderComponent }
];
