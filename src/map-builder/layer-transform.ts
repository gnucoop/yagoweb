import { Component, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';

import {
  getCurrentTransform, getSelectedLayer,
  GeoStatisticTransform, GeoStatisticTransformOption,
  IMapLayer, MapBuilderService, State
} from '@yago/core';

import { environment } from '../environments/environment';


@Component({
  selector: 'yago-layer-transform',
  templateUrl: './layer-transform.html',
  styleUrls: ['./layer-transform.scss']
})
export class LayerTransformComponent implements OnDestroy {
  private _transform: Observable<typeof GeoStatisticTransform>;
  get transform(): Observable<typeof GeoStatisticTransform> { return this._transform; }

  private _layer: Observable<IMapLayer>;
  get layer(): Observable<IMapLayer> { return this._layer; }

  private _options: Observable<GeoStatisticTransformOption[]>;
  get options(): Observable<GeoStatisticTransformOption[]> { return this._options; }

  private _form: Observable<FormGroup>;
  get form(): Observable<FormGroup> { return this._form; }

  private _valid: Observable<boolean>;
  get valid(): Observable<boolean> { return this._valid; }

  private _addLayerEvent: EventEmitter<void> = new EventEmitter<void>();
  private _addLayerSubscription: Subscription;
  private _formUpdateSubscription: Subscription;

  constructor(
    private _service: MapBuilderService,
    private _store: Store<State>,
    private _fb: FormBuilder
  ) {
    this._transform = _store.select(getCurrentTransform)
      .filter((idx: number) => idx != null && idx >= 0 && idx < _service.geostatsTransforms.length)
      .map((idx: number) => _service.geostatsTransforms[idx])
      .publishReplay(1)
      .refCount();

    this._layer = _store.select(getSelectedLayer);

    this._options = this.transform
      .withLatestFrom(this._layer)
      .publishReplay(1)
      .refCount()
      .map((r: [typeof GeoStatisticTransform, IMapLayer]) => r[0].getOptions(r[1]));

    this._form = this._options
      .map((options: GeoStatisticTransformOption[]) => {
        return this._fb.group(this._optionsToFormFields(options));
      })
      .publishReplay(1)
      .refCount();

    this._valid = this._form
      .switchMap((f: FormGroup) => f.valueChanges)
      .withLatestFrom(this._form)
      .map((r: [any, FormGroup]) => r[1].valid);

    this._formUpdateSubscription = this._form
      .switchMap((f: FormGroup) => f.valueChanges)
      .distinctUntilChanged(
        (v1: any, v2: any) => JSON.stringify(v1).localeCompare(JSON.stringify(v2)) === 0
      )
      .withLatestFrom(this._form, this._transform)
      .subscribe((r: [any, FormGroup, typeof GeoStatisticTransform]) => {
        const values = r[0];
        const form = r[1];
        const transform = r[2];
        if (form != null && transform != null) {
          transform.updateForm(form, values);
        }
      });

    this._addLayerSubscription = this._addLayerEvent
      .withLatestFrom(this._layer, this._transform, this._form)
      .subscribe((r: [void, IMapLayer, typeof GeoStatisticTransform, FormGroup]) => {
        const layer = r[1];
        const transform = r[2];
        const form = r[3];
        if (layer != null && transform != null && form != null && form.valid) {
          this._service.createGeoStatisticalLayer(layer, transform, form.value);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._addLayerSubscription != null) { this._addLayerSubscription.unsubscribe(); }
    if (this._formUpdateSubscription != null) { this._formUpdateSubscription.unsubscribe(); }
  }

  addLayer(): void {
    this._addLayerEvent.emit();
  }

  translate(str: string): string {
    return environment.labels[str] || str;
  }

  private _optionsToFormFields(options: GeoStatisticTransformOption[]): {[key: string]: any} {
    return options.map(this._optionToFormField)
      .reduce((map, obj) => {
        map[obj.name] = obj.field;
        return map;
      }, {});
  }
  private _optionToFormField(option: GeoStatisticTransformOption): {name: string, field: any} {
    const validators = <ValidatorFn[]>[];
    if (option.required) {
      validators.push(Validators.required);
    }
    return {name: option.name, field: [option.defaultValue || null, validators]};
  }
}
