import { Component, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/withLatestFrom';

import { IPersistentMap, MapBuilderService, MapsListService } from '@yago/core';


@Component({
  selector: 'yago-maps-list-dialog',
  templateUrl: './maps-list-dialog.html',
  styleUrls: ['./maps-list-dialog.scss']
})
export class MapsListDialogComponent {
  private _loadMapEvent: EventEmitter<void> = new EventEmitter<void>();
  private _loadMapSubscription: Subscription;

  private _selectedMap: Observable<IPersistentMap | null>;
  get selectedMap(): Observable<IPersistentMap | null> { return this._selectedMap; }

  constructor(
    public dialogRef: MatDialogRef<MapsListDialogComponent>,
    private _mapBuilderService: MapBuilderService,
    private _mapsListService: MapsListService
  ) {
    this._selectedMap = _mapsListService.getSelectedMap();

    this._loadMapSubscription = this._loadMapEvent
      .withLatestFrom(this._selectedMap)
      .filter((r: [void, IPersistentMap]) => r[1].id != null)
      .subscribe((r: [void, IPersistentMap]) => {
        const map: IPersistentMap = r[1];
        this._mapBuilderService.loadMap(<number>map.id);
      });
  }

  loadMap(): void {
    this._loadMapEvent.emit();
  }

  close(): void {
    this._mapBuilderService.toggleMapsList();
  }
}
