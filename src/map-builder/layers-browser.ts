import { Component, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';

import { ILayerTreeItem, LayersBrowserService } from '@yago/core';


@Component({
  selector: 'yago-layers-browser',
  templateUrl: './layers-browser.html',
  styleUrls: ['./layers-browser.scss']
})
export class LayersBrowserComponent implements OnDestroy {
  private _layersTree: Observable<ILayerTreeItem[]>;
  get layersTree(): Observable<ILayerTreeItem[]> { return this._layersTree; }

  private _selected: Observable<number>;
  get selected(): Observable<number> { return this._selected; }

  private _isFiltered: Observable<boolean>;
  get isFiltered(): Observable<boolean> { return this._isFiltered; }

  private _filteredItems: Observable<number[]>;
  get filteredItems(): Observable<number[]> { return this._filteredItems; }

  private _filterString: string | null;
  get filterString(): string | null { return this._filterString; }
  set filterString(filterString: string | null) {
    this._filterString = filterString;
    this._service.setFilter(filterString);
  }

  private _filterSub: Subscription;

  constructor(
    private _service: LayersBrowserService
  ) {
    this._layersTree = _service.getLayersTree();
    this._selected = _service.getSelected()
      .map((selected: number[]) => selected.length);
    this._filteredItems = _service.getFilteredItems();
    this._isFiltered = _service.getIsFiltered();

    this._filterSub = this._service.getFilter()
      .subscribe(f => this.filterString = f);
  }

  ngOnDestroy(): void {
    if (this._filterSub != null) { this._filterSub.unsubscribe(); }
  }

  isVisible(item: ILayerTreeItem): Observable<boolean> {
    return this._isFiltered
      .combineLatest(this._filteredItems)
      .map((r: [boolean, number[]]) => !r[0] || r[1].indexOf(item.theme.id) > -1);
  }
}
