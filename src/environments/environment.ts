// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  sentryUrl: 'https://d60fc1def029468d947007b74e61a020@sentry.gnucoop.com/8',
  apiConfig: {
    baseUrl: 'http://127.0.0.1:8000',
    arcgisSourcesEndpoint: 'layers/arcgis_sources',
    arcgisSourcesLayersSubpath: 'available_layers',
    arcgisSourcesLayerSubpath: 'available_layer',
    wmsSourcesEndpoint: 'layers/wms_sources',
    wmsSourcesLayersSubpath: 'available_layers',
    wmsSourcesLayerSubpath: 'available_layer',
    themesEndpoint: 'layers/themes',
    importShapefileEndpoint: 'layers/import_shapefile',
    layersEndpoint: 'layers/layers',
    layersShapefileSubpath: 'get_shapefile',
    loginEndpoint: 'accounts/api-token-auth',
    logoutEndpoint: 'accounts/api-token-invalidate',
    authRefreshEndpoint: 'accounts/api-token-refresh',
    tokenExpirationDelta: 300,
    meEndpoint: 'accounts/me',
    checkExistentUsername: 'accounts/username_exists',
    checkExistentEmail: 'accounts/email_exists',
    registerEndpoint: 'accounts/register',
    recoverPasswordEndpoint: 'accounts/recover_password',
    resetPasswordEndpoint: 'accounts/reset_password',
    mapsEndpoint: 'maps/maps',
    galleryThemesEndpoint: 'gallery/themes',
    galleryThemesByParentSubpath: 'by_parent',
    galleryItemsEndpoint: 'gallery/items',
    galleryItemsByThemeSubpath: 'by_theme',
    spatialAnalysisEndpoint: 'layers/spatial_analysis',
    usersEndpoint: 'accounts/users'
  },
  recaptchaSiteKey: '6LerJCsUAAAAAGp2jFEdwd-mUM8TaS085BGnQ_zM',
  authConfig: {
    tokenName: 'yago_jwt_token',
    headerPrefix: 'JWT'
  },
  mapsConfig: {
    googleApiKey: 'AIzaSyDMwMvMCoKhB2qoHB9ETk1zCaGUhAMUxPw',
    bingApiKey: 'AvzAYijqgnf0CJUwe09mXuPWIZBGlDx3aFIFww2s5g5Bz_wqBmjtZhWBykM3q8TS',
    boundingBox: '-12.0438233283, -77.0532222304, -12.0035190009, -77.0129949049',
    limits: '-18.3479753557, -81.4109425524, -0.0572054988649, -68.6650797187'
  },
  labels: {
    selected: 'Selected',
    notSelected: 'Not selected',
    Buffer: '',
    'Distance (m)': '',
    'Stroke': '',
    'Stroke color': '',
    'Stroke opacity': '',
    'Stroke width': '',
    'Fill': '',
    'Fill color': '',
    'Fill opacity': '',
    Choropleth: '',
    Indicator: '',
    'Number of classes': '',
    'Classes': '',
    'Start fill color': '',
    'End fill color': '',
    'Proportional symbols': '',
    'Minimum radius': '',
    'Maximum radius': ''
  }
};
