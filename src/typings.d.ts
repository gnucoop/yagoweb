// Typings reference file, you can add your own global typings here
// https://www.typescriptlang.org/docs/handbook/writing-declaration-files.html

/// <reference types="geojson" />

declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare namespace reproj {
  export function detectCrs(geojson: GeoJSON.GeoJsonObject, crs: GeoJSON.CoordinateReferenceSystem): GeoJSON.CoordinateReferenceSystem;
  export function reproject(
    geojson: GeoJSON.GeoJsonObject,
    from: string | GeoJSON.CoordinateReferenceSystem,
    to: string | GeoJSON.CoordinateReferenceSystem,
    projs?: {[projection: string]: string}
  ): GeoJSON.GeoJsonObject;
  export function reverse(geojson: GeoJSON.GeoJsonObject): GeoJSON.GeoJsonObject;
  export function toWgs84(
    geojson: GeoJSON.GeoJsonObject,
    from: string | GeoJSON.CoordinateReferenceSystem,
    projs?: {[projection: string]: string}
  );
}

declare module 'reproject' {
  export = reproj;
}
