import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DndModule } from 'ngx-dnd';

import {
  GradientPanelComponent, ValuesPanelComponent, PalettePanelComponent, HistoryPanelComponent,
  ColorPickerComponent, ColorPickerOverlayComponent, ColorPickerInputComponent
} from './color-picker/index';
import { LayerIconComponent } from './layer-icon';
import {
  MapLayersControlComponent,
  MapMeasureControlComponent,
  MapTableControlComponent,
  MapViewControlComponent,
  MapLegendIconComponent,
  MapLegendControlComponent,
  MapFeatureInfoDialogComponent,
  MapChangeLayerLabelDialogComponent,
  MapImageSaveErrorComponent,
  MapScaleControlComponent,
  MapElevationProfileControlComponent,
  MapSpatialAnalysisControlComponent
} from './map/index';
import { MapComponent } from './map';
import { MapDialogComponent } from './map-dialog';
import { LoadingDialogComponent } from './loading-dialog';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSliderModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,

    NgxDatatableModule,
    DndModule
  ],
  declarations: [
    GradientPanelComponent,
    ValuesPanelComponent,
    PalettePanelComponent,
    HistoryPanelComponent,
    ColorPickerComponent,
    ColorPickerOverlayComponent,
    ColorPickerInputComponent,
    LayerIconComponent,
    MapLayersControlComponent,
    MapMeasureControlComponent,
    MapTableControlComponent,
    MapLegendIconComponent,
    MapLegendControlComponent,
    MapViewControlComponent,
    MapFeatureInfoDialogComponent,
    MapChangeLayerLabelDialogComponent,
    MapImageSaveErrorComponent,
    MapScaleControlComponent,
    MapElevationProfileControlComponent,
    MapSpatialAnalysisControlComponent,
    MapComponent,
    MapDialogComponent,
    LoadingDialogComponent
  ],
  exports: [
    ColorPickerComponent,
    ColorPickerInputComponent,
    LayerIconComponent,
    MapComponent,
    MapDialogComponent,
    LoadingDialogComponent
  ],
  entryComponents: [
    ColorPickerOverlayComponent,
    MapDialogComponent,
    LoadingDialogComponent,
    MapFeatureInfoDialogComponent,
    MapChangeLayerLabelDialogComponent,
    MapImageSaveErrorComponent
  ]
})
export class YagoWebCommonModule { }
