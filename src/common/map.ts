import {
  AfterViewInit, Component, ContentChild, ElementRef, EventEmitter, Input,
  OnDestroy, OnInit, Renderer, ViewChild, ViewEncapsulation
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/withLatestFrom';

import { Actions } from '@ngrx/effects';

import * as L from 'leaflet';
import * as p4 from 'proj4';
import { reproject } from 'reproject';

const proj4: typeof p4 = (<any>p4).default || p4;

import { leafletImage } from './map/leaflet-image';
import 'leaflet.gridlayer.googlemutant/Leaflet.GoogleMutant';
import 'leaflet-plugins/layer/tile/Bing';

import * as html2canvas from 'html2canvas';

import { environment } from '../environments/environment';

import {
  calculateInverseHexColor, defaultMapStyle, crsNameToEpsg,
  ILayer, IMap, IMapLayer, IMapLayerStyle, IMapConditionalLayerStyle,
  IMapShowFeatureInfo,
  LayerTypes, MapCenter, MapImageFormat, MapService,
  MapSetMapAction, MapToggleLayerVisibilitySuccessAction,
  MapUpdateLayerStyleAction, MapSelectFeaturesSuccessAction,
  MapActionTypes, MapBaseLayer,
  State,
  Proj4jsProjections, IWMSSource,
  generateWMSUrl
} from '@yago/core';

import { LoadingDialogComponent } from './loading-dialog';
import {
  MapLegendControlComponent, MapFeatureInfoDialogComponent, MapImageSaveErrorComponent
} from './map/index';


@Component({
  selector: 'yago-map',
  templateUrl: './map.html',
  styleUrls: ['./map.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnDestroy, OnInit {
  @ViewChild('mapContainer') mapContainer: ElementRef;
  @ViewChild('downloadImageLink') downloadImageLink: ElementRef;
  @ViewChild(MapLegendControlComponent) legendControl: MapLegendControlComponent;

  private _hideViewControl: boolean;
  @Input()
  get hideViewControl(): boolean { return this._hideViewControl; }
  set hideViewControl(z: boolean) { this._hideViewControl = coerceBooleanProperty(z); }

  private _showLayersControl = true;
  @Input()
  get showLayersControl(): boolean { return this._showLayersControl; }
  set showLayersControl(z: boolean) { this._showLayersControl = coerceBooleanProperty(z); }

  private _showTableControl = false;
  @Input()
  get showTableControl(): boolean { return this._showTableControl; }
  set showTableControl(t: boolean) { this._showTableControl = coerceBooleanProperty(t); }

  private _showMeasureControl = false;
  @Input()
  get showMeasureControl(): boolean { return this._showMeasureControl; }
  set showMeasureControl(m: boolean) { this._showMeasureControl = coerceBooleanProperty(m); }

  private _showElevationProfileControl = false;
  @Input()
  get showElevationProfileControl(): boolean { return this._showElevationProfileControl; }
  set showElevationProfileControl(m: boolean) { this._showElevationProfileControl = coerceBooleanProperty(m); }

  private _showSpatialAnalysisControl = false;
  @Input()
  get showSpatialAnalysisControl(): boolean { return this._showSpatialAnalysisControl; }
  set showSpatialAnalysisControl(m: boolean) { this._showSpatialAnalysisControl = coerceBooleanProperty(m); }

  private _showLegendControl = false;
  @Input()
  get showLegendControl(): boolean { return this._showLegendControl; }
  set showLegendControl(t: boolean) { this._showLegendControl = coerceBooleanProperty(t); }

  private _showScaleControl = true;
  @Input()
  get showScaleControl(): boolean { return this._showScaleControl; }
  set showScaleControl(t: boolean) { this._showScaleControl = coerceBooleanProperty(t); }

  private _boundingBox: number[];
  @Input()
  set boundingBox(boundingBox: number[]) {
    this._boundingBox = boundingBox.slice(0);
    this._setMapBoundingBox();
  }

  private _limits: number[];
  @Input()
  set limits(limits: number[]) {
    if (limits == null) { return; }
    this._limits = limits.slice(0);
    this._setMapLimits();
  }

  private _map: IMap;
  @Input() get map(): IMap { return this._map; }
  set map(map: IMap) {
    this._map = map;
    this._updateMap();
  }

  private _baseLayer: Observable<MapBaseLayer>;
  private _baseLLayer: L.Layer;

  private _currentMap: Observable<IMap | null>;
  get currentMap(): Observable<IMap | null> { return this._currentMap; }

  private _lMap: L.Map | null;
  get lMap(): L.Map | null { return this._lMap; }

  private _currentLayers: {[id: number]: L.Layer} = {};
  private _currentFeatures: {[id: string]: L.Layer} = {};
  private _currentHighlights: string[] = [];

  private _targetCrs: string = <string>L.CRS.EPSG4326.code;
  private _layers: Observable<IMapLayer[]>;
  private _layersInit = false;
  private _defaultCrs: GeoJSON.CoordinateReferenceSystem = {
    type: 'EPSG',
    properties: {
      code: 4326
    }
  };

  private _mapCenterEvent: EventEmitter<MapCenter> = new EventEmitter<MapCenter>();

  private _featureInfoDialog: MatDialogRef<MapFeatureInfoDialogComponent> | null;

  private _mapSubscription: Subscription;
  private _layersSubscription: Subscription;
  private _zoomSubscription: Subscription;
  private _mapCenterSubscription: Subscription;
  private _mapCenterEventSubscription: Subscription;
  private _toggleLayerVisibilitySubscription: Subscription;
  private _updateLayerStyleSubscription: Subscription;
  private _saveImageSubscription: Subscription;
  private _printSubscription: Subscription;
  private _baseLayerSub: Subscription;
  private _featureInfoSub: Subscription;
  private _featureInfoDialogSub: Subscription;

  constructor(
    private _actions: Actions,
    private _service: MapService,
    private _renderer: Renderer,
    private _dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {
    this._layers = _service.getMapLayers();
    this._currentMap = _service.getMap();
    this._baseLayer = _service.getBaseLayer();

    this._toggleLayerVisibilitySubscription = _actions
      .ofType(MapActionTypes.TOGGLE_LAYER_VISIBILITY_SUCCESS)
      .withLatestFrom(this._layers)
      .subscribe((r: [MapToggleLayerVisibilitySuccessAction, IMapLayer[]]) => {
        const layerVisibilty = r[0].payload;
        const layers = r[1];
        this._toggleLayerVisibility(layerVisibilty.id, layerVisibilty.visibility);
        this._reorderLayers(layers);
      });

    this._updateLayerStyleSubscription = _actions
      .ofType(MapActionTypes.UPDATE_LAYER_STYLE)
      .subscribe((action: MapUpdateLayerStyleAction) =>
        this._updateLayerStyle(action.payload));

    this._actions
      .ofType(MapActionTypes.SELECT_FEATURES_SUCCESS)
      .subscribe((action: MapSelectFeaturesSuccessAction) => {
        action.payload.forEach((selection) => {
          const layerId = selection.layer;
          const layerPrefix = `l${layerId}_`;
          const layerPrefixLen = layerPrefix.length;
          Object.keys(this._currentFeatures).filter(k => k.startsWith(layerPrefix))
            .forEach((featureKey: string) => {
              const featureId = featureKey.substr(layerPrefixLen);
              const feature = <L.Path>this._currentFeatures[`${layerPrefix}${featureId}`];
              if (feature != null && feature.setStyle != null) {
                const opts = (<any>feature).options;
                if (selection.features.indexOf(featureId) === -1) {
                  const hi = this._currentHighlights.indexOf(featureKey);
                  if (hi > -1) {
                    this._currentHighlights.splice(hi, 1);
                    opts.color = calculateInverseHexColor(opts.color);
                    opts.fillColor = calculateInverseHexColor(opts.fillColor);
                    feature.setStyle(opts);
                  }
                } else {
                  if (this._currentHighlights.indexOf(featureKey) === -1) {
                    this._currentHighlights.push(featureKey);
                    feature.setStyle(Object.assign({}, opts, {
                      color: calculateInverseHexColor(opts.color),
                      fillColor: calculateInverseHexColor(opts.fillColor)
                    }));
                  }
                  feature.bringToFront();
                }
              }
            });
        });
      });

    this._saveImageSubscription = _service.getSaveImageRequest()
      .withLatestFrom(this._service.getLegendControlOpen())
      .subscribe((r: [MapImageFormat, boolean]) => {
        const fmt: MapImageFormat = r[0];
        const includeLegend: boolean = r[1];
        if (fmt == null) { return; }

        if (this._lMap == null || this.downloadImageLink == null) {
          _service.saveImageSuccess();
          return;
        }
        const loadingDialog: MatDialogRef<LoadingDialogComponent> = this._dialog
          .open(LoadingDialogComponent, {disableClose: true});
        _service.saveImageInProgress();
        leafletImage(this._lMap, (err, canvas) => {
          const scale = this.mapContainer.nativeElement.childNodes[1].childNodes[2].childNodes[0];
          const north = this.mapContainer.nativeElement.childNodes[1].childNodes[1].childNodes[0];
          html2canvas(scale)
            .then(scaleCanvas => {
              html2canvas(north)
                .then(northCanvas => {
                  if (includeLegend) {
                    html2canvas(this.legendControl.panel)
                      .then(legendCanvas => {
                        this._saveImage(
                          canvas, fmt, loadingDialog, scaleCanvas, northCanvas, legendCanvas
                        );
                      })
                      .catch(() => {
                        this._saveImageError(loadingDialog);
                      });
                  } else {
                    this._saveImage(canvas, fmt, loadingDialog, scaleCanvas, northCanvas);
                  }
                });
            });
        });
      });

    this._printSubscription = this._service.getPrint()
      .withLatestFrom(this._layers, this._service.getLegendControlOpen())
      .subscribe((r: [boolean, IMapLayer[], boolean]) => {
        const print = r[0];
        const layers = r[1];
        const printLegend = r[2];
        if (
          !print || layers == null || layers.length === 0 ||
          this.mapContainer == null || document == null
        ) { return; }

        const container = document.createElement('div');
        container.classList.add('yago-map-print-container');
        document.body.appendChild(container);
        container.innerHTML = this.mapContainer.nativeElement.innerHTML;
        if (printLegend && this.legendControl.panel != null) {
          const legendStyle: CSSStyleDeclaration = window != null ?
            window.getComputedStyle(this.legendControl.panel) :
            new CSSStyleDeclaration();
          const printLegendPanel = this.legendControl.panel.cloneNode(true);
          const addStyles = {
            position: 'absolute',
            right: '0',
            bottom: '0',
            'zIndex': '1000',
            'backgroundColor': '#ffffff',
            'height': 'auto'
          };
          const addStylesKeys = Object.keys(addStyles);
          Object.keys(legendStyle).filter(k => addStylesKeys.indexOf(k) === -1).forEach((k) => {
            const val = legendStyle[k];
            if (val != null) {
              this._renderer.setElementStyle(printLegendPanel, k, val);
            }
          });
          this._renderer.setElementStyle(printLegendPanel, 'position', 'absolute');
          this._renderer.setElementStyle(printLegendPanel, 'right', '0');
          this._renderer.setElementStyle(printLegendPanel, 'top', '0');
          this._renderer.setElementStyle(printLegendPanel, 'z-index', '1000');
          this._renderer.setElementStyle(printLegendPanel, 'padding', '10px');
          this._renderer.setElementStyle(printLegendPanel, 'backgroundColor', '#ffffff');
          this._renderer.setElementStyle(printLegendPanel, 'height', 'auto');
          container.appendChild(printLegendPanel);
        }
        const scale = this.mapContainer.nativeElement.childNodes[1].childNodes[2].childNodes[0];
        const sourceCanvas: HTMLCanvasElement = this.mapContainer.nativeElement
          .getElementsByTagName('canvas')[0];
        const destCanvas: HTMLCanvasElement = container.getElementsByTagName('canvas')[0];
        const ctx = <CanvasRenderingContext2D>destCanvas.getContext('2d');
        ctx.drawImage(sourceCanvas, 0, 0);
        window.print();
        container.innerHTML = '';
        container.remove();
        this._service.printComplete();
      });

    this._baseLayerSub = this._baseLayer
      .subscribe((b) => this._setBaseLayer(b));

    this._featureInfoSub = this._service
      .getShowFeatureInfo()
      .filter(fi => fi != null)
      .subscribe((fi: IMapShowFeatureInfo) => {
        if (this._featureInfoDialog != null) {
          this._featureInfoDialog.close();
          this._featureInfoDialog = null;
        }
        const padding = 48;
        const width = this.mapContainer.nativeElement.clientWidth;
        const height = this.mapContainer.nativeElement.clientHeight;
        this._featureInfoDialog = this._dialog.open(MapFeatureInfoDialogComponent, {
          position: {
            top: `${Math.min(height - 256, fi.y)}px`,
            left: `${Math.min(width - 256, fi.x)}px`
          }
        });
        this._featureInfoDialogSub = this._featureInfoDialog
          .afterClosed()
          .subscribe(() => {
            this._featureInfoDialog = null;
            this._service.hideFeatureInfo();
          });
      });
  }

  ngOnInit(): void {
    this._updateMap();
    this._initZoom();
    this._initMapCenter();

    if (this.mapContainer != null) {
      this._mapSubscription = this._currentMap
        .subscribe((map: IMap) => {
          this._destroyMap();

          this._lMap = L.map(this.mapContainer.nativeElement, {
            crs: L.CRS.EPSG3857,
            zoomControl: false,
            preferCanvas: true
          });
          L.control.scale().addTo(this._lMap);
          const north = new L.Control({position: 'topright'});
          north.onAdd = function(cmap) {
            const div = L.DomUtil.create('div', 'north_arrow');
            div.innerHTML = '<i class="yago-icons north"></i>';
            return div;
          };
          north.addTo(this._lMap);
          this._lMap.addEventListener('zoomend', this._updateZoomLevel, this);
          this._lMap.addEventListener('zoomend', this._updatePanOffset, this);
          this._lMap.addEventListener('dragend', this._updateMapCenter, this);
          this._setMapBoundingBox();
          this._setMapLimits();
          this._updateMapCenter();
          this._updatePanOffset();
          this._setBaseLayer(map != null && map.baseLayer ? map.baseLayer : 'osm');

          if (!this._layersInit) {
            this._layersInit = true;

            this._layersSubscription = this._layers
              .filter((layers: IMapLayer[]) => layers != null)
              .subscribe((layers: IMapLayer[]) => {
                if (this._lMap == null) { return; }
                let bounds: L.LatLngBounds | null = null;
                const currentLayersIds = Object.keys(this._currentLayers);
                layers.filter(l => currentLayersIds.indexOf(`${l.uniqueId}`) === -1)
                  .forEach(layer => {
                    const leafletLayer = this._mapLayerToLeaftlet(layer);
                    if (leafletLayer == null) { return; }
                    if (layer.visible) {
                      leafletLayer.addTo(<L.Map>this._lMap);
                    }
                    this._currentLayers[layer.uniqueId] = leafletLayer;

                    const crs = layer.boundingBox != null && layer.boundingBox.crs != null ?
                      layer.boundingBox.crs : this._defaultCrs;
                    const sourceCrs = `EPSG:${crs.properties.code}`;
                    const sw: [number, number] = <[number, number]>proj4(sourceCrs, this._targetCrs).forward([
                      layer.boundingBox.coordinates[0][0][0], layer.boundingBox.coordinates[0][0][1]
                    ]).reverse();
                    const ne: [number, number] = <[number, number]>proj4(sourceCrs, this._targetCrs).forward([
                      layer.boundingBox.coordinates[0][2][0], layer.boundingBox.coordinates[0][2][1]
                    ]).reverse();
                    const layerBounds = L.latLngBounds(sw, ne);
                    bounds = bounds == null ? layerBounds : bounds.extend(layerBounds);
                  });

                const mapLayersIds = layers.map((l) => l.uniqueId);
                currentLayersIds
                  .forEach((i) => {
                    const idx = parseInt(i, 10);
                    if (mapLayersIds.indexOf(idx) === -1) {
                      (<L.Map>this._lMap).removeLayer(this._currentLayers[i]);
                    }
                  });

                this._reorderLayers(layers);

                if (bounds != null) {
                  this._lMap.fitBounds(bounds);
                  this._service.setCenterZoom(
                    (<L.LatLngBounds>bounds).getCenter(),
                    this._lMap.getBoundsZoom(bounds)
                  );
                }
              });
          }

          this._updatePanOffset();
        });
    }
  }

  ngOnDestroy(): void {
    if (this._layersSubscription != null) { this._layersSubscription.unsubscribe(); }
    if (this._mapSubscription != null) { this._mapSubscription.unsubscribe(); }
    if (this._zoomSubscription != null) { this._zoomSubscription.unsubscribe(); }
    if (this._mapCenterSubscription != null) { this._mapCenterSubscription.unsubscribe(); }
    if (this._mapCenterEventSubscription != null) { this._mapCenterEventSubscription.unsubscribe(); }
    if (this._toggleLayerVisibilitySubscription != null) { this._toggleLayerVisibilitySubscription.unsubscribe(); }
    if (this._saveImageSubscription != null) { this._saveImageSubscription.unsubscribe(); }
    if (this._featureInfoSub != null) { this._featureInfoSub.unsubscribe(); }
    if (this._featureInfoDialogSub != null ) { this._featureInfoDialogSub.unsubscribe(); }
  }

  private _setMapLimits(): void {
    if (this._lMap == null) { return; }
    const limits: [number, number][] = this._limits != null && this._limits.length === 4 ? [
      [this._limits[0], this._limits[1]],
      [this._limits[2], this._limits[3]]
    ] : [[-180.0000, -90.0000], [180.0000, 90.0000]];
    this._lMap.setMaxBounds(limits);
    this._lMap.setMinZoom(this._lMap.getBoundsZoom(limits));
    this._updateMapCenter();
  }

  private _setMapBoundingBox(): void {
    if (this._lMap == null) { return; }
    const bounds: [number, number][] = this._boundingBox != null && this._boundingBox.length === 4 ? [
      [this._boundingBox[0], this._boundingBox[1]],
      [this._boundingBox[2], this._boundingBox[3]],
    ] : [[-180.0000, -90.0000], [180.0000, 90.0000]];
    this._lMap.fitBounds(bounds);
  }

  private _saveImage(
    canvas: any, fmt: MapImageFormat,
    loadingDialog: MatDialogRef<LoadingDialogComponent>,
    scaleCanvas: any,
    northCanvas: any,
    legendCanvas: any = null
  ): void {
    try {
      const p = this._imageFormatToSaveAsDataUrlOptions(fmt);
      const ctx = canvas.getContext('2d');
      if (scaleCanvas != null) {
        ctx.fillStyle = '#ffffff';
        const baseX = 40;
        const baseY = canvas.height - scaleCanvas.height - 20;
        ctx.fillRect(baseX - 20, baseY - 20, scaleCanvas.width + 40, scaleCanvas.height + 40 );
        ctx.drawImage(scaleCanvas, baseX, baseY, scaleCanvas.width, scaleCanvas.height);
      }
      if (northCanvas != null) {
        ctx.fillStyle = '#ffffff';
        const baseX = canvas.width - northCanvas.width - 40;
        const baseY = 40;
        ctx.fillRect(baseX - 20, baseY - 20, northCanvas.width + 40, northCanvas.height + 40 );
        ctx.drawImage(northCanvas, baseX, baseY, northCanvas.width, northCanvas.height);
      }
      if (legendCanvas != null) {
        ctx.fillStyle = '#ffffff';
        const baseX = canvas.width - legendCanvas.width - 20;
        const baseY = canvas.height - legendCanvas.height - 20;
        ctx.fillRect(baseX - 20, baseY - 20, legendCanvas.width + 40, legendCanvas.height + 40 );
        ctx.drawImage(legendCanvas, baseX, baseY, legendCanvas.width, legendCanvas.height);
      }
      const img = canvas.toDataURL(p.type, p.options);
      const el = this.downloadImageLink.nativeElement;
      const filename = `${this.map && this.map.name ? this.map.name : 'map'}.${p.extension}`;
      this._renderer.setElementAttribute(el, 'download', filename);
      this._renderer.setElementAttribute(el, 'href', img);
      loadingDialog.close();
      el.click();
    } catch (e) {
      this._saveImageError(loadingDialog);
    }
  }

  private _saveImageError(loadingDialog: MatDialogRef<LoadingDialogComponent>): void {
    loadingDialog.close();
    this._snackBar.openFromComponent(MapImageSaveErrorComponent, {
      duration: 2000
    });
  }

  private _setBaseLayer(baseLayer: MapBaseLayer): void {
    if (this._lMap == null) { return; }
    if (this._baseLLayer != null) {
      this._baseLLayer.remove();
    }
    if (baseLayer !== 'none') {
      const baseLayerOpts = this._getBaseLayer(baseLayer);
      this._baseLLayer = this._getBaseLayer(baseLayer)
        .addTo(this._lMap).bringToBack();
    }
  }

  private _getBaseLayer(baseLayer: MapBaseLayer): L.TileLayer {
    switch (baseLayer) {
      case 'google_roadmap':
      case 'google_satellite':
      case 'google_terrain':
      case 'google_hybrid':
      return (<any>L.gridLayer).googleMutant({
        type: (<string>baseLayer).substr(7)
      });
      case 'bing_road':
      case 'bing_satellite':
      case 'bing_hybrid':
      let type = 'Road';
      if (baseLayer === 'bing_satellite') { type = 'Aerial'; }
      if (baseLayer === 'bing_hybrid') { type = 'AerialWithLabels'; }
      return (<any>L).bingLayer(environment.mapsConfig.bingApiKey, {
        type: type
      });
      case 'osm':
      default:
      return L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      });
    }
  }

  private _imageFormatToSaveAsDataUrlOptions(
    format: MapImageFormat
  ): {type: string, extension: string, options?: number} {
    switch (format) {
      case 'png':
      return {type: 'image/png', extension: 'png'};
      case 'jpeg_high':
      return {type: 'image/jpeg', extension: 'jpg', options: 0.8};
      case 'jpeg_medium':
      return {type: 'image/jpeg', extension: 'jpg', options: 0.6};
      case 'jpeg_low':
      return {type: 'image/jpeg', extension: 'jpg', options: 0.3};
    }
  }

  private _reorderLayers(layers: IMapLayer[]): void {
    layers.slice(0)
      .filter(l => l.visible)
      .sort((l1, l2) => l1.zIndex - l2.zIndex)
      .forEach((l) => {
        try {
          (<any>this._currentLayers[l.uniqueId]).bringToFront();
        } catch (e) { }
      });
  }

  private _toggleLayerVisibility(layerId: number, visibility: boolean) {
    if (this._lMap != null) {
      const ll = this._currentLayers[layerId];
      if (ll == null) { return; }
      if (visibility) {
        ll.addTo(this._lMap);
      } else {
        ll.removeFrom(this._lMap);
      }
    }
  }

  private _updateLayerStyle(layer: IMapLayer) {
    if (this._lMap != null) {
      const ll = this._currentLayers[layer.uniqueId];
      if (ll == null) { return; }
      if (ll instanceof L.GeoJSON) {
        ll.setStyle((feature: GeoJSON.Feature<GeoJSON.GeometryObject>) => {
          return this._getStyleForFeature(feature, layer.style);
        });
      }
    }
  }

  private _destroyMap(): void {
    if (this._lMap != null) {
      this._lMap.removeEventListener('zoomend', this._updateZoomLevel, this);
      this._lMap.removeEventListener('zoomend', this._updatePanOffset, this);
      this._lMap.removeEventListener('dragend', this._updateMapCenter, this);
      this._lMap.remove();
      this._lMap = null;
    }
    this._currentLayers = {};
    this._currentFeatures = {};
  }

  private _updateZoomLevel(): void {
    if (this._lMap != null) {
      this._service.setZoomLevel(this._lMap.getZoom());
    }
  }

  private _updateMap(): void {
    if (this.map != null) {
      this._service.setMap(this.map);
    }
  }

  private _mapLayerToLeaftlet(layer: IMapLayer): L.Layer | null {
    if (layer.layerType === 'WMS_RASTER' && layer.url) {
      return L.tileLayer.wms(generateWMSUrl(layer.url), {
        layers: layer.layers,
        format: 'image/png',
        transparent: true,
        crs: L.CRS.EPSG3857
      });
    } else if (layer.layerType === 'VECTOR' && layer.features != null) {
      layer = crsNameToEpsg(layer, this._defaultCrs);
      /*return new (<any>L).VectorGrid.Slicer(
        reproject(
          <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features,
          <any>null, this._targetCrs, Proj4jsProjections
        ),
        {
          maxZoom: 18,
          tolerance: 3,
          extent: 1024,
          buffer: 64,
          indexMaxZoom: 4,
          indexMaxPoints: 100000,
          solidChildren: false
        }
      );*/
      const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
      const reprojected = reproject(
        features,
        <any>null, this._targetCrs, Proj4jsProjections
      );
      return L.geoJSON(
        reprojected,
        {
          style: (feature: GeoJSON.Feature<GeoJSON.GeometryObject>) => {
            return this._getStyleForFeature(feature, layer.style);
          },
          pointToLayer: (feature, latlng) => {
            return <L.CircleMarker>this._createPoint(feature, layer.style, latlng);
          },
          onEachFeature: (f, l) => {
            l.addEventListener('click', this._onClickFeature, this);
            (<any>f).parentLayer = layer;
            this._currentFeatures[`l${layer.uniqueId}_${f.id}`] = l;
          }
        }
      );
    }
    return null;
  }

  private _onClickFeature(evt): void {
    const feature = evt.target.feature;
    const mouseEvent: MouseEvent = evt.originalEvent;
    const layer: IMapLayer = (<any>feature).parentLayer;
    this._service.showFeatureInfo({
      feature: {
        layer: feature.parentLayer.uniqueId,
        features: [feature.id]
      },
      properties: feature.properties,
      labels: layer.dataLabels || {},
      x: mouseEvent.clientX,
      y: mouseEvent.clientY
    });
  }

  private _createPoint(
    feature: GeoJSON.Feature<GeoJSON.Point>,
    style: IMapLayerStyle | IMapConditionalLayerStyle | undefined,
    latlng: L.LatLng
  ): L.CircleMarker | null {
    const s: IMapLayerStyle = style != null && Object.getOwnPropertyNames(style).indexOf('prop') > -1 ?
      <IMapLayerStyle>this._resolveConditionalMapStyle(feature, <IMapConditionalLayerStyle>style) :
      <IMapLayerStyle>style;
    const pointType = s ? s.pointType || 'circle' : 'circle';
    if (pointType === 'circle') {
      const y = this._getStyleForFeature(feature, s);
      return L.circleMarker(latlng, y);
    }
    return null;
  }

  private _resolveConditionalMapStyle(
    feature: GeoJSON.Feature<GeoJSON.GeometryObject>,
    style: IMapConditionalLayerStyle
  ): IMapLayerStyle | null {
    if (feature.properties == null) { return null; }
    const values = style.styles.map(c => c.range);
    const prop = style.prop;
    let i = 0;
    const stepsLimit = values.length - 1;
    const value = feature.properties[prop];
    while (i < stepsLimit) {
      const range = values[i];
      const numRange = <[number, number]>range;
      if ((typeof range === 'string' || range == null) && (typeof value === 'string' || value === null)) {
        if (value === range) {
          return style.styles[i].style;
        }
      } else if (numRange[0] <= value && numRange[1] > value) {
        return style.styles[i].style;
      }
      i++;
    }
    return style.styles[stepsLimit].style;
  }

  private _getStyleForFeature(
    feature: GeoJSON.Feature<GeoJSON.GeometryObject>,
    style?: IMapLayerStyle | IMapConditionalLayerStyle
  ): L.PathOptions {
    if (style != null && Object.getOwnPropertyNames(style).indexOf('prop') > -1) {
      return this._getStyleForFeature(
        feature, <IMapLayerStyle>this._resolveConditionalMapStyle(feature, <IMapConditionalLayerStyle>style)
      );
    } else {
      const s = <IMapLayerStyle>(style != null ? style : defaultMapStyle);
      switch (feature.geometry.type) {
        case 'LineString':
        case 'MultiLineString':
        return <L.PathOptions>{
          stroke: s.stroke,
          color: s.color,
          opacity: s.opacity,
          fill: false
        };
        default:
        return <L.PathOptions>s;
      }
    }
  }

  private _stringToCrs(crs: string): L.CRS {
    switch (crs) {
      case 'EPSG:3857':
      return L.CRS.EPSG3857;
      case 'EPSG:4326':
      return L.CRS.EPSG4326;
      case 'EPSG:3395':
      return L.CRS.EPSG3395;
      default:
      return L.CRS.EPSG3857;
    }
  }

  private _initZoom(): void {
    this._zoomSubscription = this._service.getZoomLevel()
      .subscribe((zoom: number) => {
        if (this._lMap != null) {
          this._lMap.setZoom(zoom);
        }
      });
  }

  private _initMapCenter(): void {
    this._mapCenterSubscription = this._service.getCenter()
      .withLatestFrom(this._service.getZoomLevel())
      .subscribe((v: [MapCenter, number]) => {
        if (this._lMap != null) {
          const center = v[0];
          const zoom = v[1];
          this._lMap.setView(center, zoom);
        }
      });
    this._mapCenterEventSubscription = this._mapCenterEvent
      .subscribe((center: MapCenter) => {
        this._service.setCenter(center);
      });
  }

  private _updatePanOffset(): void {
    if (this._lMap != null) {
      const zoom = this._lMap.getZoom();
      const center = this._lMap.getCenter();
      const start = <any>this._lMap.project(center, zoom);
      const pan = this._lMap.unproject(this._lMap.containerPointToLayerPoint([start.x + 80, start.y + 80]), zoom);
      this._service.setMapParams({
        panOffsetX: Math.abs(pan.lng - center.lng),
        panOffsetY: Math.abs(pan.lat - center.lat)
      });
    }
  }

  private _updateMapCenter(evt?): void {
    if (this._lMap != null) {
      this._mapCenterEvent.emit(this._lMap.getCenter());
    }
  }
}
