import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, ViewChild } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/withLatestFrom';

import * as L from 'leaflet';

import * as turfHelpers from '@turf/helpers';
import { default as turfCentroid } from '@turf/centroid';
import { default as turfBooleanPointInPolygon } from '@turf/boolean-point-in-polygon';

import { Chart, ChartData } from 'chart.js';

import { reproject } from 'reproject';

import {
  composeUrl, crsNameToEpsg, guessGeometryTypes,
  IMapLayer, MapService, MapBuilderService, MapSpatialFilterMode, Proj4jsProjections
} from '@yago/core';

import { environment } from '../../environments/environment';


@Component({
  selector: 'yago-map-spatial-analysis-control',
  templateUrl: './spatial-analysis-control.html',
  styleUrls: ['./spatial-analysis-control.scss']
})
export class MapSpatialAnalysisControlComponent implements AfterViewInit, OnDestroy {
  @Input()
  map: L.Map;

  @ViewChild('analysisChart')
  chartCanvas: ElementRef;

  private _indicator: string;
  get indicator(): string { return this._indicator; }
  set indicator(indicator: string) {
    this._indicator = indicator;
    this._redrawChartEvent.emit(false);
  }

  private _chartType: 'pie' | 'bar' = 'pie';
  get chartType(): 'pie' | 'bar' { return this._chartType; }
  set chartType(chartType: 'pie' | 'bar') {
    this._chartType = chartType;
    this._redrawChartEvent.emit(true);
  }

  private _mode: MapSpatialFilterMode = 'centroid';
  get mode(): MapSpatialFilterMode { return this._mode; }
  set mode(mode: MapSpatialFilterMode) {
    this._mode = mode;
    this._redrawChartEvent.emit(false);
  }

  private _partitionLayer: IMapLayer;
  private _reprojectedPartitionLayer: IMapLayer;
  get partitionLayer(): IMapLayer { return this._partitionLayer; }
  set partitionLayer(partitionLayer: IMapLayer) {
    if (partitionLayer != null && partitionLayer.features != null) {
      const rpl = crsNameToEpsg(partitionLayer);
      if (rpl.features != null) {
        rpl.features = <any>reproject(rpl.features, <any>null, <string>L.CRS.EPSG4326.code, Proj4jsProjections);
      }
      this._reprojectedPartitionLayer = rpl;
    }
    this._partitionLayer = partitionLayer;
    if (
      partitionLayer == null || partitionLayer.features == null ||
      partitionLayer.features.features == null || partitionLayer.features.features.length === 0
    ) {
      this._partitionLabels = [];
    } else {
      this._partitionLabels = Object.keys(partitionLayer.features.features[0].properties || {});
    }
    this._redrawChartEvent.emit(false);
  }

  private _partitionLabels: string[] = [];
  get partitionLabels(): string[] { return this._partitionLabels; }

  private _partitionLabel: string;
  get partitionLabel(): string { return this._partitionLabel; }
  set partitionLabel(partitionLabel: string) {
    this._partitionLabel = partitionLabel;
    this._redrawChartEvent.emit(false);
  }

  private _layer: Observable<IMapLayer | null>;
  get layer(): Observable<IMapLayer | null> { return this._layer; }

  private _layers: Observable<IMapLayer[]>;
  get layers(): Observable<IMapLayer[]> { return this._layers; }

  private _indicators: Observable<string[]>;
  get indicators(): Observable<string[]> { return this._indicators; }

  private _canDownload = false;
  get canDownload(): boolean { return this._canDownload; }

  private _curLabels: string[];
  private _curValues: number[];
  private _curColors: string[];
  private _features: Observable<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>;
  private _init: boolean;
  private _chart: Chart | null;
  private _drawLayer: L.FeatureGroup = L.featureGroup();
  private _points: L.LatLng[] = [];
  private _redrawChartEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  private _redrawChartSub: Subscription;

  private _selectionDraw = false;
  get selectionDraw(): boolean { return this._selectionDraw; }
  private _partitionByLayer = false;
  get partitionByLayer(): boolean { return this._partitionByLayer; }

  private _chartOptions: any = {
    legend: {
      display: false
    }
  };

  private _barChartOptions: any = Object.assign({}, this._chartOptions, {
    scales: {
      xAxes: [{
        display: false
      }]
    }
  });

  private _colors: string[] = [
    '#ef5350', '#7e57c2', '#29b6f6', '#66bb6a', '#ffee58',
    '#ec407a', '#5c6bc0', '#26c6da', '#9ccc65', '#ffca28',
    '#ab47bc', '#42a5f5', '#26a69a', '#d4e157', '#ffa726',
    '#ff7043', '#8d6e63', '#bdbdbd', '#78909c'
  ];

  constructor(
    private _service: MapService,
    private _mapBuilderService: MapBuilderService,
    private _http: Http
  ) {
    this._layer = _service.getSelectedLayer();
    this._layers = _service.getLayers()
      .withLatestFrom(this._layer)
      .map((r: [IMapLayer[], IMapLayer]) => {
        const layers: IMapLayer[] = r[0] || [];
        const layer: IMapLayer = r[1];
        if (layer == null) { return layers; }
        return layers.filter(l => {
          if (l.features == null) { return false; }
          const geometries = guessGeometryTypes(l.features);
          return l.uniqueId !== layer.uniqueId &&
            geometries.indexOf('Point') === -1 &&
            geometries.indexOf('LineString') === -1 &&
            geometries.indexOf('MultiLineString') === -1;
        });
      });
    this._features = <any>this._layer
      .map(l => {
        if (l != null && l.features != null) {
          l = crsNameToEpsg(l);
          return reproject(
            <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>l.features,
            <any>null, <string>L.CRS.EPSG4326.code, Proj4jsProjections
          );
        }
        return null;
      });
    this._indicators = this._layer
      .filter(l => l != null && l.features != null)
      .map(layer => {
        const indicators: {[name: string]: 'number' | 'string'} = {};
        const l = <IMapLayer>layer;
        (<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>l.features).features.forEach(f => {
          const cis = Object.keys(f.properties || {});
          cis.forEach(ci => {
            const propValue = (<any>f.properties)[ci];
            const ct = this._guessPropertyType(propValue);
            if (ct === 'number' && (indicators[ci] == null || indicators[ci] === 'number')) {
              indicators[ci] = 'number';
            } else {
              indicators[ci] = 'string';
            }
          });
        });
        const numIndicators: string[] = [];
        Object.keys(indicators)
          .forEach((i) => {
            if (indicators[i] === 'number') {
              numIndicators.push(i);
            }
          });
        return numIndicators;
      });

    this._redrawChartEvent
      .debounceTime(500)
      .withLatestFrom(this._features)
      .subscribe((r: [boolean, GeoJSON.FeatureCollection<GeoJSON.GeometryObject>]) => {
        const changeChartType: boolean = r[0];
        const features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject> = r[1];
        if (
          features == null ||
          (
            this._partitionLayer == null && this._partitionLabel == null &&
            (this._points == null || this._points.length < 3)
          ) ||
          (this._points == null && (this._partitionLayer == null || this._partitionLabel == null)) ||
          this._chartType == null || this._indicator == null
        ) {
          this._clearChart();
        } else {
          this._redrawChart(features, changeChartType);
        }
      });
  }

  ngAfterViewInit(): void {
    this._init = true;
    this._redrawChartEvent.emit(false);
  }

  ngOnDestroy(): void {
    try {
      if (this._drawLayer != null && this.map != null) {
        this._drawLayer.removeFrom(this.map);
      }
    } catch (e) { }
    if (this._redrawChartSub != null) { this._redrawChartSub.unsubscribe(); }
  }

  closeMe(): void {
    this._mapBuilderService.closeSpatialAnalysisControl();
  }

  startDrawPolygon(): void {
    if (this.map == null) { return; }

    this._resetState();
    this._selectionDraw = true;
    this.map.addEventListener('mousedown', this._onMouseDownDraw, this);
    this._drawLayer.addTo(this.map);
  }

  stopDrawPolygon(): void {
    this.map.removeEventListener('mousedown', this._onMouseDownDraw, this);
    this._selectionDraw = false;
  }

  startFeatureSelection(): void {
    if (this.map == null) { return; }

    this._resetState();
    this._partitionByLayer = true;
  }

  stopFeatureSelection(): void {

    this._resetState();
    this._partitionByLayer = false;
  }

  downloadSpreadsheet(): void {
    const data = {
      labels: this._curLabels,
      colors: this._curColors,
      values: this._curValues,
      indicator: this._indicator,
      chart: this._chartType
    };
    const url = composeUrl([
      environment.apiConfig.baseUrl, environment.apiConfig.spatialAnalysisEndpoint
    ]);
    const s = this._http.post(url, data, {responseType: ResponseContentType.Blob})
      .subscribe((r) => {
        try {
          const saBlob = r.blob();
          location.href = URL.createObjectURL(saBlob);
        } catch (e) { }
        s.unsubscribe();
      }, (e) => { s.unsubscribe(); });
  }

  private _resetState(): void {
    this._selectionDraw = false;
    this._partitionByLayer = false;
    this._points = [];
    this._canDownload = false;
    try {
      this._drawLayer.removeFrom(this.map);
    } catch (e) { }
  }

  private _redrawChart(
    features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>,
    changeChartType: boolean
  ): void {
    if (!this._init) { return; }
    if (this._points != null && this._points.length > 0) {
      this._redrawPolygonChart(features, changeChartType);
    } else if (this._partitionLayer != null && this._partitionLabel != null) {
      this._redrawPolygonLayer(features, changeChartType);
    }
  }

  private _redrawPolygonLayer(
    features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>,
    changeChartType: boolean
  ): void {
    const labels = <any>[];
    const values = <any>[];

    const rf = this._reprojectedPartitionLayer.features != null ?
      this._reprojectedPartitionLayer.features.features : <GeoJSON.Feature<GeoJSON.GeometryObject>[]>[];
    rf.forEach((partitionFeature) => {
        if (partitionFeature.properties != null) {
          labels.push(partitionFeature.properties[this._partitionLabel] != null);
          let value = 0;
          features.features
            .forEach((feature) => {
              if (feature.properties != null) {
                const selected = this._isFeatureSelected(feature, <GeoJSON.Feature<GeoJSON.Polygon>>partitionFeature);
                if (selected) {
                  value += feature.properties[this.indicator];
                }
              }
            });
          values.push(value);
        }
      });
    this._createChart(changeChartType, labels, values);
  }

  private _redrawPolygonChart(
    features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>,
    changeChartType: boolean
  ): void {
    if (this._points.length < 3) { return; }
    let selectedValue = 0;
    let unselectedValue = 0;
    const polygon: number[][] = this._points.map(p => [p.lng, p.lat]);
    polygon.push([this._points[0].lng, this._points[0].lat]);
    const gPolygon = turfHelpers.polygon([polygon]);
    features.features
      .forEach((feature) => {
        if (feature.properties != null) {
          const selected = this._isFeatureSelected(feature, gPolygon);
          let featureValue = feature.properties[this.indicator];
          if (featureValue == null) { featureValue = 0; }
          if (selected) {
            selectedValue += featureValue;
          } else {
            unselectedValue += featureValue;
          }
        }
      });
    this._createChart(
      changeChartType,
      [environment.labels.selected, environment.labels.notSelected],
      [selectedValue, unselectedValue]
    );
  }

  private _createChart(changeChartType: boolean, labels: string[], values: number[]): void {
    if (changeChartType) {
      this._clearChart();
    }
    if (this._chart == null) {
      this._createEmptyChart();
    }
    if (this._chart == null) {
      return;
    }
    let colors = this._colors.slice(0);
    while (colors.length < labels.length) {
      colors = colors.concat(this._colors);
    }
    this._curLabels = labels;
    this._curValues = values;
    this._curColors = colors;
    this._canDownload = true;
    const data: ChartData = <ChartData>this._chart.data;
    data.labels = labels;
    data.datasets = [{
      label: this.indicator,
      data: values,
      backgroundColor: <any>colors,
      borderColor: <any>colors
    }];
    this._chart.update();
  }

  private _isFeatureSelected(
    feature: GeoJSON.Feature<GeoJSON.GeometryObject>,
    polygon: GeoJSON.Feature<GeoJSON.Polygon> | GeoJSON.Feature<GeoJSON.MultiPolygon>
  ): boolean {
    let selected = false;
    if (this.mode === 'centroid') {
      selected = turfBooleanPointInPolygon(turfCentroid(feature), polygon);
    } else if (this.mode === 'whole') {
      let isInside = true;
      const coordinates: number[][] | number[] = (<any>feature.geometry).coordinates || [];
      const coordsNum = coordinates.length;
      if (coordsNum > 0) {
        if (coordinates[0] instanceof Array) {
          let i = 0;
          while (isInside && i < coordsNum) {
            isInside = isInside && turfBooleanPointInPolygon(turfHelpers.point(<number[]>coordinates[i]), polygon);
            i++;
          }
        } else if (coordsNum === 2) {
          isInside = turfBooleanPointInPolygon(turfHelpers.point(<number[]>coordinates), polygon);
        }
        selected = isInside;
      }
    }
    return selected;
  }

  private _createEmptyChart(): void {
    if (this.chartCanvas == null) { return; }
    this._chart = new Chart(this.chartCanvas.nativeElement, {
      type: this.chartType,
      data: {},
      options: this.chartType === 'bar' ? this._barChartOptions : this._chartOptions
    });
  }

  private _clearChart(): void {
    if (this._chart == null) { return; }
    this._chart.destroy();
    this._chart = null;
  }

  private _guessPropertyType(val: any): 'number' | 'string' {
    try {
      const fVal = parseFloat(val);
      if (!isNaN(fVal)) {
        return 'number';
      }
    } catch (e) { }
    return 'string';
  }

  private _onMouseDownDraw(evt: L.LeafletMouseEvent): void {
    this._points.push(evt.latlng);
    this._redraw();
  }

  private _redraw(): void {
    this._drawLayer.clearLayers();

    if (this._points.length === 1) {
      this._drawLayer.addLayer(L.circle(this._points[0]));
    } else if (this._points.length === 2) {
      this._drawLayer.addLayer(L.polyline(this._points));
    } else {
      this._drawLayer.addLayer(L.polygon(this._points));
    }

    this._redrawChartEvent.emit();
  }
}
