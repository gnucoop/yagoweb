import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';

import * as L from 'leaflet';

import { default as turfDistance } from '@turf/distance';
import * as helpers from '@turf/helpers';

import { Chart, ChartData, ChartDataSets } from 'chart.js';

declare var google: any;

interface MapElevationPoint {
  point: L.LatLng;
  distance: number;
  elevation?: number;
}


@Component({
  selector: 'yago-map-elevation-profile-control',
  templateUrl: './elevation-profile-control.html',
  styleUrls: ['./elevation-profile-control.scss']
})
export class MapElevationProfileControlComponent implements AfterViewInit, OnDestroy, OnInit {
  @Input()
  map: L.Map;

  @ViewChild('elevationChart')
  chartCanvas: ElementRef;

  private _chart: Chart;
  private _elevator: any;
  private _layer: L.FeatureGroup = L.featureGroup();

  private _points: MapElevationPoint[] = [];
  get points(): MapElevationPoint[] { return this._points; }

  ngOnInit(): void {
    if (this.map != null) {
      this.map.addEventListener('mousedown', this._onMouseDown, this);
      this._elevator = new google.maps.ElevationService;
      this._layer.addTo(this.map);
    }
  }

  ngAfterViewInit(): void {
    if (this.chartCanvas != null) {
      const chartOptions: any = {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            title: (tooltipItems, data) => {
              let title = '';
              const labels = data.labels;
              const labelCount = labels ? labels.length : 0;

              if (tooltipItems.length > 0) {
                const item = tooltipItems[0];

                if (item.xLabel) {
                  title = item.xLabel;
                } else if (labelCount > 0 && item.index < labelCount) {
                  title = labels[item.index];
                }
              }

              return `Distance: ${this._addMeters(title)}`;
            },
            label: (tooltipItem, data) => {
              let label = data.datasets[tooltipItem.datasetIndex].label || '';
              if (label) {
                label += ': ';
              }
              label += this._addMeters(tooltipItem.yLabel);
              return label;
            }
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
              callback: this._addMeters,
              beginAtZero: true
            }
          }],
          yAxes: [{
            ticks: {
              callback: this._addMeters,
              beginAtZero: true
            }
          }]
        }
      };
      this._chart = new Chart(this.chartCanvas.nativeElement, {
        type: 'line',
        data: {},
        options: chartOptions
      });
    }
  }

  ngOnDestroy(): void {
    if (this.map != null) {
      this.map.removeEventListener('mousedown', this._onMouseDown, this);
      this.map.removeLayer(this._layer);
    }
  }

  reset(): void {
    this._points = [];
    this._redraw();
  }

  private _addMeters(value, index?, values?) {
    return `${Math.round(value * 100) / 100}m`;
  }

  private _onMouseDown(evt: L.LeafletMouseEvent): void {
    const pointsNum = this._points.length;
    let curDistance = 0;
    if (pointsNum > 0) {
      const lastPoint = this._points[pointsNum - 1];
      const lastPointCoords = lastPoint.point;
      curDistance = lastPoint.distance + turfDistance(
        helpers.point([lastPointCoords.lng, lastPointCoords.lat]),
        helpers.point([evt.latlng.lng, evt.latlng.lat]),
        {units: 'meters'}
      );
    }
    const newPoint = {
      point: evt.latlng,
      distance: curDistance
    };
    this._points.push(newPoint);
    this._getPointElevation(newPoint);
    this._redraw();
  }

  private _redraw(): void {
    this._layer.clearLayers();

    if (this._points.length === 1) {
      this._layer.addLayer(L.circle(this._points[0].point));
    } else if (this._points.length > 1) {
      this._layer.addLayer(L.polyline(this._points.map(p => p.point)));
    }
  }

  private _updateGraph(): void {
    if (this._chart != null && this._chart.data != null && this._chart.data.datasets != null) {
      const data: ChartData = <ChartData>this._chart.data;
      const datasets: ChartDataSets[] = <ChartDataSets[]>data.datasets;
      if (datasets.length === 0) {
        datasets.push({
          label: 'Elevation',
          data: []
        });
      }
      const dataset = datasets[0];
      dataset.data = this._points.map(p => <any>{x: p.distance, y: p.elevation});
      this._chart.update();
    }
  }

  private _getPointElevation(point: MapElevationPoint): void {
     this._elevator.getElevationForLocations({
       locations: [{lat: point.point.lat, lng: point.point.lng}]
     }, (res, status) => {
       if (status === 'OK' && res.length === 1) {
         point.elevation = res[0].elevation;
       }
       this._updateGraph();
     });
  }
}
