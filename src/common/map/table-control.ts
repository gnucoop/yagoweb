import { Component, EventEmitter, HostBinding, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/withLatestFrom';

import {
  IMapLayer, IMapSelectedFeatures, MapService,
  MapTableColumn, MapTableCell, MapTableFilterOperator,
  MapTableConditionOperator, MapTableFilter
} from '@yago/core';


@Component({
  selector: 'yago-map-table-control',
  templateUrl: './table-control.html',
  styleUrls: ['./table-control.scss']
})
export class MapTableControlComponent implements OnDestroy {
  private _open = false;
  @HostBinding('class.open') get open(): boolean { return this._open; }

  private _showTable: Observable<boolean>;
  get showTable(): Observable<boolean> { return this._showTable; }

  private _layer: Observable<IMapLayer | null>;
  get layer(): Observable<IMapLayer | null> { return this._layer; }

  private _isValidLayerSelected: Observable<boolean>;
  get isValidLayerSelected(): Observable<boolean> { return this._isValidLayerSelected; }

  private _data: Observable<MapTableCell[]>;
  private _filteredData: Observable<MapTableCell[]>;
  get filteredData(): Observable<MapTableCell[]> { return this._filteredData; }

  private _columns: Observable<MapTableColumn[]>;
  get columns(): Observable<MapTableColumn[]> { return this._columns; }

  private _columnOptions: Observable<{[column: string]: any[]}>;

  private _orderedColumns: Observable<MapTableColumn[]>;
  get orderedColumns(): Observable<MapTableColumn[]> { return this._orderedColumns; }

  private _selectedFeatures: Observable<MapTableCell[]>;
  get selectedFeatures(): Observable<MapTableCell[]> { return this._selectedFeatures; }

  private _filteredFeatures: Observable<IMapSelectedFeatures[]>;
  get filteredFeatures(): Observable<IMapSelectedFeatures[]> { return this._filteredFeatures; }

  private _filter: MapTableFilter = { operator: 'and', conditions: [] };
  get filter(): MapTableFilter { return this._filter; }

  private _openSub: Subscription;
  private _featureSelectedEvent: EventEmitter<any> = new EventEmitter<any>();
  private _featureSelectedSubscription: Subscription;
  private _applyFilterEvt: EventEmitter<MapTableFilter> = new EventEmitter<MapTableFilter>();
  private _applyFilterSub: Subscription;
  private _createFilteredLayerEvt: EventEmitter<MapTableFilter> = new EventEmitter<MapTableFilter>();
  private _createFilteredLayerSub: Subscription;

  constructor(private _service: MapService) {
    this._showTable = _service.getTableControlOpen();

    this._openSub = this._showTable.subscribe((s) => this._open = s);

    this._layer = _service.getSelectedLayer();

    this._isValidLayerSelected = this._layer
      .map((l) => l != null && l.features != null && l.features.features != null && l.features.features.length > 0);

    this._columns = this._layer
      .map(l => {
        if (l == null || l.features == null || l.features.features == null || l.features.features.length === 0) {
          return [];
        }
        const dataLabels = l.dataLabels || {};
        return Object.keys(l.features.features[0].properties || {})
          .map(c => {
            return {prop: c, name: dataLabels[c] || c};
          });
      })
      .publishReplay(1)
      .refCount();

    this._orderedColumns = this._columns
      .publishReplay(1)
      .refCount();

    this._data = this._layer
      .map(l => {
        if (l == null || l.features == null || l.features.features == null || l.features.features.length === 0) {
          return [];
        }
        return l.features.features.map(f => Object.assign({'$$fid': <string>f.id}, f.properties));
      })
      .publishReplay(1)
      .refCount();

    this._filteredFeatures = this._service.getFilteredFeatures();

    this._filteredData = this._data
      .combineLatest(this._filteredFeatures, this._layer)
      .map((r: [MapTableCell[], IMapSelectedFeatures[], IMapLayer]) => {
        const data: MapTableCell[] = r[0];
        const layer: IMapLayer = r[2];
        const filtered = r[1]
          .filter((f) => f.layer === layer.uniqueId)
          .map((f) => f.features)
          .reduce((f1, f2) => f1.concat(f2), []);
        if (filtered.length > 0) {
          return data.filter((cell) => filtered.indexOf(cell['$$fid']) > -1);
        }
        return data;
      })
      .publishReplay(1)
      .refCount();

    this._columnOptions = this._columns
      .combineLatest(this._data)
      .map((r: [MapTableColumn[], MapTableCell[]]) => {
        const columns = r[0];
        const cells = r[1];
        const options: {[column: string]: any[] } = {};
        columns.forEach((column: MapTableColumn) => {
          options[column.prop] = cells
            .map(cell => cell[column.prop])
            .sort((v1, v2) => {
              if (typeof v1 === 'string' && typeof v2 === 'string') {
                return v1.localeCompare(v2);
              }
              return (<number>v1) - (<number>v2);
            })
            .reduce((prev, cur) => {
              if (prev.indexOf(cur) < 0) {
                prev.push(cur);
              }
              return prev;
            }, <Array<string|number>>[]);
        });
        return options;
      })
      .publishReplay(1)
      .refCount();

    this._featureSelectedSubscription = this._featureSelectedEvent
      .withLatestFrom(this._layer)
      .subscribe((r: [any, IMapLayer]) => _service.selectFeatures([{layer: r[1].uniqueId, features: [r[0]]}]));

    this._selectedFeatures = _service.getSelectedFeatures()
      .withLatestFrom(this._layer, this._data)
      .map((r: [IMapSelectedFeatures[], IMapLayer, MapTableCell[]]) => {
        const layer = r[1];
        const features = r[0].filter(f => f.layer === layer.uniqueId);
        const data = r[2];
        let selectedCells: MapTableCell[] = [];
        features.forEach(f => {
          selectedCells = selectedCells.concat(data.filter(c => f.features.indexOf(c['$$fid']) > -1));
        });
        return selectedCells;
      });

    this._applyFilterSub = this._applyFilterEvt
      .withLatestFrom(this._layer)
      .subscribe((r: [MapTableFilter, IMapLayer]) => {
        this._service.applyFilter(r[1], r[0]);
      });

    this._createFilteredLayerSub = this._createFilteredLayerEvt
      .withLatestFrom(this._layer)
      .subscribe((r: [MapTableFilter, IMapLayer]) => {
        this._service.createFilteredLayer(r[1], r[0]);
      });
  }

  ngOnDestroy(): void {
    if (this._featureSelectedSubscription != null) { this._featureSelectedSubscription.unsubscribe(); }
    if (this._applyFilterSub != null) { this._applyFilterSub.unsubscribe(); }
    if (this._createFilteredLayerSub != null) { this._createFilteredLayerSub.unsubscribe(); }
    if (this._openSub != null) { this._openSub.unsubscribe(); }
  }

  onTableClick(event: any): void {
    const featureId = event.row.$$fid;
    this._featureSelectedEvent.emit(featureId);
  }

  addFilterCondition(): void {
    this._filter.conditions.push({field: null, operator: 'equal', params: [null, null]});
  }

  removeFilterCondition(idx: number) {
    if (idx < 0 || idx >= this.filter.conditions.length) { return; }
    this._filter.conditions.splice(idx, 1);
  }

  getFieldOptions(field: string): Observable<any[]> {
    return this._columnOptions
      .map((options: {[field: string]: any[]}) => {
        if (field == null || options[field] == null) { return []; }
        return options[field];
      });
  }

  applyFilter(): void {
    if (this.filter.conditions.length === 0) { return; }
    this._applyFilterEvt.emit(Object.assign({}, this.filter, {
      conditions: this.filter.conditions.slice(0)
    }));
  }

  removeFilter(): void {
    this._applyFilterEvt.emit({operator: 'and', conditions: []});
  }

  createFilteredLayer(): void {
    if (this.filter.conditions.length === 0) { return; }
    this._createFilteredLayerEvt.emit(Object.assign({}, this.filter, {
      conditions: this.filter.conditions.slice(0)
    }));
  }

  toggleTable(): void {
    this._service.toggleTableControl();
  }
}
