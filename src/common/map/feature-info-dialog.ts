import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';

import { IMapShowFeatureInfo, MapService, decamelize } from '@yago/core';


@Component({
  selector: 'yago-map-feature-info-dialog',
  templateUrl: './feature-info-dialog.html',
  styleUrls: ['./feature-info-dialog.scss']
})
export class MapFeatureInfoDialogComponent {
  private _properties: Observable<{label: string, value: any}[]>;
  get properties(): Observable<{label: string, value: any}[]> { return this._properties; }

  constructor(
    public dialogRef: MatDialogRef<MapFeatureInfoDialogComponent>,
    private _service: MapService
  ) {
    this._properties = _service.getShowFeatureInfo()
      .filter(fi =>
        fi != null && fi.properties != null
      )
      .map((fi: IMapShowFeatureInfo) => {
        return Object.keys(fi.properties).map((p) => {
          const dp = decamelize(p);
          const label = fi.labels[p] ? `${fi.labels[p]} (${dp})` : dp;
          return {
            label: label,
            value: fi.properties[p]
          };
        });
      })
      .publishReplay(1)
      .refCount();
  }
}
