import { Component } from '@angular/core';

import { MapService } from '@yago/core';


@Component({
  selector: 'yago-map-scale-control',
  templateUrl: './scale-control.html',
  styleUrls: ['./scale-control.scss']
})
export class MapScaleControlComponent {
  constructor(private _service: MapService) {
    this._service.getZoomLevel();
  }
}
