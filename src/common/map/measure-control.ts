import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import * as L from 'leaflet';

import { default as turfArea } from '@turf/area';
import { default as turfDistance } from '@turf/distance';
import * as helpers from '@turf/helpers';


@Component({
  selector: 'yago-map-measure-control',
  templateUrl: './measure-control.html',
  styleUrls: ['./measure-control.scss']
})
export class MapMeasureControlComponent implements OnDestroy, OnInit {
  @Input()
  map: L.Map;

  private _lat = NaN;
  get lat(): number { return this._lat; }

  private _lng = NaN;
  get lng(): number { return this._lng; }

  private _distanceUnits = 'kilometers';
  get distanceUnits(): string { return this._distanceUnits; }
  set distanceUnits(distanceUnits: string) {
    this._distanceUnits = distanceUnits;
    this._updateDistance();
  }

  private _distance = NaN;
  get distance(): number { return this._distance; }

  private _areaUnits = 'kilometers';
  get areaUnits(): string { return this._areaUnits; }
  set areaUnits(areaUnits: string) {
    this._areaUnits = areaUnits;
    this._updateArea();
  }

  private _area = NaN;
  get area(): number { return this._area; }

  private _points: L.LatLng[] = [];
  get points(): L.LatLng[] { return this._points; }

  private _layer: L.FeatureGroup = L.featureGroup();

  ngOnInit(): void {
    if (this.map != null) {
      this.map.addEventListener('mouseover', this._onMouseOver, this);
      this.map.addEventListener('mouseout', this._onMouseOut, this);
      this._layer.addTo(this.map);
    }
  }

  ngOnDestroy(): void {
    if (this.map != null) {
      this.map.removeEventListener('mouseover', this._onMouseOver, this);
      this.map.removeEventListener('mouseout', this._onMouseOut, this);
      this.map.removeLayer(this._layer);
    }
  }

  reset(): void {
    this._points = [];
    this._redraw();
    this._updateDistance();
    this._updateArea();
  }

  private _onMouseOver(evt: L.LeafletMouseEvent): void {
    this.map.addEventListener('mousemove', this._onMouseMove, this);
    this.map.addEventListener('mousedown', this._onMouseDown, this);
    this._onMouseMove(evt);
  }

  private _onMouseMove(evt: L.LeafletMouseEvent): void {
    const lat = evt.latlng.lat;
    const lng = evt.latlng.lng;
    if (this._lat !== lat) { this._lat = lat; }
    if (this._lng !== lng) { this._lng = lng; }
  }

  private _onMouseOut(evt: L.LeafletMouseEvent): void {
    // this.map.removeEventListener('mousemove', this._onMouseMove, this);
    // this.map.removeEventListener('mousedown', this._onMouseDown, this);
    if (this._lat != null) { this._lat = NaN; }
    if (this._lng != null) { this._lng = NaN; }
  }

  private _onMouseDown(evt: L.LeafletMouseEvent): void {
    this._points.push(evt.latlng);
    this._redraw();
  }

  private _redraw(): void {
    this._layer.clearLayers();

    if (this._points.length > 0) {
      if (this._points.length === 1) {
        this._layer.addLayer(L.circle(this._points[0]));
      } else if (this._points.length === 2) {
        this._layer.addLayer(L.polyline(this._points));
      } else {
        this._layer.addLayer(L.polygon(this._points));
      }
    }
    this._updateDistance();
    this._updateArea();
  }

  private _updateDistance(): void {
    const pointsNum = this._points.length;
    if (pointsNum < 2) {
      this._distance = NaN;
    } else {
      const point1 = this._points[pointsNum - 2];
      const point2 = this._points[pointsNum - 1];
      let distanceUnits = this.distanceUnits;
      let distanceMultiplier = 1;

      if (distanceUnits === 'meters') {
        distanceUnits = 'kilometers';
        distanceMultiplier = 1000;
      }
      if (distanceUnits === 'feet') {
        distanceUnits = 'miles';
        distanceMultiplier = 5280;
      }

      this._distance = turfDistance(
        helpers.point([point1.lng, point1.lat]),
        helpers.point([point2.lng, point2.lat]),
        {units: <helpers.Units>distanceUnits}
      ) * distanceMultiplier;
    }
  }

  private _updateArea(): void {
    const pointsNum = this._points.length;
    if (pointsNum < 3) {
      this._area = NaN;
    } else {
      const polygon = [this._points.map((point) => [point.lng, point.lat])];
      polygon[0].push(polygon[0][0]);
      let areaMultiplier: number;
      switch (this.areaUnits) {
        case 'kilometers':
        areaMultiplier = 0.000001;
        break;
        case 'miles':
        areaMultiplier = 0.000000386102;
        break;
        case 'feet':
        areaMultiplier = 10.7639;
        break;
        case 'acres':
        areaMultiplier = 0.000247105;
        break;
        case 'hectares':
        areaMultiplier = 0.0001;
        break;
        default:
        areaMultiplier = 1;
      }
      this._area = turfArea(helpers.polygon(polygon)) * areaMultiplier;
    }
  }
}
