import { AfterViewInit, Component, ElementRef, OnDestroy, QueryList, ViewChildren } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

import { IMapLayer, MapService } from '@yago/core';


@Component({
  selector: 'yago-map-legend-control',
  templateUrl: './legend-control.html',
  styleUrls: ['./legend-control.scss']
})
export class MapLegendControlComponent implements AfterViewInit, OnDestroy {
  @ViewChildren('panel') panelQuery: QueryList<ElementRef>;

  private _panel: any;
  get panel(): any { return this._panel; }

  private _showLegendPanel: Observable<boolean>;
  get showLegendPanel(): Observable<boolean> { return this._showLegendPanel; }

  private _tablePanelVisible: Observable<boolean>;
  get tablePanelVisible(): Observable<boolean> { return this._tablePanelVisible; }

  private _layers: Observable<IMapLayer[]>;
  get layers(): Observable<IMapLayer[]> { return this._layers; }

  private _vcSub: Subscription;

  constructor(private _service: MapService) {
    this._showLegendPanel = _service.getLegendControlOpen();
    this._tablePanelVisible = _service.getTableControlOpen();
    this._layers = _service.getLayers()
      .filter((layers: IMapLayer[]) => layers != null)
      .map((layers: IMapLayer[]) => {
        return layers.filter(l => l.visible && l.layerType === 'VECTOR')
          .slice(0)
          .sort((l1, l2) => l1.zIndex - l2.zIndex);
      });
  }

  ngAfterViewInit(): void {
    this._vcSub = this.panelQuery.changes
      .subscribe(x => {
        if (this.panelQuery.length === 1) {
          this._panel = this.panelQuery.first.nativeElement;
        }
      });
  }

  ngOnDestroy(): void {
    if (this._vcSub != null) { this._vcSub.unsubscribe(); }
  }

  toggleLegendPanel(): void {
    this._service.toggleLegendControl();
  }
}
