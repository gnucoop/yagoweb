import { Component } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import {
  getCanZoomIn, getCanZoomOut, getCanViewGoBack, getCanViewGoForward,
  MapService, MapPanDirection, MapZoomDirection,
  State
} from '@yago/core';


@Component({
  selector: 'yago-map-view-control',
  templateUrl: './view-control.html',
  styleUrls: ['./view-control.scss']
})
export class MapViewControlComponent {
  private _canZoomIn: Observable<boolean>;
  get canZoomIn(): Observable<boolean> { return this._canZoomIn; }

  private _canZoomOut: Observable<boolean>;
  get canZoomOut(): Observable<boolean> { return this._canZoomOut; }

  private _canViewGoBack: Observable<boolean>;
  get canViewGoBack(): Observable<boolean> { return this._canViewGoBack; }

  private _canViewGoForward: Observable<boolean>;
  get canViewGoForward(): Observable<boolean> { return this._canViewGoForward; }

  constructor(private _service: MapService, private _store: Store<State>) {
    this._canZoomIn = _store.select(getCanZoomIn);
    this._canZoomOut = _store.select(getCanZoomOut);
    this._canViewGoBack = _store.select(getCanViewGoBack);
    this._canViewGoForward = _store.select(getCanViewGoForward);
  }

  zoomIn(): void {
    this._service.zoom(MapZoomDirection.In);
  }

  zoomOut(): void {
    this._service.zoom(MapZoomDirection.Out);
  }

  panUp(): void {
    this._service.pan(MapPanDirection.Up);
  }

  panRight(): void {
    this._service.pan(MapPanDirection.Right);
  }

  panDown(): void {
    this._service.pan(MapPanDirection.Down);
  }

  panLeft(): void {
    this._service.pan(MapPanDirection.Left);
  }

  back(): void {
    this._service.viewBack();
  }

  forward(): void {
    this._service.viewForward();
  }
}
