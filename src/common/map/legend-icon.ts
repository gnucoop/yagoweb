import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import {
  IMapLayer, IMapLayerStyle, IMapConditionalLayerStyle,
  MapService, MapUpdateLayerStyleAction,
  getFirstGeometryType, defaultMapStyle
} from '@yago/core';


interface LegendIconStyle {
  backgroundColor: string;
  borderColor: string;
  borderVisible: boolean;
  label?: string;
  size: number;
}


@Component({
  selector: 'yago-map-legend-icon',
  templateUrl: './legend-icon.html',
  styleUrls: ['./legend-icon.scss']
})
export class MapLegendIconComponent implements OnInit {
  private _featuresType: string | null;
  get featuresType(): string | null { return this._featuresType; }

  private _styles: LegendIconStyle[];
  get styles(): LegendIconStyle[] {
    return this._styles;
  }

  private _layer: IMapLayer;
  get layer(): IMapLayer { return this._layer; }
  @Input() set layer(layer: IMapLayer) {
    this._layer = layer;
    this._updateIcon();
  }

  private _styleUpdateSub: Subscription;

  constructor(private _service: MapService) {
    this._styleUpdateSub = _service
      .layerStyleUpdates()
      .subscribe(l => {
        if (this.layer != null && l.uniqueId === this.layer.uniqueId) {
          this.layer = l;
        }
      });
  }

  ngOnInit(): void {
    this._updateIcon();
  }

  private _updateIcon(): void {
    if (this._layer == null && this.layer.features == null) {
      this._featuresType = null;
      this._styles = [];
    } else {
      const t: string | null = getFirstGeometryType(
        <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>this.layer.features
      );
      this._featuresType = t ? t.toLowerCase() : null;
      if (this.layer.style != null) {
        const cs = <IMapConditionalLayerStyle>this.layer.style;
        if (cs.prop != null) {
          const stylesLast = cs.styles.length - 1;
          const sizeStep = 0.7 / stylesLast;
          const styles: LegendIconStyle[] = [];
          const stylesNum: number = cs.styles.length;
          for (let i = 0 ; i < stylesNum ; i++) {
            const s = cs.styles[i];
            const range = s.range;
            const label = typeof range === 'string' ? range :
              `${parseFloat(range[0]).toFixed(3)} - ${parseFloat(range[1]).toFixed(3)}`;
            styles.push({
              backgroundColor: s.style.fillColor,
              borderColor: s.style.color,
              borderVisible: s.style.stroke,
              label: label,
              size: cs.proportionalLegendSize ? 0.3 + sizeStep * i : 1
            });
          }
          this._styles = styles;
        } else {
          const fs = <IMapLayerStyle>this.layer.style;
          this._styles = [{
            backgroundColor: fs.fillColor,
            borderColor: fs.color,
            borderVisible: fs.stroke,
            size: 1
          }];
        }
      } else {
        this._styles = [{
          backgroundColor: defaultMapStyle.fillColor,
          borderColor: defaultMapStyle.color,
          borderVisible: defaultMapStyle.stroke,
          size: 1
        }];
      }
    }
  }
}
