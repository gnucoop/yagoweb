import { Component, EventEmitter, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

import { IMapLayer, MapService } from '@yago/core';

import { MapChangeLayerLabelDialogComponent } from './change-layer-label-dialog';


@Component({
  selector: 'yago-map-layers-control',
  templateUrl: './layers-control.html',
  styleUrls: ['./layers-control.scss']
})
export class MapLayersControlComponent implements OnDestroy {
  showLayersPanel = false;

  private _layers: IMapLayer[];
  get layers(): IMapLayer[] { return this._layers; }

  private _selectedLayer: Observable<IMapLayer | null>;
  get selectedLayer(): Observable<IMapLayer | null> { return this._selectedLayer; }

  private _layersSub: Subscription;
  private _dialogRef: MatDialogRef<MapChangeLayerLabelDialogComponent>;
  private _dialogSub: Subscription;

  constructor(private _service: MapService, private _dialog: MatDialog) {
    this._layersSub = _service.getLayers()
      .filter((layers: IMapLayer[]) => layers != null)
      .map((layers: IMapLayer[]) => layers.slice(0).sort((l1, l2) => l1.zIndex - l2.zIndex))
      .subscribe((ls: IMapLayer[]) => this._layers = ls );
    this._selectedLayer = _service.getSelectedLayer();
  }

  toggleLayerVisibility(layer: IMapLayer): void {
    this._service.toggleLayerVisibility(layer);
  }

  removeLayer(layer: IMapLayer): void {
    this._service.removeLayer(layer);
  }

  onLayerReorder(): void {
    this._service.updateLayersOrder(this._layers.map((l, idx) => {
      return {layer: l.uniqueId, zIndex: idx};
    }));
  }

  selectLayer(layer: IMapLayer): void {
    this._service.selectLayer(layer);
  }

  changeLayerLabel(layer: IMapLayer): void {
    this._dialogRef = this._dialog.open(MapChangeLayerLabelDialogComponent);
    this._dialogRef.componentInstance.layer = layer;
    this._dialogSub = this._dialogRef.afterClosed()
      .subscribe((label: string) => {
        if (label != null) {
          this._service.changeLayerLabel(layer, label);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._dialogSub != null) { this._dialogSub.unsubscribe(); }
  }

  private _destroyDialog(): void {
    if (this._dialogSub != null) { this._dialogSub.unsubscribe(); }
    if (this._dialogRef != null) { this._dialogRef.close(); }
  }
}
