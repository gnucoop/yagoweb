import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { IMapLayer } from '@yago/core';


@Component({
  selector: 'yago-map-change-layer-label-dialog',
  templateUrl: './change-layer-label-dialog.html',
  styleUrls: ['./change-layer-label-dialog.scss']
})
export class MapChangeLayerLabelDialogComponent {
  layerLabel = '';

  private _layer: IMapLayer;
  get layer(): IMapLayer { return this._layer; }
  set layer(layer: IMapLayer) {
    this._layer = layer;
    if (layer != null) {
      this.layerLabel = layer.label;
    }
  }

  constructor(public dialogRef: MatDialogRef<MapChangeLayerLabelDialogComponent>) { }

  close(save = false): void {
    this.dialogRef.close(save ? this.layerLabel : null);
  }
}
