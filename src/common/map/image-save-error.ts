import { Component } from '@angular/core';


@Component({
  template: '<ng-container i18n>There was an error saving your image</ng-container>',
  styles: [':host { color: #ffffff; }']
})
export class MapImageSaveErrorComponent { }
