import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-loading-dialog',
  templateUrl: './loading-dialog.html',
  styleUrls: ['./loading-dialog.scss']
})
export class LoadingDialogComponent {
  constructor(public dialogRef: MatDialogRef<LoadingDialogComponent>) { }
}
