import {
  Component, ElementRef, forwardRef, HostListener, Input, NgZone,
  OnInit, OnDestroy, Optional, ViewContainerRef
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Dir } from '@angular/cdk/bidi';
import {
  OriginConnectionPosition, Overlay, OverlayConfig, OverlayConnectionPosition, OverlayRef
} from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/first';

import * as Color from 'color';

import { ColorModel, ColorParam } from '@yago/core';

import {
  ColorPickerOverlayComponent, ColorPickerPosition, MdColorPickerInputInvalidPositionError
} from './color-picker-overlay';
import { ColorPickerComponent } from './color-picker';


const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;

const noop = () => {};

export const MD_COLOR_PICKER_VALUE_ACCESSOR: any = { /* tslint:disable:no-use-before-declare */
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ColorPickerInputComponent),
  multi: true
};

export type ColorPickerOutputType = 'string' | 'color';
export type ColorPickerViewMode = 'input' | 'button';

@Component({
  selector: 'yago-color-picker-input',
  templateUrl: './color-picker-input.html',
  styleUrls: ['./color-picker-input.scss'],
  providers: [MD_COLOR_PICKER_VALUE_ACCESSOR],
})
export class ColorPickerInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
  _overlayRef: OverlayRef | null;
  _colorPickerOverlay: ColorPickerOverlayComponent | null;

  private _position: ColorPickerPosition = 'below';
  get position(): ColorPickerPosition { return this._position; }
  @Input() set position(position: ColorPickerPosition) { this._position = position; }

  @Input() showDelay = 0;
  @Input() hideDelay = 0;
  @Input() viewMode: ColorPickerViewMode = 'input';

  private _outputType: ColorPickerOutputType = 'string';
  get outputType(): ColorPickerOutputType { return this._outputType; }
  @Input() set outputType(outputType: ColorPickerOutputType) {
    this._outputType = outputType;
    this._emitValue();
  }

  private _outputColorModel: ColorModel = 'rgb';
  @Input() set outputColorModel(outputColorModel: ColorModel) {
    this._outputColorModel = outputColorModel;
    this._emitValue();
  }

  private _color: Color;

  private _value: Color | string | null;
  get value(): Color | string | null { return this._value; }
  @Input() set value(color: Color | string | null) {
    if (typeof color === 'string') {
      color = colorConstructor(color);
    }
    if (color != null && typeof color !== 'string') {
      const c = <Color>color;
      if (
        c.red() !== this._color.red() || c.green() !== this._color.green() ||
        c.blue() !== this._color.blue() || c.alpha() !== this._color.alpha()
      ) {
        this._color = c;
        this._emitValue();
      }
    }
  }

  private _colorString = '';
  get colorString(): string { return this._colorString; }

  private _disabled = false;
  get disabled(): boolean { return this._disabled; }
  @Input() set disabled(disabled: boolean) {
    this._disabled = disabled != null && `${disabled}` !== 'false';
  }

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  private _closeSubscription: Subscription;
  private _colorSubscription: Subscription;

  constructor(
    private _overlay: Overlay,
    private _elementRef: ElementRef,
    private _viewContainerRef: ViewContainerRef,
    private _ngZone: NgZone,
    @Optional() private _dir: Dir
  ) { }

  writeValue(value: string | Color) {
    this._color = typeof value === 'string' ? colorConstructor(value) : value;
    this._updateColorString();
  }

  registerOnChange(fn: (value: any) => void): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: () => any): void {
    this._onTouchedCallback = fn;
  }

  @HostListener('longpress')
  @HostListener('click')
  show(delay: number = this.showDelay): void {
    if (!this._colorPickerOverlay) {
      this._createColorPicker();
    }
    (<ColorPickerOverlayComponent>this._colorPickerOverlay).show(this._position, delay);

    const s = this._ngZone.onMicrotaskEmpty.first().subscribe(() => {
      s.unsubscribe();
      if (this._colorPickerOverlay) {
        (<OverlayRef>this._overlayRef).updatePosition();
      }
    });
  }

  hide(delay: number = this.hideDelay): void {
    if (this._colorPickerOverlay) {
      this._colorPickerOverlay.hide(delay);
    }
  }

  ngOnInit(): void {
    this._updateColorString();
  }

  ngOnDestroy(): void {
    if (this._colorPickerOverlay) {
      this._disposeColorPicker();
    }
  }

  private _createColorPicker(): void {
    this._createOverlay();
    const portal = new ComponentPortal(ColorPickerOverlayComponent, this._viewContainerRef);
    const overlayRef = <OverlayRef>this._overlayRef;
    this._colorPickerOverlay = overlayRef.attach(portal).instance;
    const colorPickerOverlay = <ColorPickerOverlayComponent>this._colorPickerOverlay;
    colorPickerOverlay.colorPicker.color = this._color;
    this._closeSubscription = colorPickerOverlay.close
      .subscribe(() => this._disposeColorPicker());
    this._colorSubscription = colorPickerOverlay.colorSelected
      .subscribe((color: Color) => {
        this._color = color;
        this._emitValue();
      });
  }

  private _createOverlay(): void {
    const origin = this._getOrigin();
    const position = this._getOverlayPosition();
    const strategy = this._overlay.position().global()
      .centerHorizontally()
      .centerVertically();
    const config = new OverlayConfig();
    config.positionStrategy = strategy;
    config.hasBackdrop = true;
    this._overlayRef = this._overlay.create(config);
  }

  private _disposeColorPicker(): void {
    if (this._overlayRef != null) {
      this._overlayRef.dispose();
      this._overlayRef = null;
      this._colorPickerOverlay = null;
    }
  }

  _getOrigin(): OriginConnectionPosition {
    if (this.position === 'above' || this.position === 'below') {
      return {originX: 'center', originY: this.position === 'above' ? 'top' : 'bottom'};
    }

    const isDirectionLtr = !this._dir || this._dir.value === 'ltr';
    if (this.position === 'left' ||
        this.position === 'before' && isDirectionLtr ||
        this.position === 'after' && !isDirectionLtr) {
      return {originX: 'start', originY: 'center'};
    }

    if (this.position === 'right' ||
        this.position === 'after' && isDirectionLtr ||
        this.position === 'before' && !isDirectionLtr) {
      return {originX: 'end', originY: 'center'};
    }

    throw new MdColorPickerInputInvalidPositionError(this.position);
  }

  _getOverlayPosition(): OverlayConnectionPosition {
    if (this.position === 'above') {
      return {overlayX: 'center', overlayY: 'bottom'};
    }

    if (this.position === 'below') {
      return {overlayX: 'center', overlayY: 'top'};
    }

    const isLtr = !this._dir || this._dir.value === 'ltr';
    if (this.position === 'left' ||
        this.position === 'before' && isLtr ||
        this.position === 'after' && !isLtr) {
      return {overlayX: 'end', overlayY: 'center'};
    }

    if (this.position === 'right' ||
        this.position === 'after' && isLtr ||
        this.position === 'before' && !isLtr) {
      return {overlayX: 'start', overlayY: 'center'};
    }

    throw new MdColorPickerInputInvalidPositionError(this.position);
  }

  private _emitValue(): void {
    const outputColor = this._color != null && this._color[this._outputColorModel] != null ?
      this._color[this._outputColorModel]() : null;
    if (this._outputType === 'string') {
      this._value = outputColor != null ? (<Color>outputColor).string() : null;
    } else {
      this._value = <Color>outputColor;
    }
    this._updateColorString();
    this._onChangeCallback(this._value);
  }

  private _updateColorString(): void {
    this._colorString = this._color != null ? this._color.rgb().string() : '';
  }
}
