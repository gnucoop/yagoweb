import {
  Component, EventEmitter, Optional, Output, ViewChild,
  animate, state, style, transition, trigger
} from '@angular/core';
import { Dir } from '@angular/cdk/bidi';

import { Observable } from 'rxjs/Observable';

import * as Color from 'color';

import { ColorPickerComponent } from './color-picker';


export type ColorPickerVisibility = 'initial' | 'visible' | 'hidden';
export type ColorPickerPosition = 'left' | 'right' | 'above' | 'below' | 'before' | 'after';


export class MdColorPickerInputInvalidPositionError extends Error {
  constructor(position: string) {
    super(`Color picker position "${position}" is invalid.`);
  }
}


@Component({
  selector: 'yago-color-picker-overlay',
  templateUrl: './color-picker-overlay.html',
  styleUrls: ['./color-picker-overlay.scss'],
  animations: [
    trigger('state', [
      state('void', style({transform: 'scale(0)'})),
      state('initial', style({transform: 'scale(0)'})),
      state('visible', style({transform: 'scale(1)'})),
      state('hidden', style({transform: 'scale(0)'})),
      transition('* => visible', animate('150ms cubic-bezier(0.0, 0.0, 0.2, 1)')),
      transition('* => hidden', animate('150ms cubic-bezier(0.4, 0.0, 1, 1)')),
    ])
  ],
})
export class ColorPickerOverlayComponent {
  @ViewChild(ColorPickerComponent) colorPicker: ColorPickerComponent;

  private _showTimeoutId: any;
  private _hideTimeoutId: any;
  private _visibility: ColorPickerVisibility = 'initial';
  private _transformOrigin = 'bottom';

  private _colorSelectedEvent: EventEmitter<Color> = new EventEmitter<Color>();
  private _colorSelected: Observable<Color> = this._colorSelectedEvent.asObservable();
  get colorSelected(): Observable<Color> { return this._colorSelected; }

  private _closeEvent: EventEmitter<void> = new EventEmitter<void>();
  private _close: Observable<void> = this._closeEvent.asObservable();
  get close(): Observable<void> { return this._close; }

  constructor(
    @Optional() private _dir: Dir
  ) { }

  show(position: ColorPickerPosition, delay: number): void {
    if (this._hideTimeoutId) {
    }

    this._setTransformOrigin(position);
    this._showTimeoutId = setTimeout(() => {
      this._visibility = 'visible';
    }, delay);
  }

  hide(delay: number): void {
    if (this._showTimeoutId) {
      clearTimeout(this._showTimeoutId);
    }

    this._hideTimeoutId = setTimeout(() => {
      this._visibility = 'hidden';
    }, delay);
  }

  isVisible(): boolean {
    return this._visibility === 'visible';
  }

  selectColor(): void {
    if (this.colorPicker != null) {
      this.colorPicker.addCurrentColorToHistory();
      this._colorSelectedEvent.emit(this.colorPicker.color);
      this.closeMe();
    }
  }

  closeMe(): void {
    this._closeEvent.emit();
  }

  _setTransformOrigin(value: ColorPickerPosition) {
    const isLtr = !this._dir || this._dir.value === 'ltr';
    switch (value) {
      case 'before': this._transformOrigin = isLtr ? 'right' : 'left'; break;
      case 'after':  this._transformOrigin = isLtr ? 'left' : 'right'; break;
      case 'left':   this._transformOrigin = 'right'; break;
      case 'right':  this._transformOrigin = 'left'; break;
      case 'above':    this._transformOrigin = 'bottom'; break;
      case 'below': this._transformOrigin = 'top'; break;
      default: throw new MdColorPickerInputInvalidPositionError(value);
    }
  }
}
