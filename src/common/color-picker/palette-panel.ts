import { Component, EventEmitter, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import * as Color from 'color';

import { ColorParam } from '@yago/core';

const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


const palette = [
  [
    'rgb(255, 204, 204)', 'rgb(255, 230, 204)', 'rgb(255, 255, 204)', 'rgb(204, 255, 204)',
    'rgb(204, 255, 230)', 'rgb(204, 255, 255)', 'rgb(204, 230, 255)', 'rgb(204, 204, 255)',
    'rgb(230, 204, 255)', 'rgb(255, 204, 255)'
  ],
  [
    'rgb(255, 153, 153)', 'rgb(255, 204, 153)', 'rgb(255, 255, 153)', 'rgb(153, 255, 153)',
    'rgb(153, 255, 204)', 'rgb(153, 255, 255)', 'rgb(153, 204, 255)', 'rgb(153, 153, 255)',
    'rgb(204, 153, 255)', 'rgb(255, 153, 255)'
  ],
  [
    'rgb(255, 102, 102)', 'rgb(255, 179, 102)', 'rgb(255, 255, 102)', 'rgb(102, 255, 102)',
    'rgb(102, 255, 179)', 'rgb(102, 255, 255)', 'rgb(102, 179, 255)', 'rgb(102, 102, 255)',
    'rgb(179, 102, 255)', 'rgb(255, 102, 255)'
  ],
  [
    'rgb(255, 51, 51)', 'rgb(255, 153, 51)', 'rgb(255, 255, 51)', 'rgb(51, 255, 51)',
    'rgb(51, 255, 153)', 'rgb(51, 255, 255)', 'rgb(51, 153, 255)', 'rgb(51, 51, 255)',
    'rgb(153, 51, 255)', 'rgb(255, 51, 255)'
  ],
  [
    'rgb(255, 0, 0)', 'rgb(255, 128, 0)', 'rgb(255, 255, 0)', 'rgb(0, 255, 0)',
    'rgb(0, 255, 128)', 'rgb(0, 255, 255)', 'rgb(0, 128, 255)', 'rgb(0, 0, 255)',
    'rgb(128, 0, 255)', 'rgb(255, 0, 255)'
  ],
  [
    'rgb(245, 0, 0)', 'rgb(245, 123, 0)', 'rgb(245, 245, 0)', 'rgb(0, 245, 0)',
    'rgb(0, 245, 123)', 'rgb(0, 245, 245)', 'rgb(0, 123, 245)', 'rgb(0, 0, 245)',
    'rgb(123, 0, 245)', 'rgb(245, 0, 245)'
  ],
  [
    'rgb(214, 0, 0)', 'rgb(214, 108, 0)', 'rgb(214, 214, 0)', 'rgb(0, 214, 0)',
    'rgb(0, 214, 108)', 'rgb(0, 214, 214)', 'rgb(0, 108, 214)', 'rgb(0, 0, 214)',
    'rgb(108, 0, 214)', 'rgb(214, 0, 214)'
  ],
  [
    'rgb(163, 0, 0)', 'rgb(163, 82, 0)', 'rgb(163, 163, 0)', 'rgb(0, 163, 0)',
    'rgb(0, 163, 82)', 'rgb(0, 163, 163)', 'rgb(0, 82, 163)', 'rgb(0, 0, 163)',
    'rgb(82, 0, 163)', 'rgb(163, 0, 163)'
  ],
  [
    'rgb(92, 0, 0)', 'rgb(92, 46, 0)', 'rgb(92, 92, 0)', 'rgb(0, 92, 0)',
    'rgb(0, 92, 46)', 'rgb(0, 92, 92)', 'rgb(0, 46, 92)', 'rgb(0, 0, 92)',
    'rgb(46, 0, 92)', 'rgb(92, 0, 92)'
  ],
  [
    'rgb(255, 255, 255)', 'rgb(205, 205, 205)', 'rgb(178, 178, 178)', 'rgb(153, 153, 153)',
    'rgb(127, 127, 127)', 'rgb(102, 102, 102)', 'rgb(76, 76, 76)', 'rgb(51, 51, 51)',
    'rgb(25, 25, 25)', 'rgb(0, 0, 0)'
  ]
];


@Component({
  selector: 'yago-palette-panel',
  templateUrl: './palette-panel.html',
  styleUrls: ['./palette-panel.scss']
})
export class PalettePanelComponent {
  private _palette: string[][] = palette;
  get palette(): string[][] { return this._palette; }

  private _colorSelectedEvent: EventEmitter<Color> = new EventEmitter<Color>();

  private _colorSelected: Observable<Color> = this._colorSelectedEvent.asObservable();
  @Output() get colorSelected(): Observable<Color> { return this._colorSelected; }

  selectColor(color: string): void {
    this._colorSelectedEvent.emit(colorConstructor(color));
  }
}
