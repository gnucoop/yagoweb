import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import * as Color from 'color';

import { ColorParam } from '@yago/core';

const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


@Component({
  selector: 'yago-gradient-panel',
  templateUrl: './gradient-panel.html',
  styleUrls: ['./gradient-panel.scss']
})
export class GradientPanelComponent implements AfterViewInit {
  @ViewChild('spectrumContainer') spectrumContainer: ElementRef;
  @ViewChild('hueContainer') hueContainer: ElementRef;
  @ViewChild('alphaContainer') alphaContainer: ElementRef;
  @ViewChild('spectrumCanvas') spectrumCanvas: ElementRef;
  @ViewChild('hueCanvas') hueCanvas: ElementRef;
  @ViewChild('alphaCanvas') alphaCanvas: ElementRef;

  private _height = 256;

  private _alphaPosition = '0';
  get alphaPosition(): string { return this._alphaPosition; }
  private _huePosition = '0';
  get huePosition(): string { return this._huePosition; }
  private _spectrumXPosition = '0';
  get spectrumXPosition(): string { return this._spectrumXPosition; }
  private _spectrumYPosition = '0';
  get spectrumYPosition(): string { return this._spectrumYPosition; }

  private _color: Color;
  get color(): Color { return this._color; }
  @Input() set color(color: Color) {
    this._updateCurrentColor(color);
  }

  private _colorSelectedEvent: EventEmitter<Color> = new EventEmitter<Color>();

  private _colorSelected: Observable<Color> = this._colorSelectedEvent.asObservable();
  @Output() get colorSelected(): Observable<Color> { return this._colorSelected; }

  ngAfterViewInit(): void {
    this._drawSpectrumCanvas();
    this._drawHueCanvas();
    this._drawAlphaCanvas();
    setTimeout(() => {
      this._updateAlphaPosition();
      this._updateHuePosition();
      this._updateSpectrumPosition();
    }, 100);
  }

  changeColor(origin: string, evt: MouseEvent): void {
    evt.preventDefault();
    evt.stopImmediatePropagation();

    const offsetX = evt.offsetX;
    const offsetY = evt.offsetY;

    switch (origin) {
      case 'spectrum':
      this._colorSelectedEvent.emit(this._getSpectrumValue(offsetX, offsetY));
      break;
      case 'hue':
      const newHue = this._getHueValue(offsetY);
      const s = this._color.saturationl();
      const l = this._color.lightness();
      this._colorSelectedEvent.emit(colorConstructor(`hsl(${newHue}, ${s}%, ${l}%)`).rgb());
      break;
      case 'alpha':
      this._colorSelectedEvent.emit(this._color.alpha(this._getAlphaValue(offsetY)));
      break;
    }
  }

  redraw(): void {
    this._drawSpectrumCanvas();
    this._drawAlphaCanvas();
    this._updateHuePosition();
    this._updateAlphaPosition();
    this._updateSpectrumPosition();
  }

  private _updateCurrentColor(color: Color): void {
    this._color = color;

    this.redraw();
  }

  private _updateSpectrumPosition(): void {
    if (this.spectrumContainer == null) { return; }
    const s = this._color.saturationv() / 100;
    const v = this._color.value() / 100;
    const cw = this.spectrumContainer.nativeElement.clientWidth;
    const ch = this.spectrumContainer.nativeElement.clientHeight;
    this._spectrumXPosition = `${cw * s}px`;
    this._spectrumYPosition = `${ch - ch * v}px`;
  }

  private _updateAlphaPosition(): void {
    if (this.alphaContainer == null) { return; }
    const alpha = this._color.alpha();
    const ch = this.alphaContainer.nativeElement.clientHeight;
    const newY = ch * (1 - alpha);
    this._alphaPosition = `${newY}px`;
  }

  private _updateHuePosition(): void {
    if (this.hueContainer == null) { return; }
    const hue = this._color.hue();
    const ch = this.hueContainer.nativeElement.clientHeight;
    this._huePosition = `${ch - hue / 360 * ch}px`;
  }

  private _getSpectrumValue(x: number, y: number): Color {
    if (this.spectrumContainer == null || this.spectrumCanvas == null) { return colorConstructor('#ff0000'); }
    const ctx = this.spectrumCanvas.nativeElement.getContext('2d');
    const newX = Math.floor(ctx.canvas.width / this.spectrumContainer.nativeElement.clientWidth * x);
    const newY = Math.floor(ctx.canvas.height / this.spectrumContainer.nativeElement.clientHeight * y);
    const imageData = ctx.getImageData(newX, newY, 1, 1).data;
    return colorConstructor(`rgb(${imageData[0]}, ${imageData[1]}, ${imageData[2]})`);
  }

  private _getHueValue(y: number): number {
    if (this.hueContainer == null || this.hueCanvas == null) { return 0; }
    const ctx = this.hueCanvas.nativeElement.getContext('2d');
    const newY = Math.round(ctx.canvas.height / this.alphaContainer.nativeElement.clientHeight * y);
    if (newY === 0) { return 0; }
    if (newY === ctx.canvas.height) { return 359; }
    const imageData = ctx.getImageData(0, newY, 1, 1).data;
    return colorConstructor(`rgb(${imageData[0]}, ${imageData[1]}, ${imageData[2]})`).hue();
  }

  private _getAlphaValue(y: number): number {
    if (this.alphaContainer == null || this.alphaCanvas == null) { return 1; }
    const ctx = this.alphaCanvas.nativeElement.getContext('2d');
    const newY = Math.round(ctx.canvas.height / this.alphaContainer.nativeElement.clientHeight * y);
    if (newY === 0) { return 1; }
    if (newY === ctx.canvas.height) { return 0; }
    const imageData = ctx.getImageData(0, newY, 1, 1).data;
    return imageData[3] / 255;
  }

  private _getCanvasFromString(canvas: string): any {
    switch (canvas) {
      case 'spectrum':
      return this.spectrumCanvas.nativeElement;
      case 'hue':
      return this.hueCanvas.nativeElement;
      case 'alpha':
      return this.alphaCanvas.nativeElement;
      default:
      return null;
    }
  }

  private _drawSpectrumCanvas(): void {
    if (this.spectrumCanvas == null) { return; }
    const canvas = this.spectrumCanvas.nativeElement;
    const ctx = canvas.getContext('2d');

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    const curHue = this._color.hue();

    const whiteGradient = ctx.createLinearGradient(0, 0, ctx.canvas.width, 0);
    whiteGradient.addColorStop(0, 'rgba(255, 255, 255, 1.0)');
    whiteGradient.addColorStop(1, 'rgba(255, 255, 255, 0.0)');

    const blackGradient = ctx.createLinearGradient(0, 0, 0, ctx.canvas.height);
    blackGradient.addColorStop(0, 'rgba(0, 0, 0, 0.0)');
    blackGradient.addColorStop(1, 'rgba(0, 0, 0, 1.0)');

    ctx.fillStyle = `hsl(${curHue}, 100%, 50%)`;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    ctx.fillStyle = whiteGradient;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    ctx.fillStyle = blackGradient;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  private _drawHueCanvas(): void {
    if (this.hueCanvas == null) { return; }
    const canvas = this.hueCanvas.nativeElement;
    const ctx = canvas.getContext('2d');
    const gradient = ctx.createLinearGradient(0, 0, 0, ctx.canvas.height);
    gradient.addColorStop(0,     'rgb(255,   0,   0)');
    gradient.addColorStop(0.167, 'rgb(255,   0, 255)');
    gradient.addColorStop(0.333, 'rgb(0,     0, 255)');
    gradient.addColorStop(0.500, 'rgb(0,   255, 255)');
    gradient.addColorStop(0.666, 'rgb(0,   255, 0  )');
    gradient.addColorStop(0.828, 'rgb(255, 255, 0  )');
    gradient.addColorStop(0.999, 'rgb(255,   0, 0  )');
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  private _drawAlphaCanvas(): void {
    if (this.alphaCanvas == null) { return; }
    const canvas = this.alphaCanvas.nativeElement;
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    const gradient = ctx.createLinearGradient(0, 0, 0, ctx.canvas.height);
    const curColor = (<any>this._color).rgb();
    gradient.addColorStop(0.01,  curColor.alpha(1.0).string());
    gradient.addColorStop(0.999, curColor.alpha(0.0).string());
    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }
}
