import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import * as Color from 'color';

import { ColorModel } from '@yago/core';


interface ValuesPanelSlider {
  label: string;
  minValue: number;
  maxValue: number;
  step: number;
  property: string;
  value?: number;
  valueTx?: (val: number) => string;
}

function numToHex(val: number): string {
  const str = val.toString(16).toUpperCase();
  if (str.length === 2) { return str; }
  return `0${str}`;
}

function alphaToHex(val: number): string {
  return numToHex(Math.round(val * 255));
}

const hexSliders: ValuesPanelSlider[] = [
  {
    label: 'Red', minValue: 0, maxValue: 255, step: 1, property: 'red', value: undefined,
    valueTx: numToHex
  },
  {
    label: 'Green', minValue: 0, maxValue: 255, step: 1, property: 'green', value: undefined,
    valueTx: numToHex
  },
  {
    label: 'Blue', minValue: 0, maxValue: 255, step: 1, property: 'blue', value: undefined,
    valueTx: numToHex
  },
  {
    label: 'Alpha', minValue: 0, maxValue: 1, step: 0.05, property: 'alpha', value: undefined,
    valueTx: alphaToHex
  }
];

const rgbSliders: ValuesPanelSlider[] = [
  { label: 'Red', minValue: 0, maxValue: 255, step: 1, property: 'red', value: undefined },
  { label: 'Green', minValue: 0, maxValue: 255, step: 1, property: 'green', value: undefined },
  { label: 'Blue', minValue: 0, maxValue: 255, step: 1, property: 'blue', value: undefined },
  { label: 'Alpha', minValue: 0, maxValue: 1, step: 0.05, property: 'alpha', value: undefined }
];

const hslSliders: ValuesPanelSlider[] = [
  { label: 'Hue', minValue: 0, maxValue: 359, step: 1, property: 'hue', value: undefined },
  { label: 'Saturation', minValue: 0, maxValue: 100, step: 1, property: 'saturationl', value: undefined },
  { label: 'Lightness', minValue: 0, maxValue: 100, step: 1, property: 'lightness', value: undefined },
  { label: 'Alpha', minValue: 0, maxValue: 1, step: 0.05, property: 'alpha', value: undefined }
];


@Component({
  selector: 'yago-values-panel',
  templateUrl: './values-panel.html',
  styleUrls: ['./values-panel.scss']
})
export class ValuesPanelComponent implements OnInit {
  private _sliders: ValuesPanelSlider[];
  get sliders(): ValuesPanelSlider[] { return this._sliders; }

  private _colorModel: ColorModel = 'rgb';
  @Input() set colorModel(colorModel: ColorModel) {
    this._updateColorModel(colorModel);
  }

  private _color: Color;
  get color(): Color { return this._color; }
  @Input() set color(color: Color) {
    this._updateCurrentColor(color);
  }
  private _colorSelectedEvent: EventEmitter<Color> = new EventEmitter<Color>();

  private _colorSelected: Observable<Color> = this._colorSelectedEvent.asObservable();
  @Output() get colorSelected(): Observable<Color> { return this._colorSelected; }

  ngOnInit(): void {
    this._redrawSliders();
  }

  getSliderValue(slider: ValuesPanelSlider): number {
    return this._color[slider.property]();
  }

  onChangeSliderValue(slider: ValuesPanelSlider): void {
    const newColor = this._color[slider.property](slider.value);
    this._colorSelectedEvent.emit(newColor);
  }

  private _redrawSliders(): void {
    if (this._color == null) { return; }
    let newSliders: ValuesPanelSlider[];
    switch (this._colorModel) {
      case 'hex':
      newSliders = hexSliders.slice(0);
      break;
      case 'rgb':
      newSliders = rgbSliders.slice(0);
      break;
      case 'hsl':
      newSliders = hslSliders.slice(0);
      break;
      default:
      newSliders = [];
    }
    newSliders.forEach((s) => {
      s.value = this._color[s.property]();
    });
    this._sliders = newSliders;
  }

  private _updateColorModel(colorModel: ColorModel): void {
    this._colorModel = colorModel;
    this._redrawSliders();
  }

  private _updateCurrentColor(color: Color): void {
    this._color = color;
    this._redrawSliders();
  }
}
