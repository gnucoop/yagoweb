import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import * as Color from 'color';

import { ColorParam } from '@yago/core';


const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


@Component({
  selector: 'yago-history-panel',
  templateUrl: './history-panel.html',
  styleUrls: ['./history-panel.scss']
})
export class HistoryPanelComponent {
  private _colorSelectedEvent: EventEmitter<Color> = new EventEmitter<Color>();

  @Input()
  history: Color[] = [];

  private _colorSelected: Observable<Color> = this._colorSelectedEvent.asObservable();
  @Output() get colorSelected(): Observable<Color> { return this._colorSelected; }

  selectColor(color: string): void {
    this._colorSelectedEvent.emit(colorConstructor(color));
  }
}
