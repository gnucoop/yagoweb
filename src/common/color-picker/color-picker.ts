import { Component, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';

import * as Color from 'color';

import { ColorModel, ColorParam } from '@yago/core';

import { GradientPanelComponent } from './gradient-panel';

const colorHistory: Color[] = [];
const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


@Component({
  selector: 'yago-color-picker',
  templateUrl: './color-picker.html',
  styleUrls: ['./color-picker.scss']
})
export class ColorPickerComponent {
  @ViewChild(GradientPanelComponent) gradientPanel: GradientPanelComponent;

  private _colorModels: ColorModel[] = [
    'hex',
    'rgb',
    'hsl'
  ];
  get colorModels(): ColorModel[] { return this._colorModels; }

  private _colorModel: ColorModel = this._colorModels[0];
  get colorModel(): ColorModel { return this._colorModel; }

  private _color: Color = colorConstructor('#ff0000', 'hex');
  get color(): Color { return this._color; }
  @Input() set color(color: Color) {
    if (color != null) {
      this._color = color;
    }
  }

  get history(): Color[] {
    return colorHistory;
  }

  inverseColor: Color = colorConstructor('#00ffff', 'hex');

  addCurrentColorToHistory(): void {
    this.addColorToHistory(this._color);
  }

  addColorToHistory(color: Color): void {
    colorHistory.push(color);
  }

  getColorString(model: string): string {
    return (<any>this._color)[model]();
  }

  onPanelSelectChange(evt: MatTabChangeEvent) {
    if (this.gradientPanel != null && evt.index === 0) {
      this.gradientPanel.redraw();
    }
  }

  updateCurrentModel(evt: MatTabChangeEvent) {
    this._colorModel = this._colorModels[evt.index];
  }

  changeColor(color: Color) {
    this.color = color;
    const r = Math.round(255 - color.red());
    const g = Math.round(255 - color.green());
    const b = Math.round(255 - color.blue());
    this.inverseColor = colorConstructor(`rgb(${r}, ${g}, ${b})`);
  }
}
