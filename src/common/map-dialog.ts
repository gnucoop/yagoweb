import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { IMap } from '@yago/core';


@Component({
  selector: 'yago-map-dialog',
  templateUrl: './map-dialog.html',
  styleUrls: ['./map-dialog.scss']
})
export class MapDialogComponent {
  private _map: IMap;
  get map(): IMap { return this._map; }

  private _limits: number[];
  get limits(): number[] { return this._limits; }

  constructor(public dialogRef: MatDialogRef<MapDialogComponent>) { }

  setMap(map: IMap): void {
    this._map = map;
  }

  setLimits(limits: number[]): void {
    this._limits = limits;
  }
}
