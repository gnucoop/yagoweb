import { Component, Input } from '@angular/core';

import { ILayer, IMapLayer, IWMSSourceAvabilableLayer } from '@yago/core';

type Layer = ILayer | IMapLayer | IWMSSourceAvabilableLayer;


@Component({
  selector: 'yago-layer-icon',
  templateUrl: './layer-icon.html',
  styleUrls: ['./layer-icon.scss']
})
export class LayerIconComponent {
  private _layer: Layer;
  get layer(): Layer { return this._layer; }
  @Input()
  set layer(layer: Layer) {
    this._layer = layer;
    this._updateIconName();
  }

  private _iconName = '';
  get iconName(): string { return this._iconName; }

  private _iconTooltip = '';
  get iconTooltip(): string { return this._iconTooltip; }

  private _updateIconName(): void {
    if (this._layer == null) {
      this._iconName = '';
      this._iconTooltip = '';
    }
    if (this._layer.layerType === 'VECTOR') {
      this._iconName = 'multiline_chart';
      this._iconTooltip = 'Vector layer';
    } else {
      this._iconName = 'image';
      this._iconTooltip = 'Raster layer';
    }
  }
}
