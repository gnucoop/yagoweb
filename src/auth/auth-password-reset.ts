import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { AuthService, IAuthResetPassword } from '@yago/core';


function passwordMatchValidator(control: AbstractControl): {[key: string]: any; } | null {
  const value = control.value;
  const new_password1: string = value.new_password1;
  const new_password2: string = value.new_password2;
  if (
    new_password1 != null && new_password1.length > 0 &&
    new_password2 != null && new_password2.length > 0 &&
    new_password1 !== new_password2
  ) {
    return {password_match: 'The passwords must match'};
  }
  return null;
}


@Component({
  selector: 'yago-password-reset',
  templateUrl: './auth-password-reset.html',
  styleUrls: ['./auth-password-reset.scss']
})
export class AuthPasswordResetComponent implements OnDestroy, OnInit {
  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _resetPasswordForm: FormGroup;
  get resetPasswordForm(): FormGroup { return this._resetPasswordForm; }

  private _uid: string;
  get uid(): string { return this._uid; }

  private _token: string;
  get token(): string { return this._token; }

  private _paramsSub: Subscription;

  constructor(
    private _service:  AuthService,
    private _fb: FormBuilder,
    private _route: ActivatedRoute
  ) {
    this._initForm();
  }

  ngOnInit(): void {
    this._paramsSub = this._route.params
      .subscribe(params => {
        try {
          this._uid = params['uid'];
        } catch (e) { }
        this._token = params['token'];
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSub != null) { this._paramsSub.unsubscribe(); }
  }

  resetPassword(): void {
    if (
      this.uid == null || this.token == null ||
      !this.resetPasswordForm.valid
    ) { return; }
    const fv = this.resetPasswordForm.value;
    this._service.resetPassword({
      uid: this.uid,
      token: this.token,
      new_password1: fv.new_password1,
      new_password2: fv.new_password2
    });
  }

  private _initForm(): void {
    this._resetPasswordForm = this._fb.group({
      new_password1: ['', Validators.required],
      new_password2: ['', Validators.required]
    }, {
      validator: passwordMatchValidator
    });
  }
}
