import { Component } from '@angular/core';
import { AbstractControl, FormGroup, AsyncValidatorFn, FormBuilder, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';

import {
  IAuthLoginPanel, IAuthRegistration, AuthService, getAuthLoading,
  emailValidator, passwordMatchValidator, uniqueEmailValidator, uniqueUsernameValidator
} from '@yago/core';

import { environment } from '../environments/environment';


@Component({
  selector: 'yago-login',
  templateUrl: './auth-login.html',
  styleUrls: ['./auth-login.scss']
})
export class AuthLoginComponent {
  private _loginForm: FormGroup;
  get loginForm(): FormGroup { return this._loginForm; }

  private _registrationForm: FormGroup;
  get registrationForm(): FormGroup { return this._registrationForm; }

  private _recoverPasswordForm: FormGroup;
  get recoverPasswordForm(): FormGroup { return this._recoverPasswordForm; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _loginPanel: Observable<IAuthLoginPanel>;
  get loginPanel(): Observable<IAuthLoginPanel> { return this._loginPanel; }

  private _recoverPasswordMailSent: Observable<boolean>;
  get recoverPasswordMailSent(): Observable<boolean> { return this._recoverPasswordMailSent; }

  private _recaptchaSiteKey: string = environment.recaptchaSiteKey;
  get recaptchaSiteKey(): string { return this._recaptchaSiteKey; }

  constructor(private _service: AuthService, private _fb: FormBuilder) {
    this._loading = _service.getLoading();
    this._loginPanel = _service.getCurrentLoginPanel();

    this._initLoginForm();
    this._initRegistrationForm();
    this._initRecoverPasswordForm();
  }

  login(): void {
    this._service.login(this._loginForm.value);
  }

  showRegistration(): void {
    this._service.showRegistrationPanel();
  }

  showPasswordRecovery(): void {
    this._service.showPasswordRecoveryPanel();
  }

  showLogin(): void {
    this._service.showLoginPanel();
  }

  register(): void {
    if (!this.registrationForm.valid) { return; }
    this._service.register(<IAuthRegistration>this.registrationForm.value);
  }

  recoverPassword(): void {
    if (!this.recoverPasswordForm.valid) { return; }
    this._service.recoverPassword(this.recoverPasswordForm.value.email);
  }

  private _initRecoverPasswordForm(): void {
    this._recoverPasswordForm = this._fb.group({
      email: ['', [Validators.required, emailValidator]]
    });
  }

  private _initRegistrationForm(): void {
    this._registrationForm = this._fb.group({
      username: ['', Validators.required, uniqueUsernameValidator(this._service)],
      email: ['', [Validators.required, emailValidator], uniqueEmailValidator(this._service)],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      recaptcha: [null, Validators.required]
    }, {
      validator: passwordMatchValidator
    });
  }

  private _initLoginForm(): void {
    this._loginForm = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
}
