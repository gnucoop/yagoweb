import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatIconModule,
  MatInputModule
} from '@angular/material';

import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

import { YagoCoreModule } from '@yago/core';

import { AuthLoginComponent } from './auth-login';
import { AuthLoginDialogComponent } from './auth-login-dialog';
import { AuthPasswordResetComponent } from './auth-password-reset';
import { authRoutes } from './auth.routes';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(authRoutes),
    RecaptchaFormsModule,

    MatButtonModule,
    MatIconModule,
    MatInputModule,

    RecaptchaModule,
    YagoCoreModule
  ],
  declarations: [
    AuthLoginComponent,
    AuthLoginDialogComponent,
    AuthPasswordResetComponent
  ],
  entryComponents: [
    AuthLoginDialogComponent
  ]
})
export class AuthModule { }
