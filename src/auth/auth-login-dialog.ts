import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';

import { AuthService, IAuthLoginPanel } from '@yago/core';


@Component({
  selector: 'yago-login-dialog',
  templateUrl: './auth-login-dialog.html',
  styleUrls: ['./auth-login-dialog.scss']
})
export class AuthLoginDialogComponent {
  private _loginPanel: Observable<IAuthLoginPanel>;
  get loginPanel(): Observable<IAuthLoginPanel> { return this._loginPanel; }

  constructor(
    public dialogReg: MatDialogRef<AuthLoginDialogComponent>,
    private _service: AuthService
  ) {
    this._loginPanel = _service.getCurrentLoginPanel();
  }

  close(): void {
    this._service.closeLoginDialog();
  }
}
