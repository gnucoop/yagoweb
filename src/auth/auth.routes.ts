import { Routes } from '@angular/router';

import { AuthGuard } from '@yago/core';

import { AuthPasswordResetComponent } from './auth-password-reset';


export const authRoutes: Routes = [
  { path: 'passwordReset/:uid/:token', component: AuthPasswordResetComponent }
];
