import { Routes } from '@angular/router';

import { GalleryComponent } from './gallery';


export const GALLERY_ROUTES: Routes = [
  { path: 'gallery', component: GalleryComponent }
];
