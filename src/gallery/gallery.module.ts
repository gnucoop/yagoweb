import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { GalleryThemeComponent } from './gallery-theme';
import { GalleryItemComponent } from './gallery-item';
import { GalleryPreviewDialogComponent } from './gallery-preview-dialog';
import { GalleryComponent } from './gallery';
import { GALLERY_ROUTES } from './gallery.routes';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GALLERY_ROUTES),

    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatGridListModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    GalleryThemeComponent,
    GalleryItemComponent,
    GalleryPreviewDialogComponent,
    GalleryComponent
  ],
  entryComponents: [
    GalleryPreviewDialogComponent
  ]
})
export class GalleryModule { }
