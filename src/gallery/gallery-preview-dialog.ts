import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';

import { GalleryService } from '@yago/core';


@Component({
  selector: 'yago-gallery-preview-dialog',
  templateUrl: './gallery-preview-dialog.html',
  styleUrls: ['./gallery-preview-dialog.scss']
})
export class GalleryPreviewDialogComponent {
  private _url: Observable<string | null>;
  get url(): Observable<string | null> { return this._url; }

  constructor(
    public dialogRef: MatDialogRef<GalleryPreviewDialogComponent>,
    private _service: GalleryService
  ) {
    this._url = _service.getPreviewUrl();
  }

  close(): void {
    this._service.closePreviewImage();
  }
}
