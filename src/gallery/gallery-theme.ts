import { Component, Input } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

import { IGalleryTheme, GalleryService } from '@yago/core';


@Component({
  selector: 'yago-gallery-theme',
  templateUrl: './gallery-theme.html',
  styleUrls: ['./gallery-theme.scss']
})
export class GalleryThemeComponent {
  @Input() theme: IGalleryTheme;
  @Input() parentId: number;

  private _parent: boolean;
  @Input()
  get parent(): boolean { return this._parent; }
  set parent(parent: boolean) { this._parent = coerceBooleanProperty(parent); }

  constructor(private _service: GalleryService) { }

  openTheme(): void {
    this._service.list(this.theme);
  }

  openParent(): void {
    this._service.list(this.parentId);
  }
}
