import { AfterViewInit, Component, ElementRef, OnDestroy, Renderer } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { IGalleryTheme, IGalleryItem, GalleryService } from '@yago/core';

import { GalleryPreviewDialogComponent } from './gallery-preview-dialog';


@Component({
  selector: 'yago-gallery',
  templateUrl: './gallery.html',
  styleUrls: ['./gallery.scss']
})
export class GalleryComponent implements AfterViewInit, OnDestroy {
  private _theme: Observable<IGalleryTheme | null>;
  get theme(): Observable<IGalleryTheme | null> { return this._theme; }

  private _subThemes: Observable<IGalleryTheme[]>;
  get subThemes(): Observable<IGalleryTheme[]> { return this._subThemes; }

  private _items: Observable<IGalleryItem[]>;
  get items(): Observable<IGalleryItem[]> { return this._items; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _cols = 6;
  get cols(): number { return this._cols; }

  private _listenFunc: Function;
  private _dialogRef: MatDialogRef<GalleryPreviewDialogComponent> | null;
  private _previewSub: Subscription;

  constructor(
    private _service: GalleryService,
    private _el: ElementRef,
    private _renderer: Renderer,
    private _dialog: MatDialog
  ) {
    this._theme = _service.getTheme();
    this._subThemes = _service.getSubThemes();
    this._items = _service.getItems();
    this._loading = _service.getLoading();

    _service.list();

    this._listenFunc = _renderer.listenGlobal('window', 'resize', () => this._updateColsNum());

    this._previewSub = _service.getPreviewUrl()
      .subscribe((url: string) => {
        this._destroyPreviewDialog();
        if (url != null) {
          this._dialogRef = _dialog.open(GalleryPreviewDialogComponent, {
            disableClose: true
          });
        }
      });
  }

  ngAfterViewInit(): void {
    this._updateColsNum();
  }

  ngOnDestroy() {
    if (this._listenFunc != null) { this._listenFunc(); }
    if (this._previewSub != null) { this._previewSub.unsubscribe(); }
  }

  private _destroyPreviewDialog(): void {
    if (this._dialogRef == null) { return; }
    this._dialogRef.close();
    this._dialogRef = null;
  }

  private _updateColsNum(): void {
    const width: number = this._el.nativeElement.clientWidth;
    this._cols = this._colsForWidth(width);
  }

  private _colsForWidth(width: number): number {
    const newCols = Math.floor(width / 200);
    if (newCols < 1) { return 1; }
    if (newCols > 6) { return 6; }
    return newCols;
  }
}
