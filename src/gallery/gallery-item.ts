import { Component, Input } from '@angular/core';

import { IGalleryItem, GalleryService } from '@yago/core';


@Component({
  selector: 'yago-gallery-item',
  templateUrl: './gallery-item.html',
  styleUrls: ['./gallery-item.scss']
})
export class GalleryItemComponent {
  @Input() item: IGalleryItem;

  constructor(private _service: GalleryService) { }

  preview(): void {
    if (this.item == null || this.item.attachment == null) { return; }
    this._service.previewImage(this.item);
  }
}
