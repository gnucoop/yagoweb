import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieXSRFStrategy, HttpModule, XSRFStrategy } from '@angular/http';
import { RouterModule } from '@angular/router';
import { MATERIAL_COMPATIBILITY_MODE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { DndModule } from 'ngx-dnd';

import { RecaptchaModule } from 'ng-recaptcha';

import {
  API_SERVICE_CONFIG, AUTH_CONFIG,
  ApiServiceConfig, AuthConfigOptional,
  YagoCoreModule, YagoRootModule
} from '@yago/core';

import * as Raven from 'raven-js';

import { AppComponent } from './app.component';
import { HomeComponent } from '../home/index';
import { AuthModule } from '../auth/index';
import { YagoWebCommonModule } from '../common/index';
import { MapBuilderModule } from '../map-builder/index';
import { GalleryModule } from '../gallery/index';
import { APP_ROUTES } from './app.routes';
import { environment } from '../environments/environment';


export function provideAuthConfig(): AuthConfigOptional {
  return environment.authConfig;
}

export function provideApiServiceConfig(): ApiServiceConfig {
  return environment.apiConfig;
}

export function provideXSRFStrategy(): XSRFStrategy {
  return new CookieXSRFStrategy('csrftoken', 'X-CSRFToken');
}


if (environment.production) {
  Raven
    .config(environment.sentryUrl)
    .install();
}

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err.originalError || err);
  }
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(APP_ROUTES),

    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,

    BrowserAnimationsModule,
    FlexLayoutModule,
    !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 50 }) : [],
    DndModule.forRoot(),
    RecaptchaModule.forRoot(),
    YagoCoreModule.forRoot(),
    YagoRootModule.forRoot(),
    AuthModule,
    YagoWebCommonModule,
    MapBuilderModule,
    GalleryModule
  ],
  providers: [
    { provide: MATERIAL_COMPATIBILITY_MODE, useValue: true },
    { provide: XSRFStrategy, useFactory: provideXSRFStrategy },
    { provide: API_SERVICE_CONFIG, useFactory: provideApiServiceConfig },
    { provide: AUTH_CONFIG, useFactory: provideAuthConfig },
    environment.production ? {
      provide: ErrorHandler, useClass: RavenErrorHandler
    } : []
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
