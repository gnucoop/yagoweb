import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatSidenav } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import { AuthService, getLoggedIn, getAuthUser, IUser, State } from '@yago/core';

import { environment } from '../environments/environment';
import { AuthLoginDialogComponent } from '../auth/index';


interface MenuElement {
  label: string;
  route: string;
}


@Component({
  selector: 'yago-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  @ViewChild(MatSidenav) sidenav: MatSidenav;

  private _loggedIn: Observable<boolean>;
  get loggedIn(): Observable<boolean> { return this._loggedIn; }

  private _user: Observable<IUser>;
  get user(): Observable<IUser> { return this._user; }

  private _loginDialogRef: MatDialogRef<AuthLoginDialogComponent> | null;
  private _loginDialogSub: Subscription;

  constructor(
    private _authService: AuthService,
    private _store: Store<State>,
    private _dialog: MatDialog
  ) {
    this._loggedIn = _store.select(getLoggedIn);
    this._user = _store.select(getAuthUser);
  }

  ngOnInit(): void {
    this._loginDialogSub = this._authService
      .getLoginDialogVisible()
      .subscribe((visible: boolean) => {
        if (visible) {
          this._showLoginDialog();
        } else {
          this._destroyLoginDialog();
        }
      });

    this._injectGoogleMapsScript();
  }

  ngOnDestroy(): void {
    if (this._loginDialogSub != null) { this._loginDialogSub.unsubscribe(); }
  }

  login(): void {
    this._authService.openLoginDialog();
  }

  logout(): void {
    this._authService.logout();
  }

  closeSidenav(): void {
    if (this.sidenav == null) { return; }
    setTimeout(() => {
      this.sidenav.close();
    }, 300);
  }

  private _showLoginDialog(): void {
    if (this._loginDialogRef != null) { this._destroyLoginDialog(); }
    this._loginDialogRef = this._dialog.open(AuthLoginDialogComponent);
  }

  private _destroyLoginDialog(): void {
    if (this._loginDialogRef == null) { return; }
    this._loginDialogRef.close();
    this._loginDialogRef = null;
  }

  private _injectGoogleMapsScript(): void {
    if (document == null) { return; }
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.defer = true;
    script.src = `https://maps.googleapis.com/maps/api/js?key=${environment.mapsConfig.googleApiKey}`;
    document.body.appendChild(script);
  }
}
