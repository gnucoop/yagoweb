import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import {
  getAdminThemes, getAdminThemesLoading,
  ILayerTheme, State, ThemesService
} from '@yago/core';

import { AdminThemesDeleteDialogComponent } from './themes-delete-dialog';


@Component({
  selector: 'yago-admin-themes-list',
  templateUrl: './themes-list.html',
  styleUrls: ['./themes-list.scss']
})
export class AdminThemesListComponent implements OnDestroy {
  private _themes: Observable<ILayerTheme[]>;
  get themes(): Observable<ILayerTheme[]> { return this._themes; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminThemesDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _store: Store<State>,
    private _service: ThemesService,
    private _dialog: MatDialog
  ) {
    this._themes = _store.select(getAdminThemes);
    this._loading = _store.select(getAdminThemesLoading);

    this._service.list(undefined, true);
  }

  delete(theme: ILayerTheme): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminThemesDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(theme, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
