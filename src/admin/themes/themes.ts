import { Component } from '@angular/core';


@Component({
  selector: 'yago-admin-themes',
  templateUrl: './themes.html',
  styleUrls: ['./themes.scss']
})
export class AdminThemesComponent { }
