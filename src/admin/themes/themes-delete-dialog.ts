import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-themes-delete-dialog',
  templateUrl: './themes-delete-dialog.html',
  styleUrls: ['./themes-delete-dialog.scss']
})
export class AdminThemesDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminThemesDeleteDialogComponent>) { }
}
