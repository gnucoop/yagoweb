import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';

import {
  getAdminTheme, getAdminThemesLoading,
  getAdminThemes, getAdminThemeLoading,
  Go,
  ThemesCreateAction, ThemesUpdateAction,
  ILayerTheme, State, ThemesService
} from '@yago/core';


@Component({
  selector: 'yago-admin-themes-edit',
  templateUrl: './themes-edit.html',
  styleUrls: ['./themes-edit.scss']
})
export class AdminThemesEditComponent implements OnDestroy, OnInit {
  private _theme: Observable<ILayerTheme>;
  get theme(): Observable<ILayerTheme> { return this._theme; }

  private _themes: Observable<ILayerTheme[]>;
  get themes(): Observable<ILayerTheme[]> { return this._themes; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _themeForm: FormGroup;
  get themeForm(): FormGroup { return this._themeForm; }

  private _themeId: number;
  private _saveEvent: EventEmitter<void> = new EventEmitter<void>();
  private _saveSubscription: Subscription;
  private _paramsSubscription: Subscription;
  private _themeSub: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: ThemesService,
    private _store: Store<State>,
    fb: FormBuilder
  ) {
    this._theme = _store.select(getAdminTheme).filter(t => t != null);
    this._themes = _store.select(getAdminThemes);
    this._loading = _store.select(getAdminThemesLoading).startWith(false)
      .combineLatest(_store.select(getAdminThemeLoading).startWith(false))
      .map((l: [boolean, boolean]) => l[0] || l[1]);

    this._service.list(undefined, true);

    this._themeForm = fb.group({
      name: ['', Validators.required],
      parent: [null]
    });

    this._saveSubscription = this._saveEvent
      .subscribe((e: [void, ILayerTheme]) => {
        const layer: ILayerTheme = Object.assign({}, this.themeForm.value, {
          id: this._themeId
        });
        const action = this._themeId != null ?
          new ThemesUpdateAction(layer, true) :
          new ThemesCreateAction(layer, true);
        this._store.dispatch(action);
      });

    this._themeSub = this._theme
      .subscribe((theme: ILayerTheme) => {
        this._themeForm.controls['name'].setValue(theme != null ? theme.name : null);
        this._themeForm.controls['parent'].setValue(theme != null ? theme.parent : null);
      });
  }

  ngOnInit(): void {
    this._paramsSubscription = this._route.params
      .subscribe((params: Params) => {
        const id: string = params['id'];
        if (id !== 'new') {
          this._themeId = +id;
          if (isNaN(this._themeId) || this._themeId <= 0) {
            this._store.dispatch(new Go({path: ['/admin', 'themes']}));
          } else {
            this._service.get(this._themeId, true);
          }
        } else {
          this._service.resetCurrentTheme(true);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription != null) { this._paramsSubscription.unsubscribe(); }
    if (this._saveSubscription != null) { this._saveSubscription.unsubscribe(); }
    if (this._themeSub != null) { this._themeSub.unsubscribe(); }
  }

  saveTheme(): void {
    this._saveEvent.emit();
  }
}
