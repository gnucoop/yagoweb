import { Routes } from '@angular/router';

import { AdminAuthGuard } from '@yago/core';

import { AdminComponent } from './admin';
import {
  AdminThemesComponent, AdminThemesListComponent, AdminThemesEditComponent
} from './themes/index';
import {
  AdminArcGISSourcesComponent, AdminArcGISSourcesListComponent, AdminArcGISSourcesLayersComponent,
  AdminArcGISSourcesImportLayerComponent, AdminArcGISSourcesEditComponent
} from './arcgis-sources/index';
import {
  AdminWMSSourcesComponent, AdminWMSSourcesListComponent, AdminWMSSourcesLayersComponent,
  AdminWMSSourcesImportLayerComponent, AdminWMSSourcesEditComponent
} from './wms-sources/index';
import {
  AdminLayersComponent, AdminLayersListComponent, AdminLayersEditComponent
} from './layers/index';
import {
  AdminGalleryThemesComponent, AdminGalleryThemesListComponent, AdminGalleryThemesEditComponent
} from './gallery-themes/index';
import {
  AdminGalleryItemsComponent, AdminGalleryItemsListComponent, AdminGalleryItemsEditComponent
} from './gallery-items/index';
import {
  AdminUsersComponent, AdminUsersListComponent, AdminUsersEditComponent
} from './users/index';
import { AdminImportShapefileComponent } from './import-shapefile';


export const ADMIN_ROUTES: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminAuthGuard],
    canActivateChild: [AdminAuthGuard],
    children: [{
      path: 'themes',
      component: AdminThemesComponent,
      children: [{
        path: '',
        component: AdminThemesListComponent
      }, {
        path: ':id',
        component: AdminThemesEditComponent
      }]
    }, {
      path: 'wms-sources',
      component: AdminWMSSourcesComponent,
      children: [{
        path: '',
        component: AdminWMSSourcesListComponent
      }, {
        path: ':id/edit',
        component: AdminWMSSourcesEditComponent
      }, {
        path: ':id/layers',
        component: AdminWMSSourcesLayersComponent
      }, {
        path: ':id/import/:name',
        component: AdminWMSSourcesImportLayerComponent
      }]
    }, {
      path: 'arcgis-sources',
      component: AdminArcGISSourcesComponent,
      children: [{
        path: '',
        component: AdminArcGISSourcesListComponent
      }, {
        path: ':id/edit',
        component: AdminArcGISSourcesEditComponent
      }, {
        path: ':id/layers',
        component: AdminArcGISSourcesLayersComponent
      }, {
        path: ':id/import/:name',
        component: AdminArcGISSourcesImportLayerComponent
      }]
    }, {
      path: 'layers',
      component: AdminLayersComponent,
      children: [{
        path: '',
        component: AdminLayersListComponent
      }, {
        path: ':id',
        component: AdminLayersEditComponent
      }]
    }, {
      path: 'gallery-themes',
      component: AdminGalleryThemesComponent,
      children: [{
        path: '',
        component: AdminGalleryThemesListComponent
      }, {
        path: ':id',
        component: AdminGalleryThemesEditComponent
      }]
    }, {
      path: 'gallery-items',
      component: AdminGalleryItemsComponent,
      children: [{
        path: '',
        component: AdminGalleryItemsListComponent
      }, {
        path: ':id',
        component: AdminGalleryItemsEditComponent
      }]
    }, {
      path: 'users',
      component: AdminUsersComponent,
      children: [{
        path: '',
        component: AdminUsersListComponent
      }, {
        path: ':id',
        component: AdminUsersEditComponent
      }]
    }, {
      path: 'import-shapefile',
      component: AdminImportShapefileComponent
    }, {
      path: '',
      redirectTo: 'layers',
      pathMatch: 'full'
    }]
  }
];
