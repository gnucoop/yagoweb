import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import {
  getAdminArcGISSources, getAdminArcGISSourcesLoading,
  IArcGISSource, ArcGISSourcesService, State
} from '@yago/core';

import { AdminArcGISSourcesDeleteDialogComponent } from './arcgis-sources-delete-dialog';


@Component({
  selector: 'yago-admin-arcgis-sources-list',
  templateUrl: './arcgis-sources-list.html',
  styleUrls: ['./arcgis-sources-list.scss']
})
export class AdminArcGISSourcesListComponent implements OnDestroy {
  private _arcgisSources: Observable<IArcGISSource[]>;
  get arcgisSources(): Observable<IArcGISSource[]> { return this._arcgisSources; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminArcGISSourcesDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _store: Store<State>,
    private _service: ArcGISSourcesService,
    private _dialog: MatDialog
  ) {
    this._arcgisSources = _store.select(getAdminArcGISSources);
    this._loading = _store.select(getAdminArcGISSourcesLoading);

    _service.list(undefined, true);
  }

  delete(arcgisSource: IArcGISSource): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminArcGISSourcesDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(arcgisSource, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
