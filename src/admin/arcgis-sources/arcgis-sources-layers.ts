import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';

import {
  State,
  getAdminArcGISSource, getAdminArcGISSourcesLayer, getAdminArcGISSourcesLayers,
  getAdminArcGISSourceLoading, getAdminArcGISSourcesLayersLoading,
  IArcGISSource, IArcGISSourceAvabilableLayer, ILayer, LayerTypes,
  IMap,
  ArcGISSourcesService,
  getMap
} from '@yago/core';

import { LoadingDialogComponent, MapDialogComponent } from '../../common/index';


@Component({
  selector: 'yago-admin-arcgis-sources-layers',
  templateUrl: './arcgis-sources-layers.html',
  styleUrls: ['./arcgis-sources-layers.scss']
})
export class AdminArcGISSourcesLayersComponent implements OnDestroy, OnInit {
  private _arcgisSource: IArcGISSource;
  get arcgisSource(): IArcGISSource { return this._arcgisSource; }

  private _layers: Observable<IArcGISSourceAvabilableLayer[]>;
  get layers(): Observable<IArcGISSourceAvabilableLayer[]> { return this._layers; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _currentMap: Observable<IMap>;
  get currentMap(): Observable<IMap> { return this._currentMap; }

  private _layer: Observable<IArcGISSourceAvabilableLayer>;
  private _showPreviewEvent: EventEmitter<IArcGISSourceAvabilableLayer> =
    new EventEmitter<IArcGISSourceAvabilableLayer>();
  private _dialogRef: MatDialogRef<MapDialogComponent>;
  private _loadingDialogRef: MatDialogRef<LoadingDialogComponent> | null;
  private _paramSubscription: Subscription;
  private _sourceSubscription: Subscription;
  private _showPreviewSubscription: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: ArcGISSourcesService,
    private _dialog: MatDialog,
    store: Store<State>
  ) {
    this._sourceSubscription = store.select(getAdminArcGISSource)
      .filter((s: IArcGISSource) => s != null)
      .subscribe((s: IArcGISSource) => this._arcgisSource = s);

    this._layers = store.select(getAdminArcGISSourcesLayers);
    this._layer = store.select(getAdminArcGISSourcesLayer);

    this._loading = store.select(getAdminArcGISSourceLoading).startWith(false)
      .combineLatest(store.select(getAdminArcGISSourcesLayersLoading).startWith(false))
      .map((loading: [boolean, boolean]) => loading[0] || loading[1]);

    this._showPreviewSubscription = this._showPreviewEvent
      .subscribe((layer: IArcGISSourceAvabilableLayer) => this._service.availableLayer(
        this.arcgisSource.id, layer.basePath, layer.id, true
      ));

    this._showPreviewSubscription.add(this._showPreviewEvent
      .combineLatest(this._layer)
      .subscribe((l: [IArcGISSourceAvabilableLayer, IArcGISSourceAvabilableLayer]) => {
        const rLayer = l[0];
        const layer = l[1];
        if (layer == null || rLayer.basePath !== layer.basePath || rLayer.id !== layer.id) {
          return;
        }
        if (this._loadingDialogRef == null) { return; }
        const s = this._loadingDialogRef.afterClosed()
          .subscribe(() => {
            s.unsubscribe();
            this._loadingDialogRef = null;
            this._dialogRef = this._dialog.open(MapDialogComponent);
            const coords = layer.boundingBox.coordinates;
            this._dialogRef.componentInstance.setLimits([
              coords[0][2][1], coords[0][0][1],
              coords[0][2][0], coords[0][0][0],
            ]);
            this._dialogRef.componentInstance.setMap({
              baseLayer: 'osm',
              layers: [{
                layerType: layer.layerType === 'RASTER' ? 'WMS_RASTER' : 'VECTOR',
                name: 'Preview',
                label: 'Preview',
                metadata: '',
                url: this.arcgisSource.serverUrl,
                sourceName: `${layer.basePath}:${layer.id}`,
                boundingBox: layer.boundingBox,
                features: layer.features
              }]
            });
          });
        this._loadingDialogRef.close();
      }));
  }

  ngOnInit(): void {
    this._paramSubscription = this._route.params
      .subscribe((params: Params) => {
        const id: number = +params['id'];
        this._service.get(id, true);
        this._service.availableLayers(id, true);
      });
  }

  ngOnDestroy(): void {
    if (this._sourceSubscription != null) { this._sourceSubscription.unsubscribe(); }
    if (this._paramSubscription != null) { this._paramSubscription.unsubscribe(); }
    if (this._showPreviewSubscription != null) { this._showPreviewSubscription.unsubscribe(); }
  }

  showLayerPreview(layer: IArcGISSourceAvabilableLayer): void {
    this._showLoadingDialog();
    this._showPreviewEvent.emit(layer);
  }

  getImportLink(layer: IArcGISSourceAvabilableLayer): string[] {
    return ['../import', encodeURIComponent(layer.basePath) + ':' + layer.id.toString()];
  }

  private _destroyLoadingDialog(): void {
    if (this._loadingDialogRef == null) { return; }
    this._loadingDialogRef.close();
    this._loadingDialogRef = null;
  }

  private _showLoadingDialog(): void {
    this._loadingDialogRef = this._dialog.open(LoadingDialogComponent);
  }
}
