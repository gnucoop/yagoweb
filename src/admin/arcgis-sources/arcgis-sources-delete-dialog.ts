import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-arcgis-sources-delete-dialog',
  templateUrl: './arcgis-sources-delete-dialog.html',
  styleUrls: ['./arcgis-sources-delete-dialog.scss']
})
export class AdminArcGISSourcesDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminArcGISSourcesDeleteDialogComponent>) { }
}
