import {
  Component, ElementRef, EventEmitter, OnDestroy, OnInit, Renderer, ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';

import {
  ILayer, ILayerTheme,
  getAdminLayer, getAdminLayerLoading, getAdminThemes, getAdminThemesLoading, Go,
  State,
  LayersUpdateAction,
  LayersService, ThemesService,
  decamelize
} from '@yago/core';


@Component({
  selector: 'yago-admin-layers-edit',
  templateUrl: './layers-edit.html',
  styleUrls: ['./layers-edit.scss']
})
export class AdminLayersEditComponent implements OnDestroy, OnInit {
  @ViewChild('metadataInput') metadataInput: ElementRef;

  private _layer: Observable<ILayer>;
  get layer(): Observable<ILayer> { return this._layer; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _themes: Observable<ILayerTheme[]>;
  get themes(): Observable<ILayerTheme[]> { return this._themes; }

  private _layerForm: FormGroup;
  get layerForm(): FormGroup { return this._layerForm; }

  private _fileReader: FileReader | null;

  private _saveLayerEvt: EventEmitter<void> = new EventEmitter<void>();
  private _paramsSub: Subscription;
  private _layerSub: Subscription;
  private _saveLayerSub: Subscription;

  private _metadata: {name: string, size: number, content?: string} | null = null;

  private _metadataChange = false;
  get metadataChange(): boolean { return this._metadataChange; }

  private _metadataLabel: string | null = null;
  get metadataLabel(): string | null { return this._metadataLabel; }

  private _dataLabels: Observable<{[name: string]: string}>;
  get dataLabels(): Observable<{[name: string]: string}> { return this._dataLabels; }

  private _dataNames: Observable<{name: string, display: string}[]>;
  get dataNames(): Observable<{name: string, display: string}[]> { return this._dataNames; }

  constructor(
    private _route: ActivatedRoute,
    private _store: Store<State>,
    private _service: LayersService,
    private _themesService: ThemesService,
    private _renderer: Renderer,
    fb: FormBuilder
  ) {
    this._layer = _store.select(getAdminLayer);
    this._loading = _store.select(getAdminLayerLoading).startWith(false)
      .combineLatest(_store.select(getAdminThemesLoading).startWith(false))
      .map((r: [boolean, boolean]) => r[0] || r[1]);
    this._themes = _store.select(getAdminThemes);

    this._dataLabels = this._layer
      .map((layer: ILayer) => this._getLayerDataLabels(layer))
      .publishReplay(1)
      .refCount();

    this._dataNames = this._dataLabels
      .map((l) => {
        return Object.keys(l)
          .sort((l1, l2) => l1.localeCompare(l2))
          .map(n => {
            return {name: n, display: decamelize(n)};
          });
      });

    this._layerForm = fb.group({
      name: [null, Validators.required],
      label: [null, Validators.required],
      theme: [null, Validators.required],
      dataLabels: fb.group({})
    });

    this._layerSub = this._layer
      .subscribe((layer: ILayer) => {
        if (layer == null) {
          this._layerForm.controls['dataLabels'] = fb.group({});
          this._layerForm.setValue({
            name: null,
            label: null,
            theme: null,
            dataLabels: {}
          });
          this._metadata = null;
          this._metadataChange = false;
          this._metadataLabel = null;
        } else {
          this._layerForm.setValue({
            name: layer.name,
            label: layer.label,
            theme: layer.theme,
            dataLabels: {}
          });
          this._metadata = null;
          this._metadataChange = false;
          const dataLabels = this._getLayerDataLabels(layer);
          this._layerForm.controls['dataLabels'] = fb.group(
            Object.keys(dataLabels)
              .reduce((o, l) => {
                o[l] = [dataLabels[l]];
                return o;
              }, {})
          );
          try {
            this._metadataLabel = layer.metadata.split('/').reverse()[0];
          } catch (e) { this._metadataLabel = null; }
        }
      });

    this._saveLayerSub = this._saveLayerEvt
      .withLatestFrom(this._layer)
      .subscribe((r: [void, ILayer]) => {
        let layer: ILayer = r[1];
        if (layer == null) { return; }
        layer = Object.assign({}, layer, this._layerForm.value, {
          dataLabels: this._layerForm.controls['dataLabels'].value
        });
        if (this._metadataChange) {
          layer = Object.assign({}, layer, {
            metadata: this._metadata
          });
        } else {
          delete (<any>layer).metadata;
        }
        this._store.dispatch(new LayersUpdateAction(layer, true));
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSub != null) { this._paramsSub.unsubscribe(); }
    if (this._layerSub != null) { this._layerSub.unsubscribe(); }
    if (this._saveLayerSub != null) { this._saveLayerSub.unsubscribe(); }
  }

  ngOnInit(): void {
    this._themesService.list(undefined, true);

    this._paramsSub = this._route.params
      .subscribe((params: Params) => {
        const id: number = +params['id'];
        if (isNaN(id) || id <= 0) {
          this._store.dispatch(new Go({path: ['/admin', 'themes']}));
        } else {
          this._service.get(id, true);
        }
      });
  }

  saveLayer(): void {
    this._saveLayerEvt.emit();
  }

  deleteMetadata(): void {
    this._metadataChange = true;
    this._metadata = null;
    this._metadataLabel = null;
  }

  setMetadata(files: any): void {
    if (files == null || files.length < 1) { return; }
    this._initFileReader();
    if (this._fileReader == null) { return; }
    const file = files[0];
    this._metadata = {name: file.name, size: file.size};
    this._fileReader.readAsDataURL(file);
  }

  private _getLayerDataLabels(layer: ILayer): {[name: string]: string} {
    if (
      layer == null || layer.features == null || layer.features.features == null ||
      layer.features.features.length === 0
    ) { return {}; }
    if (layer.dataLabels != null) { return layer.dataLabels; }
    return Object.keys(layer.features.features[0].properties || {})
      .reduce((o, l) => {
        o[l] = null;
        return o;
      }, {});
  }

  private _initFileReader(): void {
    const self = this;

    this._fileReader = new FileReader();
    this._fileReader.addEventListener('load', this._fileReadComplete.bind(this));
    this._fileReader.addEventListener('error', this._fileReadError.bind(this));
  }

  private _fileReadComplete(event: ProgressEvent) {
    this._destroyFileReader();
    if (this._metadata != null) {
      this._metadataLabel = this._metadata.name;
      this._metadataChange = true;
      this._metadata.content = (<any>event.target).result;
    }
  }

  private _fileReadError() {
    this._destroyFileReader();
    throw new Error(`Couldn't read file`);
  }

  private _destroyFileReader(): void {
    if (this._fileReader) {
      this._fileReader.removeEventListener('load', this._fileReadComplete);
      this._fileReader.removeEventListener('error', this._fileReadError);
      this._fileReader = null;
    }
  }
}
