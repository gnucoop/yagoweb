import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import {
  composeUrl, getAdminLayers, getAdminLayersLoading,
  ILayer, State, LayersService
} from '@yago/core';

import { environment } from '../../environments/environment';
import { AdminLayersDeleteDialogComponent } from './layers-delete-dialog';


@Component({
  selector: 'yago-admin-layers-list',
  templateUrl: './layers-list.html',
  styleUrls: ['./layers-list.scss']
})
export class AdminLayersListComponent implements OnDestroy {
  private _layers: Observable<ILayer[]>;
  get layers(): Observable<ILayer[]> { return this._layers; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminLayersDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _store: Store<State>,
    private _service: LayersService,
    private _dialog: MatDialog
  ) {
    this._layers = _store.select(getAdminLayers);
    this._loading = _store.select(getAdminLayersLoading);

    this._service.list(undefined, true);
  }

  delete(theme: ILayer): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminLayersDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(theme, true);
      });
  }

  getShapefileUrl(layer: number): string {
    return composeUrl([
      environment.apiConfig.baseUrl,
      environment.apiConfig.layersEndpoint,
      layer.toString(),
      environment.apiConfig.layersShapefileSubpath
    ]);
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
