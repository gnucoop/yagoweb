import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-layers-delete-dialog',
  templateUrl: './layers-delete-dialog.html',
  styleUrls: ['./layers-delete-dialog.scss']
})
export class AdminLayersDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminLayersDeleteDialogComponent>) { }
}
