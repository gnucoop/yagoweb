import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import {
  composeUrl,
  getAdminThemes, getAdminThemesLoading,
  AuthHttp, ILayerTheme, ILayer, LayerTypes, ThemesService,
  State
} from '@yago/core';

import { environment } from '../environments/environment';
import { LoadingDialogComponent } from '../common/index';


@Component({
  selector: 'yago-admin-import-shapefile',
  templateUrl: 'import-shapefile.html',
  styleUrls: ['import-shapefile.scss']
})
export class AdminImportShapefileComponent implements AfterViewInit {
  private _importForm: FormGroup;
  get importForm(): FormGroup { return this._importForm; }

  private _themes: Observable<ILayerTheme[]>;
  get themes(): Observable<ILayerTheme[]> { return this._themes; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _fileReader: FileReader | null;

  constructor(
    private _themesService: ThemesService,
    private _store: Store<State>,
    private _dialog: MatDialog,
    private _http: AuthHttp,
    private _router: Router,
    fb: FormBuilder
  ) {
    this._themes = this._store.select(getAdminThemes);
    this._themesService.list(undefined, true);
    this._loading = this._store.select(getAdminThemesLoading);

    this._importForm = fb.group({
      name: ['', [Validators.required, Validators.pattern('[A-Za-z0-9\-\_]+')]],
      label: ['', [Validators.required]],
      theme: [null, [Validators.required]],
      shapefile: [null, [Validators.required]]
    });
  }

  importLayer(): void {
    const dialogRef = this._dialog.open(LoadingDialogComponent);
    const s = this._http.post(
      composeUrl([
        environment.apiConfig.baseUrl,
        environment.apiConfig.importShapefileEndpoint
      ]),
      this._importForm.value
    ).subscribe((res) => {
      s.unsubscribe();
      const l = dialogRef.afterClosed()
        .subscribe(() => {
          l.unsubscribe();
          this._router.navigate(['/admin', 'layers']);
        });
      dialogRef.close();
    }, (e) => {
      s.unsubscribe();
    });
  }

  ngAfterViewInit(): void {
    this._initFileReader();
  }

  changedFileInput(input: any): void {
    if (input.files == null || input.files.length <= 0) {
      return;
    }
    this._initFileReader();
    (<FileReader>this._fileReader).readAsDataURL(input.files[0]);
  }

  _fileReadComplete(content: string, evt: any): void {
    this.importForm.controls['shapefile'].setValue(content);
    this._destroyFileReader();
  }

  _fileReadError(evt: any): void {
    this._destroyFileReader();
    throw new Error(`Couldn't read file`);
  }

  private _destroyFileReader(recreate = true): void {
    if (this._fileReader != null) {
      this._fileReader.removeEventListener('load');
      this._fileReader.removeEventListener('error');
      this._fileReader = null;
    }
    if (recreate) {
      this._initFileReader();
    }
  }

  private _initFileReader(): void {
    const self = this;
    this._fileReader = new FileReader();
    this._fileReader.addEventListener('load', (event: ProgressEvent) => {
      self._fileReadComplete((<FileReader>event.target).result, event);
    });
    this._fileReader.addEventListener('error', (event: ProgressEvent) => {
      self._fileReadError(event);
    });
  }
}
