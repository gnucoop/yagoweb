import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';

import {
  State,
  getAdminWMSSource, getAdminWMSSourcesLayer, getAdminWMSSourcesLayers,
  getAdminWMSSourceLoading, getAdminWMSSourcesLayersLoading,
  IWMSSource, IWMSSourceAvabilableLayer, ILayer, LayerTypes,
  IMap,
  WMSSourcesService,
  getMap
} from '@yago/core';

import { LoadingDialogComponent, MapDialogComponent } from '../../common/index';


@Component({
  selector: 'yago-admin-wms-sources-layers',
  templateUrl: './wms-sources-layers.html',
  styleUrls: ['./wms-sources-layers.scss']
})
export class AdminWMSSourcesLayersComponent implements OnDestroy, OnInit {
  private _wmsSource: IWMSSource;
  get wmsSource(): IWMSSource { return this._wmsSource; }

  private _layers: Observable<IWMSSourceAvabilableLayer[]>;
  get layers(): Observable<IWMSSourceAvabilableLayer[]> { return this._layers; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _currentMap: Observable<IMap>;
  get currentMap(): Observable<IMap> { return this._currentMap; }

  private _layer: Observable<IWMSSourceAvabilableLayer>;
  private _showPreviewEvent: EventEmitter<IWMSSourceAvabilableLayer> =
    new EventEmitter<IWMSSourceAvabilableLayer>();
  private _dialogRef: MatDialogRef<MapDialogComponent>;
  private _loadingDialogRef: MatDialogRef<LoadingDialogComponent> | null;
  private _paramSubscription: Subscription;
  private _sourceSubscription: Subscription;
  private _showPreviewSubscription: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: WMSSourcesService,
    private _dialog: MatDialog,
    store: Store<State>
  ) {
    this._sourceSubscription = store.select(getAdminWMSSource)
      .filter((s: IWMSSource) => s != null)
      .subscribe((s: IWMSSource) => this._wmsSource = s);

    this._layers = store.select(getAdminWMSSourcesLayers);
    this._layer = store.select(getAdminWMSSourcesLayer);

    this._loading = store.select(getAdminWMSSourceLoading).startWith(false)
      .combineLatest(store.select(getAdminWMSSourcesLayersLoading).startWith(false))
      .map((loading: [boolean, boolean]) => loading[0] || loading[1]);

    this._showPreviewSubscription = this._showPreviewEvent
      .subscribe((layer: IWMSSourceAvabilableLayer) => this._service.availableLayer(
        this.wmsSource.id, layer.workspace, layer.name, true
      ));

    this._showPreviewSubscription.add(this._showPreviewEvent
      .combineLatest(this._layer)
      .subscribe((l: [IWMSSourceAvabilableLayer, IWMSSourceAvabilableLayer]) => {
        const rLayer = l[0];
        const layer = l[1];
        if (layer == null || rLayer.workspace !== layer.workspace || rLayer.name !== layer.name) {
          return;
        }
        if (this._loadingDialogRef == null) { return; }
        const s = this._loadingDialogRef.afterClosed()
          .subscribe(() => {
            s.unsubscribe();
            this._loadingDialogRef = null;
            this._dialogRef = this._dialog.open(MapDialogComponent);
            const coords = layer.boundingBox.coordinates;
            this._dialogRef.componentInstance.setLimits([
              coords[0][2][1], coords[0][0][1],
              coords[0][2][0], coords[0][0][0],
            ]);
            this._dialogRef.componentInstance.setMap({
              baseLayer: 'osm',
              layers: [{
                layerType: layer.layerType === 'RASTER' ? 'WMS_RASTER' : 'VECTOR',
                name: 'Preview',
                label: 'Preview',
                metadata: '',
                url: this.wmsSource.serverUrl,
                sourceName: `${layer.name}:${layer.workspace}`,
                boundingBox: layer.boundingBox,
                features: layer.features
              }]
            });
          });
        this._loadingDialogRef.close();
      }));
  }

  ngOnInit(): void {
    this._paramSubscription = this._route.params
      .subscribe((params: Params) => {
        const id: number = +params['id'];
        this._service.get(id, true);
        this._service.availableLayers(id, true);
      });
  }

  ngOnDestroy(): void {
    if (this._sourceSubscription != null) { this._sourceSubscription.unsubscribe(); }
    if (this._paramSubscription != null) { this._paramSubscription.unsubscribe(); }
    if (this._showPreviewSubscription != null) { this._showPreviewSubscription.unsubscribe(); }
  }

  showLayerPreview(layer: IWMSSourceAvabilableLayer): void {
    this._showLoadingDialog();
    this._showPreviewEvent.emit(layer);
  }

  private _destroyLoadingDialog(): void {
    if (this._loadingDialogRef == null) { return; }
    this._loadingDialogRef.close();
    this._loadingDialogRef = null;
  }

  private _showLoadingDialog(): void {
    this._loadingDialogRef = this._dialog.open(LoadingDialogComponent);
  }
}
