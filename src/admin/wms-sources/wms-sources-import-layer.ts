import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';

import {
  getAdminWMSSource, getAdminWMSSourceLoading,
  getAdminWMSSourcesLayer, getAdminWMSSourcesLayerLoading,
  getAdminThemes, getAdminThemesLoading,
  IWMSSource, IWMSSourceAvabilableLayer,
  ILayerTheme, ILayer, LayerTypes,
  IMap,
  ThemesService, WMSSourcesService,
  State
} from '@yago/core';


@Component({
  selector: 'yago-admin-wms-sources-import-layer',
  templateUrl: './wms-sources-import-layer.html',
  styleUrls: ['./wms-sources-import-layer.scss']
})
export class AdminWMSSourcesImportLayerComponent implements OnDestroy, OnInit {
  private _wmsSource: IWMSSource;
  get wmsSource(): IWMSSource { return this._wmsSource; }

  private _layer: Observable<IWMSSourceAvabilableLayer>;
  get layer(): Observable<IWMSSourceAvabilableLayer> { return this._layer; }

  private _boundingBox: Observable<number[]>;
  get boundingBox(): Observable<number[]> { return this._boundingBox; }

  private _map: Observable<IMap | null>;
  get map(): Observable<IMap | null> { return this._map; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _importForm: FormGroup;
  get importForm(): FormGroup { return this._importForm; }

  private _themes: Observable<ILayerTheme[]>;
  get themes(): Observable<ILayerTheme[]> { return this._themes; }

  private _importEvent: EventEmitter<void> = new EventEmitter<void>();
  private _paramsSubscription: Subscription;
  private _sourceSubscription: Subscription;
  private _importSubscription: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: WMSSourcesService,
    private _themesService: ThemesService,
    private _store: Store<State>,
    fb: FormBuilder
  ) {
    this._layer = this._store.select(getAdminWMSSourcesLayer)
      .filter((layer: IWMSSourceAvabilableLayer) => layer != null);

    this._boundingBox = this._layer
      .map((layer: IWMSSourceAvabilableLayer) => {
        try {
          const coords = layer.boundingBox.coordinates;
          return [
            coords[0][2][1], coords[0][0][1],
            coords[0][2][0], coords[0][0][0],
          ];
        } catch (e) {
          return [-90.0000, 180.0000, 90.0000, -180.0000];
        }
      });

    this._sourceSubscription = this._store.select(getAdminWMSSource)
      .filter((source: IWMSSource) => source != null)
      .withLatestFrom(this._route.params)
      .subscribe((r: [IWMSSource, Params]) => {
        const source = r[0];
        const params = r[1];
        const layer: string[] = params['name'].split(':');
        this._wmsSource = source;
        this._service.availableLayer(source.id, layer[0], layer[1], true);
      });

    this._themesService.list(undefined, true);
    this._themes = this._store.select(getAdminThemes);

    this._map = this._store.select(getAdminWMSSourcesLayer)
      .filter((layer: IWMSSourceAvabilableLayer) => layer != null)
      .map((layer: IWMSSourceAvabilableLayer) => this._buildMap(layer));

    this._importForm = fb.group({
      name: ['', [Validators.required, Validators.pattern('[A-Za-z0-9\-\_]+')]],
      label: ['', [Validators.required]],
      theme: [null, [Validators.required]]
    });

    this._loading = this._store.select(getAdminWMSSourceLoading).startWith(false)
      .combineLatest(
        this._store.select(getAdminWMSSourcesLayerLoading).startWith(false),
        this._store.select(getAdminThemesLoading).startWith(false)
      )
      .map((l: [boolean, boolean, boolean]) => l[0] || l[1] || l[2]);

    this._importSubscription = this._importEvent
      .withLatestFrom(this._layer)
      .subscribe((r: [void, IWMSSourceAvabilableLayer]) => {
        const layer = r[1];
        const data = Object.assign({}, this.importForm.value, {
          boundingBox: layer.boundingBox
        });
        this._service.importAvailableLayer(
          this.wmsSource.id, layer.workspace, layer.name, data, true
        );
      });
  }

  ngOnInit(): void {
    this._paramsSubscription = this._route.params
      .subscribe((params: Params) => {
        const sourceId: number = +params['id'];
        const layer: string[] = params['name'].split(':');
        this._service.get(sourceId, true);
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription) { this._paramsSubscription.unsubscribe(); }
    if (this._sourceSubscription) { this._sourceSubscription.unsubscribe(); }
    if (this._importSubscription) { this._importSubscription.unsubscribe(); }
  }

  importLayer(): void {
    this._importEvent.emit();
  }

  private _buildMap(layer: IWMSSourceAvabilableLayer): IMap | null {
    if (this.wmsSource == null) {
      return null;
    }
    return {
      baseLayer: 'osm',
      layers: [{
        layerType: layer.layerType === 'RASTER' ? 'WMS_RASTER' : 'VECTOR',
        name: 'Preview',
        label: 'Preview',
        metadata: '',
        url: this.wmsSource.serverUrl,
        sourceName: `${layer.name}:${layer.workspace}`,
        boundingBox: layer.boundingBox,
        features: layer.features
      }]
    };
  }
}
