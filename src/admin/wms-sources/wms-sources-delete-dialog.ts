import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-wms-sources-delete-dialog',
  templateUrl: './wms-sources-delete-dialog.html',
  styleUrls: ['./wms-sources-delete-dialog.scss']
})
export class AdminWMSSourcesDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminWMSSourcesDeleteDialogComponent>) { }
}
