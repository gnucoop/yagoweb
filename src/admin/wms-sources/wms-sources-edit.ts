import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';

import { Go, IWMSSource, State, WMSSourcesService } from '@yago/core';


@Component({
  selector: 'yago-admin-wms-sources-edit',
  templateUrl: './wms-sources-edit.html',
  styleUrls: ['./wms-sources-edit.scss']
})
export class AdminWMSSourcesEditComponent implements OnDestroy, OnInit {
  private _source: Observable<IWMSSource | null>;
  get source(): Observable<IWMSSource | null> { return this._source; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _sourceForm: FormGroup;
  get sourceForm(): FormGroup { return this._sourceForm; }

  private _sourceId: number;
  private _saveEvent: EventEmitter<void> = new EventEmitter<void>();
  private _saveSubscription: Subscription;
  private _paramsSubscription: Subscription;
  private _sourceSub: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: WMSSourcesService,
    private _store: Store<State>,
    fb: FormBuilder
  ) {
    this._source = _service.getAdminWMSSource();
    this._loading = _service.getAdminWMSSourceLoading();

    this._service.list(undefined, true);

    this._sourceForm = fb.group({
      name: [null, Validators.required],
      serverUrl: [null, Validators.required],
      workspace: [null],
      username: [null],
      password: [null]
    });

    this._saveSubscription = this._saveEvent
      .subscribe((e) => {
        const source: IWMSSource = Object.assign({}, this.sourceForm.value, {
          id: this._sourceId
        });
        if (this._sourceId == null) {
          this._service.create(source);
        } else {
          this._service.update(source);
        }
      });

    this._sourceSub = this._source
      .subscribe((source: IWMSSource) => {
        if (source == null) {
          this._sourceForm.setValue({
            name: null,
            serverUrl: null,
            workspace: null,
            username: null,
            password: null
          });
        } else {
          this._sourceForm.setValue({
            name: source.name,
            serverUrl: source.serverUrl,
            workspace: source.workspace,
            username: source.username,
            password: source.password
          });
        }
      });
  }

  ngOnInit(): void {
    this._paramsSubscription = this._route.params
      .subscribe((params: Params) => {
        const id: string = params['id'];
        if (id !== 'new') {
          this._sourceId = +id;
          if (isNaN(this._sourceId) || this._sourceId <= 0) {
            this._store.dispatch(new Go({path: ['/admin', 'wms-sources']}));
          } else {
            this._service.get(this._sourceId, true);
          }
        } else {
          this._service.resetCurrentWMSSource(true);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription != null) { this._paramsSubscription.unsubscribe(); }
    if (this._saveSubscription != null) { this._saveSubscription.unsubscribe(); }
    if (this._sourceSub != null) { this._sourceSub.unsubscribe(); }
  }

  saveSource(): void {
    this._saveEvent.emit();
  }
}
