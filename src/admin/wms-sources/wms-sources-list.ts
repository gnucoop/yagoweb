import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import {
  getAdminWMSSources, getAdminWMSSourcesLoading,
  IWMSSource, WMSSourcesService, State
} from '@yago/core';

import { AdminWMSSourcesDeleteDialogComponent } from './wms-sources-delete-dialog';


@Component({
  selector: 'yago-admin-wms-sources-list',
  templateUrl: './wms-sources-list.html',
  styleUrls: ['./wms-sources-list.scss']
})
export class AdminWMSSourcesListComponent implements OnDestroy {
  private _wmsSources: Observable<IWMSSource[]>;
  get wmsSources(): Observable<IWMSSource[]> { return this._wmsSources; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminWMSSourcesDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _store: Store<State>,
    private _service: WMSSourcesService,
    private _dialog: MatDialog
  ) {
    this._wmsSources = _store.select(getAdminWMSSources);
    this._loading = _store.select(getAdminWMSSourcesLoading);

    _service.list(undefined, true);
  }

  delete(wmsSource: IWMSSource): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminWMSSourcesDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(wmsSource, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
