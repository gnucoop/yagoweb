import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-gallery-items-delete-dialog',
  templateUrl: './gallery-items-delete-dialog.html',
  styleUrls: ['./gallery-items-delete-dialog.scss']
})
export class AdminGalleryItemsDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminGalleryItemsDeleteDialogComponent>) { }
}
