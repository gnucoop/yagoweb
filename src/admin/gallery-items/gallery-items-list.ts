import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import { IGalleryItem, GalleryItemsService } from '@yago/core';

import { AdminGalleryItemsDeleteDialogComponent } from './gallery-items-delete-dialog';


@Component({
  selector: 'yago-admin-gallery-items-list',
  templateUrl: './gallery-items-list.html',
  styleUrls: ['./gallery-items-list.scss']
})
export class AdminGalleryItemsListComponent implements OnDestroy {
  private _items: Observable<IGalleryItem[]>;
  get items(): Observable<IGalleryItem[]> { return this._items; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminGalleryItemsDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _service: GalleryItemsService,
    private _dialog: MatDialog
  ) {
    this._items = _service.getAdminItems();
    this._loading = _service.getAdminItemsLoading();

    this._service.list(undefined, true);
  }

  delete(item: IGalleryItem): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminGalleryItemsDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(item, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
