import {
  Component, ElementRef, EventEmitter, OnDestroy, OnInit, Renderer, ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';

import {
  IGalleryItem, IGalleryTheme, GalleryItemsService, GalleryThemesService, Go, State
} from '@yago/core';


@Component({
  selector: 'yago-admin-gallery-items-edit',
  templateUrl: './gallery-items-edit.html',
  styleUrls: ['./gallery-items-edit.scss']
})
export class AdminGalleryItemsEditComponent implements OnDestroy, OnInit {
  @ViewChild('attachmentInput') attachmentInput: ElementRef;

  private _item: Observable<IGalleryItem | null>;
  get item(): Observable<IGalleryItem | null> { return this._item; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _themes: Observable<IGalleryTheme[]>;
  get themes(): Observable<IGalleryTheme[]> { return this._themes; }

  private _itemForm: FormGroup;
  get itemForm(): FormGroup { return this._itemForm; }

  private _fileReader: FileReader | null;

  private _saveItemEvt: EventEmitter<void> = new EventEmitter<void>();
  private _paramsSub: Subscription;
  private _itemSub: Subscription;
  private _saveItemSub: Subscription;

  private _attachment: { name: string, size: number, content?: string } | null;

  private _attachmentChange = false;
  get attachmentChange(): boolean { return this._attachmentChange; }

  private _attachmentLabel: string | null;
  get attachmentLabel(): string | null { return this._attachmentLabel; }

  constructor(
    private _route: ActivatedRoute,
    private _store: Store<State>,
    private _service: GalleryItemsService,
    private _themesService: GalleryThemesService,
    private _renderer: Renderer,
    fb: FormBuilder
  ) {
    this._item = _service.getAdminItem();
    this._loading = _service.getAdminItemLoading().startWith(false)
      .combineLatest(_themesService.getAdminThemesLoading().startWith(false))
      .map((r: [boolean, boolean]) => r[0] || r[1]);
    this._themes = _themesService.getAdminThemes();

    this._itemForm = fb.group({
      itemType: [null, Validators.required],
      theme: [null, Validators.required],
      name: [null, Validators.required],
      description: [null]
    });

    this._itemSub = this._item
      .subscribe((item: IGalleryItem) => {
        if (item == null) {
          this._itemForm.setValue({
            itemType: null,
            theme: null,
            name: null,
            description: null
          });
          this._attachment = null;
          this._attachmentChange = false;
          this._attachmentLabel = null;
        } else {
          this._itemForm.setValue({
            itemType: item.itemType,
            theme: item.theme,
            name: item.name,
            description: item.description
          });
          this._attachment = null;
          this._attachmentChange = false;
          try {
            this._attachmentLabel = item.attachment.split('/').reverse()[0];
          } catch (e) { this._attachmentLabel = null; }
        }
      });

    this._saveItemSub = this._saveItemEvt
      .withLatestFrom(this._item)
      .subscribe((r: [void, IGalleryItem]) => {
        let item: IGalleryItem = r[1] || <any>{};
        item = Object.assign({}, item, this._itemForm.value);
        if (this._attachmentChange) {
          item = Object.assign({}, item, {
            attachment: this._attachment
          });
        } else {
          delete item.attachment;
        }
        if (item.id != null) {
          this._service.update(item, true);
        } else {
          this._service.create(item, true);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSub != null) { this._paramsSub.unsubscribe(); }
    if (this._itemSub != null) { this._itemSub.unsubscribe(); }
    if (this._saveItemSub != null) { this._saveItemSub.unsubscribe(); }
  }

  ngOnInit(): void {
    this._themesService.list(undefined, true);

    this._paramsSub = this._route.params
      .subscribe((params: Params) => {
        const id: string = params['id'];
        if (id !== 'new') {
          const iid: number = +id;
          if (isNaN(iid) || iid <= 0) {
            this._store.dispatch(new Go({ path: ['/admin', 'themes'] }));
          } else {
            this._service.get(iid, true);
          }
        } else {
          this._service.resetCurrentItem(true);
        }
      });
  }

  saveItem(): void {
    this._saveItemEvt.emit();
  }

  deleteAttachment(): void {
    this._attachmentChange = true;
    this._attachment = null;
    this._attachmentLabel = null;
  }

  setAttachment(files: any): void {
    if (files == null || files.length < 1) { return; }
    const file = files[0];

    if (!this._fileReader) {
      this._initFileReader();
    }

    if (this._fileReader) {
      this._attachment = { name: file.name, size: file.size };
      this._fileReader.readAsDataURL(file);
    }
  }

  private _initFileReader(): void {
    const self = this;

    this._fileReader = new FileReader();
    this._fileReader.addEventListener('load', (event: ProgressEvent) => {
      self._fileReadComplete(<FileReader>event.target);
    });
    this._fileReader.addEventListener('error', (event: any) => {
      self._fileReadError();
    });
  }

  private _fileReadComplete(event: any) {
    this._destroyFileReader();
    if (this._attachment != null) {
      this._attachmentLabel = this._attachment.name;
      this._attachmentChange = true;
      this._attachment.content = event.result;
    }
  }

  private _fileReadError() {
    this._destroyFileReader();
    throw new Error(`Couldn't read file`);
  }

  private _destroyFileReader(): void {
    if (this._fileReader) {
      this._fileReader.removeEventListener('load');
      this._fileReader.removeEventListener('error');
      this._fileReader = null;
    }
  }
}
