import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-gallery-themes-delete-dialog',
  templateUrl: './gallery-themes-delete-dialog.html',
  styleUrls: ['./gallery-themes-delete-dialog.scss']
})
export class AdminGalleryThemesDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminGalleryThemesDeleteDialogComponent>) { }
}
