import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';

import { IGalleryTheme, GalleryThemesService, Go, State } from '@yago/core';


@Component({
  selector: 'yago-admin-gallery-themes-edit',
  templateUrl: './gallery-themes-edit.html',
  styleUrls: ['./gallery-themes-edit.scss']
})
export class AdminGalleryThemesEditComponent implements OnDestroy, OnInit {
  private _theme: Observable<IGalleryTheme | null>;
  get theme(): Observable<IGalleryTheme | null> { return this._theme; }

  private _themes: Observable<IGalleryTheme[]>;
  get themes(): Observable<IGalleryTheme[]> { return this._themes; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _themeForm: FormGroup;
  get themeForm(): FormGroup { return this._themeForm; }

  private _themeId: number;
  private _saveEvent: EventEmitter<void> = new EventEmitter<void>();
  private _saveSubscription: Subscription;
  private _paramsSubscription: Subscription;
  private _themeSub: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _service: GalleryThemesService,
    private _store: Store<State>,
    fb: FormBuilder
  ) {
    this._theme = _service.getAdminTheme();
    this._themes = _service.getAdminThemes();
    this._loading = _service.getAdminThemeLoading().startWith(false)
      .combineLatest(_service.getAdminThemesLoading().startWith(false))
      .map((l: [boolean, boolean]) => l[0] || l[1]);

    this._service.list(undefined, true);

    this._themeForm = fb.group({
      name: ['', Validators.required],
      parent: [null]
    });

    this._saveSubscription = this._saveEvent
      .subscribe((e: [void, IGalleryTheme]) => {
        const theme: IGalleryTheme = Object.assign({}, this.themeForm.value, {
          id: this._themeId
        });
        if (this._themeId != null) {
          this._service.update(theme, true);
        } else {
          this._service.create(theme);
        }
      });

    this._themeSub = this._theme
      .subscribe((theme: IGalleryTheme) => {
        this._themeForm.controls['name'].setValue(theme != null ? theme.name : null);
        this._themeForm.controls['parent'].setValue(theme != null ? theme.parent : null);
      });
  }

  ngOnInit(): void {
    this._paramsSubscription = this._route.params
      .subscribe((params: Params) => {
        const id: string = params['id'];
        if (id !== 'new') {
          this._themeId = +id;
          if (isNaN(this._themeId) || this._themeId <= 0) {
            this._store.dispatch(new Go({path: ['/admin', 'gallery-themes']}));
          } else {
            this._service.get(this._themeId, true);
          }
        } else {
          this._service.resetCurrentTheme(true);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription != null) { this._paramsSubscription.unsubscribe(); }
    if (this._saveSubscription != null) { this._saveSubscription.unsubscribe(); }
    if (this._themeSub != null) { this._themeSub.unsubscribe(); }
  }

  saveTheme(): void {
    this._saveEvent.emit();
  }
}
