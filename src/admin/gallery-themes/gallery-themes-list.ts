import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import { IGalleryTheme, GalleryThemesService } from '@yago/core';

import { AdminGalleryThemesDeleteDialogComponent } from './gallery-themes-delete-dialog';


@Component({
  selector: 'yago-admin-gallery-themes-list',
  templateUrl: './gallery-themes-list.html',
  styleUrls: ['./gallery-themes-list.scss']
})
export class AdminGalleryThemesListComponent implements OnDestroy {
  private _themes: Observable<IGalleryTheme[]>;
  get themes(): Observable<IGalleryTheme[]> { return this._themes; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminGalleryThemesDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _service: GalleryThemesService,
    private _dialog: MatDialog
  ) {
    this._themes = _service.getAdminThemes();
    this._loading = _service.getAdminThemesLoading();

    this._service.list(undefined, true);
  }

  delete(theme: IGalleryTheme): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminGalleryThemesDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(theme, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
