import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';

import { YagoWebCommonModule } from '../common/index';
import { AdminComponent } from './admin';
import {
  AdminThemesComponent, AdminThemesListComponent, AdminThemesEditComponent,
  AdminThemesDeleteDialogComponent
} from './themes/index';
import {
  AdminArcGISSourcesComponent, AdminArcGISSourcesListComponent, AdminArcGISSourcesLayersComponent,
  AdminArcGISSourcesImportLayerComponent, AdminArcGISSourcesDeleteDialogComponent,
  AdminArcGISSourcesEditComponent
} from './arcgis-sources/index';
import {
  AdminWMSSourcesComponent, AdminWMSSourcesListComponent, AdminWMSSourcesLayersComponent,
  AdminWMSSourcesImportLayerComponent, AdminWMSSourcesDeleteDialogComponent,
  AdminWMSSourcesEditComponent
} from './wms-sources/index';
import {
  AdminLayersComponent, AdminLayersListComponent, AdminLayersEditComponent,
  AdminLayersDeleteDialogComponent
} from './layers/index';
import {
  AdminGalleryThemesComponent, AdminGalleryThemesListComponent, AdminGalleryThemesEditComponent,
  AdminGalleryThemesDeleteDialogComponent
} from './gallery-themes/index';
import {
  AdminGalleryItemsComponent, AdminGalleryItemsListComponent, AdminGalleryItemsEditComponent,
  AdminGalleryItemsDeleteDialogComponent
} from './gallery-items/index';
import {
  AdminUsersComponent, AdminUsersListComponent, AdminUsersEditComponent,
  AdminUsersDeleteDialogComponent
} from './users/index';
import { AdminImportShapefileComponent } from './import-shapefile';
import { ADMIN_ROUTES } from './admin.routes';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(ADMIN_ROUTES),

    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatToolbarModule,

    FlexLayoutModule,
    YagoWebCommonModule
  ],
  declarations: [
    AdminComponent,
    AdminThemesComponent,
    AdminThemesDeleteDialogComponent,
    AdminThemesListComponent,
    AdminThemesEditComponent,
    AdminArcGISSourcesComponent,
    AdminArcGISSourcesDeleteDialogComponent,
    AdminArcGISSourcesListComponent,
    AdminArcGISSourcesEditComponent,
    AdminArcGISSourcesLayersComponent,
    AdminArcGISSourcesImportLayerComponent,
    AdminWMSSourcesComponent,
    AdminWMSSourcesDeleteDialogComponent,
    AdminWMSSourcesListComponent,
    AdminWMSSourcesEditComponent,
    AdminWMSSourcesLayersComponent,
    AdminWMSSourcesImportLayerComponent,
    AdminLayersComponent,
    AdminLayersDeleteDialogComponent,
    AdminLayersListComponent,
    AdminLayersEditComponent,
    AdminGalleryThemesComponent,
    AdminGalleryThemesListComponent,
    AdminGalleryThemesDeleteDialogComponent,
    AdminGalleryThemesEditComponent,
    AdminGalleryItemsComponent,
    AdminGalleryItemsListComponent,
    AdminGalleryItemsDeleteDialogComponent,
    AdminGalleryItemsEditComponent,
    AdminUsersComponent,
    AdminUsersListComponent,
    AdminUsersDeleteDialogComponent,
    AdminUsersEditComponent,
    AdminImportShapefileComponent
  ],
  entryComponents: [
    AdminArcGISSourcesDeleteDialogComponent,
    AdminWMSSourcesDeleteDialogComponent,
    AdminThemesDeleteDialogComponent,
    AdminLayersDeleteDialogComponent,
    AdminGalleryThemesDeleteDialogComponent,
    AdminGalleryItemsDeleteDialogComponent,
    AdminUsersDeleteDialogComponent
  ]
})
export class AdminModule { }
