import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'yago-admin-users-delete-dialog',
  templateUrl: './users-delete-dialog.html',
  styleUrls: ['./users-delete-dialog.scss']
})
export class AdminUsersDeleteDialogComponent {
  constructor(public dialogRef: MatDialogRef<AdminUsersDeleteDialogComponent>) { }
}
