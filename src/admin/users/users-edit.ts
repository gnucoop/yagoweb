import {
  Component, ElementRef, EventEmitter, OnDestroy, OnInit, Renderer, ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';

import {
  AuthService, IUser, Go, IGalleryTheme, UsersService, State,
  emailValidator, passwordMatchValidator, uniqueEmailValidator, uniqueUsernameValidator
} from '@yago/core';


@Component({
  selector: 'yago-admin-users-edit',
  templateUrl: './users-edit.html',
  styleUrls: ['./users-edit.scss']
})
export class AdminUsersEditComponent implements OnDestroy, OnInit {
  private _user: Observable<IUser | null>;
  get user(): Observable<IUser | null> { return this._user; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _userForm: FormGroup;
  get userForm(): FormGroup { return this._userForm; }

  private _fileReader: FileReader;

  private _saveUserEvt: EventEmitter<void> = new EventEmitter<void>();
  private _paramsSub: Subscription;
  private _userSub: Subscription;
  private _saveUserSub: Subscription;
  private _valueChangesSub: Subscription | null;

  constructor(
    private _route: ActivatedRoute,
    private _store: Store<State>,
    private _service: UsersService,
    private _authService: AuthService,
    private _renderer: Renderer,
    fb: FormBuilder
  ) {
    this._user = _service.getAdminUser();
    this._loading = _service.getAdminUserLoading();

    this._userForm = fb.group({
      username: [null, Validators.required],
      email: [null, Validators.required],
      firstName: [null],
      lastName: [null],
      isStaff: [false, Validators.required],
      password: [null],
      confirmPassword: [null]
    }, {
      validator: passwordMatchValidator
    });

    this._userSub = this._user
      .subscribe((user: IUser) => {
        if (this._valueChangesSub != null) {
          this._valueChangesSub.unsubscribe();
          this._valueChangesSub = null;
        }
        const usernameControl = this._userForm.controls['username'];
        const emailControl = this._userForm.controls['email'];
        usernameControl.clearAsyncValidators();
        emailControl.clearAsyncValidators();
        if (user == null) {
          this._userForm.setValue({
            username: null,
            email: null,
            firstName: null,
            lastName: null,
            password: null,
            confirmPassword: null,
            isStaff: false
          });
          usernameControl.setAsyncValidators(uniqueUsernameValidator(this._authService));
          emailControl.setAsyncValidators(uniqueEmailValidator(this._authService));
          this._userForm.updateValueAndValidity();
        } else {
          const origUsername = user.username;
          const origEmail = user.email;
          this._userForm.setValue({
            username: user.username,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            password: null,
            confirmPassword: null,
            isStaff: user.isStaff
          });
          this._valueChangesSub = this._userForm.valueChanges
            .subscribe((v) => {
              let changed = false;
              if (v.username !== origUsername && usernameControl.asyncValidator == null) {
                usernameControl.setAsyncValidators(
                  uniqueUsernameValidator(this._authService, origUsername)
                );
                changed = true;
              } else if (v.username === origUsername && usernameControl.asyncValidator != null) {
                usernameControl.clearAsyncValidators();
                changed = true;
              }
              if (changed) {
                usernameControl.updateValueAndValidity();
              }
              changed = false;
              if (v.email !== origEmail && emailControl.asyncValidator == null) {
                emailControl.setAsyncValidators(
                  uniqueEmailValidator(this._authService, origEmail)
                );
                changed = true;
              } else if (v.email === origEmail && emailControl.asyncValidator != null) {
                emailControl.clearAsyncValidators();
                changed = true;
              }
              if (changed) {
                emailControl.updateValueAndValidity();
              }
            });
        }
      });

    this._saveUserSub = this._saveUserEvt
      .withLatestFrom(this._user)
      .subscribe((r: [void, IUser]) => {
        let user: IUser = r[1] || <any>{};
        user = Object.assign({}, user, this._userForm.value);
        if (user.id != null) {
          this._service.update(user, true);
        } else {
          this._service.create(user, true);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSub != null) { this._paramsSub.unsubscribe(); }
    if (this._userSub != null) { this._userSub.unsubscribe(); }
    if (this._saveUserSub != null) { this._saveUserSub.unsubscribe(); }
  }

  ngOnInit(): void {
    this._paramsSub = this._route.params
      .subscribe((params: Params) => {
        const id: string = params['id'];
        if (id !== 'new') {
          const iid: number = +id;
          if (isNaN(iid) || iid <= 0) {
            this._store.dispatch(new Go({path: ['/admin', 'themes']}));
          } else {
            this._service.get(iid, true);
          }
        } else {
          this._service.resetCurrentUser(true);
        }
      });
  }

  saveUser(): void {
    this._saveUserEvt.emit();
  }

  keys(dict: any): string[] {
    if (dict == null) { return []; }
    return Object.keys(dict);
  }
}
