import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Store } from '@ngrx/store';

import { IUser, UsersService } from '@yago/core';

import { AdminUsersDeleteDialogComponent } from './users-delete-dialog';


@Component({
  selector: 'yago-admin-users-list',
  templateUrl: './users-list.html',
  styleUrls: ['./users-list.scss']
})
export class AdminUsersListComponent implements OnDestroy {
  private _users: Observable<IUser[]>;
  get users(): Observable<IUser[]> { return this._users; }

  private _loading: Observable<boolean>;
  get loading(): Observable<boolean> { return this._loading; }

  private _deleteConfirmDialog: MatDialogRef<AdminUsersDeleteDialogComponent> | null;
  private _deleteSub: Subscription | null;

  constructor(
    private _service: UsersService,
    private _dialog: MatDialog
  ) {
    this._users = _service.getAdminUsers();
    this._loading = _service.getAdminUsersLoading();

    this._service.list(undefined, true);
  }

  delete(user: IUser): void {
    this._destroyDeleteConfirm();
    this._deleteConfirmDialog = this._dialog.open(AdminUsersDeleteDialogComponent);
    this._deleteSub = this._deleteConfirmDialog.afterClosed()
      .subscribe((res: boolean) => {
        if (!res) { return; }
        this._service.delete(user, true);
      });
  }

  ngOnDestroy(): void {
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
  }

  private _destroyDeleteConfirm(): void {
    if (this._deleteConfirmDialog == null) { return; }
    if (this._deleteSub != null) { this._deleteSub.unsubscribe(); }
    this._deleteConfirmDialog.close();
    this._deleteConfirmDialog = null;
    this._deleteSub = null;
  }
}
