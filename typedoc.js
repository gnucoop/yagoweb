module.exports = {
  'mode': 'modules',
  'out': 'doc',
  'theme': 'node_modules/@gnucoop/typedoc-theme',
  'ignoreCompilerErrors': 'true',
  'experimentalDecorators': 'true',
  'emitDecoratorMetadata': 'true',
  'target': 'ES5',
  'moduleResolution': 'node',
  'preserveConstEnums': 'true',
  'stripInternal': 'true',
  'suppressExcessPropertyErrors': 'true',
  'suppressImplicitAnyIndexErrors': 'true',
  'module': 'commonjs'
};
